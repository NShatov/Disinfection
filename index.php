<!DOCTYPE HTML>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta name="author" content="Контент">
    <meta name="keywords" content="keywords">
    <meta name="description" content="description">

    <title>О компании</title>

    <link rel="icon" type="image/png" href="favicon.png">
    <link rel="stylesheet" href="assets/vendor.css">
    <link rel="stylesheet" href="assets/app.min.css">

    <meta property="og:title" content="title"/>
    <meta property="og:description" content="description"/>
    <meta property="og:image" content="favicon.png">

</head>
<body>

<? require_once 'templates/_blocks/header.php'; ?>

<div class="wrapper">
    <div class="wrapper__content">
        <? require_once 'pages/about.php'; ?>
    </div>
    <? require_once 'templates/_blocks/footer.php'; ?>
</div>

<?
    
    require_once 'templates/_modals/_modal-callback.php';
    require_once 'templates/_modals/_modal-requisites.php';
    require_once 'templates/_modals/_modal-vacancy.php';
?>
<script src="/assets/app.min.js"></script>
</body>
</html>