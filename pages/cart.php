<!DOCTYPE HTML>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta name="author" content="Контент">
    <meta name="keywords" content="keywords">
    <meta name="description" content="description">

    <title>Корзина</title>

    <link rel="icon" type="image/png" href="/favicon.png">
    <link rel="stylesheet" href="/assets/vendor.css">
    <link rel="stylesheet" href="/assets/app.min.css">

    <meta property="og:title" content="title"/>
    <meta property="og:description" content="description"/>
    <meta property="og:image" content="favicon.png">

</head>
<body>

<? require_once '../templates/_blocks/header.php'; ?>
<? require_once '../templates/_blocks/cart_informer.php'; ?>

<div class="wrapper">
    <div class="wrapper__content">
        <div class="nx-section nx-section_page container" data-cart>
            <h4 class="nx-section__title">Корзина</h4>

            <div class="flex-row" data-affix-parent>
                <div class="flex-col md-18">
                    <div class="nx-cart-head">
                        <div class="nx-cart-head__item nx-cart-head__item_title">Товар</div>
                        <div class="nx-cart-head__item nx-cart-head__item_price">Цена</div>
                        <div class="nx-cart-head__item nx-cart-head__item_cnt">Колличество</div>
                        <div class="nx-cart-head__item nx-cart-head__item_total">Сумма</div>
                    </div>
                    
                    <div class="nx-cart-list">
                        <? for ($i = 0; $i < 2; $i++): ?>
                            <div class="nx-cart-list__item"
                                 data-prod-weight="14.55"
                                 data-cart-el
                                 data-type="change"
                                 data-prod-type="prod"
                                 data-prod="<? echo $i ?>">
                                <div class="nx-cart-list__main">
                                    <a href="#" class="nx-cart-list__img-link">
                                        <img src="https://via.placeholder.com/200x200"
                                             alt=""
                                             class="nx-cart-list__img">
                                    </a>
                                    
                                    <div class="nx-cart-list__main-body">
                                        <div class="nx-cart-list__note">Артикул: <span>10073940</span></div>
                                        
                                        <a href="#" class="nx-cart-list__title">
                                            <span data-prod-title="Баня Б-150 4х4 М с террасой и большой верандой ">Баня Б-150 4х4 М с террасой и большой верандой</span>
                                        </a>
                                    </div>
                                </div>
                                
                                <div class="nx-cart-list__price-wr">
                                    <div class="nx-cart-list__price nx-cart-list__price_old">
                                        <span>1 577 000</span> <i class="rub">q</i>
                                    </div>
                                    
                                    <div class="nx-cart-list__price" data-prod-price="1577000">
                                        1 577 000 <i class="rub">q</i>
                                        <span class="nx-cart-list__price-cnt">за шт</span>
                                    </div>
                                </div>
                                
                                <div class="nx-cart-list__counter">
                                    <div class="nx-counter" data-counter>
                                        <a href="#"
                                           class="nx-counter-btn nx-counter-btn_minus"
                                           data-counter-btn="minus"></a>
                                        
                                        <input name="products[quantity]"
                                               type="text"
                                               value="5"
                                               class="nx-counter__input nx-form-element"
                                               data-counter-inp
                                               data-max="999"
                                               data-prod-cnt>
                                        
                                        <a href="#"
                                           class="nx-counter-btn nx-counter-btn_plus"
                                           data-counter-btn="plus"></a>
                                    </div>
                                </div>
                                
                                <div class="nx-cart-list__total">
                                    <div class="nx-cart-list__total-price">
                                        <span data-prod-price-total>1 577 000</span> <i class="rub">q</i>
                                    </div>
                                    
                                    <div class="nx-cart-list__total-weight">
                                        <span data-prod-weight-total="14.55">14.55</span>
                                        <span>кг</span>
                                    </div>
                                </div>
                                
                                <a href="#"
                                   class="nx-cart-list__remove"
                                   data-type="delete"
                                   data-cart-remove>
                                    <svg class="icon"><use xlink:href="#icon-close"></use></svg>
                                </a>

                                <div class="nx-cart-list__deleted">
                                    <div class="nx-cart-list__deleted-text">Товар удален !</div>
                                    
                                    <a href="#"
                                       class="link"
                                       data-type="add"
                                       data-prod-add>
                                        <span>Вернуть товар в корзину</span>
                                    </a>
                                </div>
                            </div>
                        <? endfor; ?>
                    </div>
                    
                    <div class="nx-cart-complects"
                         data-cart-el
                         data-prod="123"
                         data-prod-weight="14.55"
                         data-type="change"
                         data-prod-type="set">
                        <div class="nx-cart-list__deleted" data-prod-title="Комплект">
                            <div class="nx-cart-list__deleted-text">Комплект удален !</div>

                            <a href="#"
                               class="link"
                               data-type="add"
                               data-prod-add>
                                <span>Вернуть комплект в корзину</span>
                            </a>
                        </div>
                        <div class="nx-counter" data-counter>
                            <a href="#"
                               class="nx-counter-btn nx-counter-btn_minus"
                               data-counter-btn="minus"></a>

                            <input name="products[quantity]"
                                   type="text"
                                   value="1"
                                   class="nx-counter__input nx-form-element"
                                   data-counter-inp
                                   data-max="999"
                                   data-prod-cnt>

                            <a href="#"
                               class="nx-counter-btn nx-counter-btn_plus"
                               data-counter-btn="plus"></a>
                        </div>
                        
                        <? for ($j = 0; $j < 2; $j++): ?>
                            <div class="nx-cart-list__item"
                                 data-set-item
                                 data-set-weight="14.55">
                                <div class="nx-cart-list__main">
                                    <a href="#" class="nx-cart-list__img-link">
                                        <img src="https://via.placeholder.com/200x200"
                                             alt=""
                                             class="nx-cart-list__img">
                                    </a>

                                    <div class="nx-cart-list__main-body">
                                        <div class="nx-cart-list__note">Артикул:
                                            <span>10073940</span>
                                        </div>

                                        <a href="#" class="nx-cart-list__title">
                                            <span>Баня Б-150 4х4 М с террасой и большой верандой</span>
                                        </a>
                                    </div>
                                </div>

                                <div class="nx-cart-list__price-wr">
                                    <div class="nx-cart-list__price" data-cart-price="1577000">1 577 000
                                        <i class="rub">q</i>
                                        
                                        <span class="nx-cart-list__price-cnt">за шт</span>
                                    </div>
                                </div>

                                <div class="nx-cart-list__counter">
                                    <span data-set-counter>1</span> шт
                                </div>

                                <div class="nx-cart-list__total">
                                    <div class="nx-cart-list__total-price">
                                        <span data-set-price-total>1 577 000</span>

                                        <i class="rub">q</i>
                                    </div>

                                    <div class="nx-cart-list__total-weight">
                                        <span data-set-weight-total>14.55</span>
                                        
                                        <span>кг</span>
                                    </div>
                                </div>
                            </div>
                        <? endfor; ?>
                        
                        <div class="nx-set-total">
                            <div class="nx-set-total__discount">
                                Скидка <span data-set-discount="54000">54 000</span>
                                
                                <i class="rub">q</i>
                            </div>
                            
                            <div class="nx-set-total__body">
                                <div class="nx-set-total__label">Комплект со скидкой:</div>
                                
                                <div class="nx-set-total__price">
                                    <span data-prod-price="3100000">3 100 000</span>
                                    
                                    <i class="rub">q</i>
                                </div>
                                
                                <div class="nx-set-total__price-old">
                                    <span data-set-old-price="3200000">3 200 000</span>
                                    
                                    <i class="rub">q</i>
                                </div>
                            </div>
                        </div>

                        <a href="#"
                           class="nx-cart-complects__remove"
                           data-type="delete"
                           data-cart-remove>
                            <svg class="icon"><use xlink:href="#icon-close"></use></svg>
                        </a>
                    </div>
                </div>
                
                <div class="flex-col md-6" data-affix-wrap>
                    <div class="nx-affix" data-affix-block>
                        <div class="nx-cart-info">
                            <div class="nx-cart-info__title">Ваш заказ</div>
                            
                            <div class="nx-cart-info__body">
                                <div class="nx-cart-info__total">Итого, <span data-cart-count>4</span>
                                    <span
                                            data-cart-note
                                            data-words="позиция, позиции,позиций">позиции</span>:</div>
                                
                                <div class="nx-cart-info__price h3">
                                    <span data-cart-total-price>6 308 000</span>
                                    
                                    <i class="rub">q</i>
                                </div>
                                
                                <div class="nx-cart-info__discount">
                                    <span data-total-discount>Скидка: 33 000</span>
                                    <i class="rub">q</i>
                                </div>
                                
                                <div class="nx-cart-info__weight">Вес:
                                    <span data-cart-total-weight>58,83</span> кг
                                </div>
                            </div>

                            <a href="#"
                               class="btn btn_d-block"
                               data-cart-btn>
                                <span>Оформить заказ</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            
            <div data-stop-affix></div>
        </div>
    </div>
    <? require_once '../templates/_blocks/footer.php'; ?>
</div>

<script src="/assets/app.min.js"></script>
</body>
</html>
