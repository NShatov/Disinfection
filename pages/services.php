<!DOCTYPE HTML>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta name="author" content="Контент">
    <meta name="keywords" content="keywords">
    <meta name="description" content="description">

    <title>Услуги</title>

    <link rel="icon" type="image/png" href="/favicon.png">
    <link rel="stylesheet" href="/assets/vendor.css">
    <link rel="stylesheet" href="/assets/app.min.css">

    <meta property="og:title" content="title"/>
    <meta property="og:description" content="description"/>
    <meta property="og:image" content="favicon.png">

</head>
<body>

<? require_once '../templates/_blocks/header.php'; ?>

<div class="wrapper">
    <div class="wrapper__content">
        <div class="nx-section container">
            <div class="nx-section__head">
                <div class="nx-section__title h2">Услуги</div>
            </div>

            <div class="flex-row nx-listing nx-listing_serv">
                <? for ($i = 0; $i < 8; $i++): ?>
                    <div class="flex-col sm-8 nx-listing__item">
                        <a href="#" class="nx-listing__link">
                            <div class="nx-listing__head">
                                <img src="https://via.placeholder.com/750x420" alt="" class="nx-listing__img">
                            </div>
                            
                            <div class="nx-listing__body">
                                <span class="nx-listing__title">Распродажа зимней коллекции 2020 в самом разгаре</span>
                            </div>
                        </a>
                    </div>
                <? endfor; ?>
            </div>
        </div>

        <div class="nx-section container">
            <div class="content-narrow-med">
                <div class="nx-section__head">
                    <div class="text">
                        <h2>SEO текст</h2>
                        <p>Создание или разработка дизайн макета является первоначальным этапом в изготовление рекламы.
                            Главная задача любой рекламы – привлечение внимания потенциальных покупателей и побуждение к
                            действию. Продуманный дизайн листовок, баннеров, щитов даст хороший эффект и поможет вам
                            продвинуть свой бизнес. В ходе разработки макета широкоформатная печать и полиграфия требует
                            к себе определенного подхода.Красивый дизайн также сможет создать положительный имидж вашей
                            организации и показать потребителям, что вы серьезно относитесь к своему делу</p>
                        <img src="https://via.placeholder.com/995x560" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <? require_once '../templates/_blocks/footer.php'; ?>
</div>

<script src="/assets/app.min.js"></script>
</body>
</html>
