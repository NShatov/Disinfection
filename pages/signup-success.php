<!DOCTYPE HTML>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <meta name="author" content="Контент">
    <meta name="keywords" content="keywords">
    <meta name="description" content="description">
    
    <title>Корзина оформление заказа</title>
    
    <link rel="icon" type="image/png" href="/favicon.png">
    <link rel="stylesheet" href="/assets/vendor.css">
    <link rel="stylesheet" href="/assets/app.min.css">
    
    <meta property="og:title" content="title"/>
    <meta property="og:description" content="description"/>
    <meta property="og:image" content="favicon.png">

</head>
<body>

<? require_once '../templates/_blocks/header.php'; ?>

<div class="wrapper wrapper_flex">
    <div class="wrapper__content">
        <div class="container">
            <div class="nx-message">
                <div class="nx-message__body">
                    <h3 class="nx-message__title">
                        <span class="nx-message__icon" style="background-image: url('https://via.placeholder.com/100x100')"></span>
                        Спасибо за регистрацию!</h3>
                    
                    <div class="nx-message__text">Мы отправили вам на почту инструкции по активации аккаунта, письмо придет в течение 5 минут. Если письмо не приходит, проверьте папку “спам”</div>
            
                    <div class="nx-actions">
                        <div class="nx-actions__item">
                            <a href="#" class="btn">
                                <span>Хорошо</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <? require_once '../templates/_blocks/footer.php'; ?>
</div>

<script src="/assets/app.min.js"></script>
</body>
</html>
