<!DOCTYPE HTML>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <meta name="author" content="Контент">
    <meta name="keywords" content="keywords">
    <meta name="description" content="description">
    
    <title>Сравнение</title>
    
    <link rel="icon" type="image/png" href="/favicon.png">
    <link rel="stylesheet" href="/assets/vendor.css">
    <link rel="stylesheet" href="/assets/app.min.css">
    
    <meta property="og:title" content="title"/>
    <meta property="og:description" content="description"/>
    <meta property="og:image" content="favicon.png">

</head>
<body>

<? require_once '../templates/_blocks/header.php'; ?>

<div class="wrapper">
    <div class="wrapper__content">
        <div class="nx-section nx-section_page container">
            <div class="nx-section__head">
                <h2 class="nx-section__title">Сравнение</h2>
        
                <a href="#" class="nx-section__link link">
                    <span>Удалить весь список</span>
                </a>
            </div>
            
            <div class="nx-compare-list-wr" data-compare-head>
                <div class="nx-compare-list">
                    <div class="nx-compare-list__item"></div>
                </div>
                
                <div class="nx-compare-list" data-attach-dragger="multiple">
                    <? for ($i = 0; $i < 12; $i++): ?>
                        <div class="nx-compare-list__item">
                            <div class="nx-compare-list__img">
                                <img src="https://via.placeholder.com/160x160" alt="">
                            </div>
                            
                            <div class="nx-compare-list__body">
                                <a href="" class="nx-compare-list__title">
                                    <span>Колесный диск Alutec X10 8.5x18/5x120 D65.1 ET50 Racing Black</span>
                                </a>
                                
                                <div class="nx-compare-list__price">12 720
                                    <i class="rub">q</i>
                                </div>
                            </div>
                        </div>
                    <? endfor; ?>
                </div>
            </div>
    
            <div class="nx-section__block">
                <div class="nx-tabs" data-tabs-slider>
                    <div class="swiper-wrapper nx-tabs__wrap">
                        <? for ($i = 0; $i < 3; $i++): ?>
                            <div class="swiper-slide nx-tabs__item <?if ($i == 0):?>active<?endif;?>">
                                <a href="#"
                                   class="nx-tabs__link"
                                   data-nx-tabs="content-load"
                                   data-load-id="123"
                                   data-nx-url="">
                                    
                                    <span>Колесные диски (3)</span>
                                </a>
                            </div>
                        <? endfor; ?>
                    </div>
                </div>
            </div>

            <div class="nx-section__block" data-compare-body>
                <div class="nx-compare-wr">
                    <div class="nx-listing nx-listing_prod nx-listing_compare">
                        <div class="nx-listing__item nx-listing__item-compare">
                            <a href="#" class="nx-listing__link">
                                <div class="nx-listing__body">
                                    <div class="nx-listing__add">+</div>
                                    
                                    <div class="nx-listing__caption">Добавить товары к сравнению</div>
                                </div>
                            </a>
                        </div>
                    </div>
                    
                    <div class="nx-listing nx-listing_prod nx-listing_compare" data-attach-dragger="multiple">
                        <? for ($i = 0; $i < 12; $i++): ?>
                            <div class="nx-listing__item">
                                <div class="nx-listing__icons">
                                    <a href="#" class="nx-listing__icon-btn">
                                        <svg class="nx-listing__icon"><use xlink:href="#icon-close"></use></svg>
                                    </a>
                                </div>
                                
                                <a href="#" class="nx-listing__link">
                                    <div class="nx-listing__head">
                                        <img src="https://via.placeholder.com/550x550"
                                             alt=""
                                             class="nx-listing__img">
                    
                                        <div class="nx-tags nx-tags_small">
                                            <? for ($j = 0; $j < 3; $j++): ?>
                                                <div class="nx-tags__item">Лучшая цена</div>
                                            <? endfor; ?>
                                        </div>
                                    </div>
                    
                                    <div class="nx-listing__body">
                                        <div class="nx-listing__note">
                                            <div class="nx-listing__articul">Артикул:
                                                <span>10073940</span>
                                            </div>
                    
                                            <div class="nx-listing__availible">В наличии</div>
                                        </div>
                    
                                        <span class="nx-listing__title">Баня Б-150 4х4 М с террасой и большой верандой </span>
                    
                                        <div class="spacer-v"></div>
                                    </div>
                                </a>
                    
                                <div class="nx-listing__actions">
                                    <div class="nx-listing__actions-row">
                                        <div class="nx-listing__actions-item">
                                            <div class="nx-prices-item">
                                                <div class="nx-prices-item__current">8 000 &#8381;</div>
                                            </div>
                                        </div>
                                        
                                        <div class="nx-listing__actions-item">
                                            <a href="" class="link nx-listing__buy">
                                                <svg class="icon"><use xlink:href="#icon-cart"></use></svg>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <? endfor; ?>
                    </div>
                </div>
            </div>
            
            <div class="nx-section__item">
                <div class="nx-compare-filter">
                    <div class="nx-compare-filter__title">Показывать:</div>
                    
                    <div class="nx-compare-filter__item">
                        <a href="#" class="nx-compare-filter__link btn btn_br active">
                            <span>Все характеристики</span>
                        </a>
                    </div>
    
                    <div class="nx-compare-filter__item">
                        <a href="#" class="nx-compare-filter__link btn btn_br">
                            <span>Различающиеся характеристики</span>
                        </a>
                    </div>
                </div>
            </div>
            
            <div class="nx-section__block">
                <div class="nx-compare-wr">
                    <div class="nx-compare nx-compare_light" data-compare-table-col>
                        <? for ($x = 0; $x < 15; $x++): ?>
                            <div class="nx-compare__row" data-compare-row="<?echo $x?>">
                                <div class="nx-compare__cell" data-compare-cell>Диаметр центрального отверстия (DIA) Диаметр центрального отверстия (DIA) Диаметр центрального отверстия (DIA) Диаметр центрального отверстия (DIA)</div>
                            </div>
                        <?endfor;?>
                    </div>
                    
                    <div data-attach-dragger="multiple">
                        <div class="nx-compare" data-compare-table-body>
                            <? for ($x = 0; $x < 15; $x++): ?>
                                <div class="nx-compare__row" data-compare-row="<?echo $x?>">
                                    <? for ($i = 0; $i < 5; $i++): ?>
                                        <div class="nx-compare__cell" data-compare-cell>Легкий сплав</div>
                                    <? endfor; ?>
                                </div>
                            <? endfor; ?>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    <? require_once '../templates/_blocks/footer.php'; ?>
</div>

<script src="/assets/app.min.js"></script>
</body>
</html>
