<!DOCTYPE HTML>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <meta name="author" content="Контент">
    <meta name="keywords" content="keywords">
    <meta name="description" content="description">
    
    <title>Корзина сокращенный вариант</title>
    
    <link rel="icon" type="image/png" href="/favicon.png">
    <link rel="stylesheet" href="/assets/vendor.css">
    <link rel="stylesheet" href="/assets/app.min.css">
    
    <meta property="og:title" content="title"/>
    <meta property="og:description" content="description"/>
    <meta property="og:image" content="favicon.png">

</head>
<body data-home="">

<? require_once '../templates/_blocks/header.php'; ?>
<? require_once '../templates/_blocks/cart_informer.php'; ?>

<div class="wrapper">
    <div class="wrapper__content">
        <div class="nx-section nx-section_page container" data-cart>
            <h4 class="nx-section__title">Корзина</h4>
            
            <div class="nx-section__box">
                <div class="flex-row" data-affix-parent>
                    <div class="flex-col md-18">
                        <div class="nx-cart-head">
                            <div class="nx-cart-head__item nx-cart-head__item_title">Товар</div>
                            
                            <div class="nx-cart-head__item nx-cart-head__item_price">Цена</div>
                            
                            <div class="nx-cart-head__item nx-cart-head__item_cnt">Колличество</div>
                            
                            <div class="nx-cart-head__item nx-cart-head__item_total">Сумма</div>
                        </div>
                        
                        <div class="nx-cart-list">
                            <? for ($i = 0; $i < 2; $i++): ?>
                                <div class="nx-cart-list__item" data-cart-el data-prod="<? echo $i ?>">
                                    <div class="nx-cart-list__main">
                                        <a href="#" class="nx-cart-list__img-link">
                                            <img src="https://via.placeholder.com/200x200"
                                                 alt=""
                                                 class="nx-cart-list__img">
                                        </a>
                                        
                                        <div class="nx-cart-list__main-body">
                                            <div class="nx-cart-list__note">Артикул:
                                                <span>10073940</span>
                                            </div>
                                            
                                            <a href="#" class="nx-cart-list__title">
                                                <span>Баня Б-150 4х4 М с террасой и большой верандой</span>
                                            </a>
                                        </div>
                                    </div>
                                    
                                    <div class="nx-cart-list__price-wr">
                                        <div class="nx-cart-list__price nx-cart-list__price_old">
                                            <span>1 577 000</span>
                                            
                                            <i class="rub">q</i>
                                        </div>
                                        
                                        <div class="nx-cart-list__price" data-cart-price>1 577 000
                                            <i class="rub">q</i>
                                            <span class="nx-cart-list__price-cnt">за шт</span>
                                        </div>
                                    </div>
                                    
                                    <div class="nx-cart-list__counter">
                                        <div class="nx-counter" data-counter>
                                            <a href="#"
                                               class="nx-counter-btn nx-counter-btn_minus"
                                               data-counter-btn="minus"></a>
                                            
                                            <input name="products[quantity]"
                                                   type="text"
                                                   value="1"
                                                   class="nx-counter__input nx-form-element"
                                                   data-counter-inp
                                                   data-max="999"
                                                   data-prod-cnt>
                                            
                                            <a href="#"
                                               class="nx-counter-btn nx-counter-btn_plus"
                                               data-counter-btn="plus"></a>
                                        </div>
                                    </div>
                                    
                                    <div class="nx-cart-list__total">
                                        <div class="nx-cart-list__total-price">
                                            <span>1 577 000</span>
                                            
                                            <i class="rub">q</i>
                                        </div>
                                        
                                        <div class="nx-cart-list__total-weight">
                                            <span>14.55</span>
                                            <span>кг</span>
                                        </div>
                                    </div>
                                    
                                    <a href="#"
                                       class="nx-cart-list__remove"
                                       data-cart-remove>
                                        <svg class="icon"><use xlink:href="#icon-close"></use></svg>
                                    </a>
                                    
                                    <div class="nx-cart-list__deleted">
                                        <div class="nx-cart-list__deleted-text">Товар удален !</div>
                                        
                                        <a href="#" class="link">
                                            <span>Вернуть товар в корзину</span>
                                        </a>
                                    </div>
                                </div>
                            <? endfor; ?>
                        </div>
                        
                        <div class="nx-cart-complects">
                            <? for ($j = 0; $j < 2; $j++): ?>
                                <div class="nx-cart-list__item">
                                    <div class="nx-cart-list__main">
                                        <a href="#" class="nx-cart-list__img-link">
                                            <img src="https://via.placeholder.com/200x200"
                                                 alt=""
                                                 class="nx-cart-list__img">
                                        </a>
                                        
                                        <div class="nx-cart-list__main-body">
                                            <div class="nx-cart-list__note">Артикул:
                                                <span>10073940</span>
                                            </div>
                                            
                                            <a href="#" class="nx-cart-list__title">
                                                <span>Баня Б-150 4х4 М с террасой и большой верандой</span>
                                            </a>
                                        </div>
                                    </div>
                                    
                                    <div class="nx-cart-list__price-wr">
                                        <div class="nx-cart-list__price nx-cart-list__price_old">
                                            <span>1 577 000</span>
                                            
                                            <i class="rub">q</i>
                                        </div>
                                        
                                        <div class="nx-cart-list__price" data-cart-price>1 577 000
                                            <i class="rub">q</i>
                                            
                                            <span class="nx-cart-list__price-cnt">за шт</span>
                                        </div>
                                    </div>
                                    
                                    <div class="nx-cart-list__counter">
                                        <span>1</span> шт
                                    </div>
                                    
                                    <div class="nx-cart-list__total">
                                        <div class="nx-cart-list__total-price">
                                            <span>1 577 000</span>
                                            
                                            <i class="rub">q</i>
                                        </div>
                                        
                                        <div class="nx-cart-list__total-weight">
                                            <span>14.55</span>
                                            
                                            <span>кг</span>
                                        </div>
                                    </div>
                                </div>
                            <? endfor; ?>
                            
                            <div class="nx-set-total">
                                <div class="nx-set-total__discount">
                                    Скидка <span>54 000</span>
                                    
                                    <i class="rub">q</i>
                                </div>
                                
                                <div class="nx-set-total__body">
                                    <div class="nx-set-total__label">Комплект со скидкой:</div>
                                    
                                    <div class="nx-set-total__price">
                                        <span>3 100 000</span>
                                        
                                        <i class="rub">q</i>
                                    </div>
                                    
                                    <div class="nx-set-total__price-old">
                                        <span>3 200 000</span>
                                        
                                        <i class="rub">q</i>
                                    </div>
                                </div>
                            </div>
                            
                            <a href="#"
                               class="nx-cart-complects__remove"
                               data-cart-remove>
                                <svg class="icon"><use xlink:href="#icon-close"></use></svg>
                            </a>
                        </div>
                    </div>
                    
                    <div class="flex-col md-6">
                        <div class="nx-affix" data-affix-wrap>
                            <div class="nx-cart-info" data-affix-block>
                                <div class="nx-cart-info__title">Ваш заказ</div>
                                
                                <div class="nx-cart-info__body">
                                    <div class="nx-cart-info__total">Итого, <span data-cart-count>4</span> позиции:</div>
                                    
                                    <div class="nx-cart-info__price h3" data-cart-total-price>6 308 000
                                        <i class="rub">q</i>
                                    </div>
                                    
                                    <div class="nx-cart-info__discount">Скидка: 33 000
                                        <i class="rub">q</i>
                                    </div>
                                    
                                    <div class="nx-cart-info__weight">Вес:
                                        <span data-cart-weight>58,83</span>кг
                                    </div>
                                </div>
                                
                                <a href="#" class="btn btn_d-block">
                                    <span>Оформить заказ</span>
                                </a>
                                
                                <div class="nx-cart-info__note note">Нажимая кнопку “оформить заказ” вы соглашаетесь с
                                    <a href="" target="_blank">
                                        <span>политикой конфиденциальности</span>
                                    </a>
                                </div>
                                
                                <div class="nx-cart-info__text">Для заказа заполните все поля и выберите способ доставки</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    
            <div class="nx-section__box">
                <form action="">
                    <div class="nx-section__col">
                        <div class="h4">Оформление заказа</div>
                
                        <div class="flex-row">
                            <div class="flex-col md-16">
                                <div class="nx-section__item">
                                    <div class="form-title">Контактная информация</div>
                            
                                    <div class="form-group" data-form-group>
                                        <label class="nx-dynamic-label" data-dynamic-label>
                                            <input type="text"
                                                   class="nx-dynamic-label__input nx-form-element"
                                                   name="name"
                                                   value=""
                                                   data-dynamic-inp>
                                    
                                            <span class="nx-dynamic-label__text">Имя</span>
                                        </label>
                                    </div>
                            
                                    <div class="form-group" data-form-group>
                                        <label class="nx-dynamic-label" data-dynamic-label>
                                            <input type="text"
                                                   class="nx-dynamic-label__input nx-form-element"
                                                   name="phone"
                                                   value=""
                                                   data-phone-mask
                                                   data-dynamic-inp>
                                    
                                            <span class="nx-dynamic-label__text">Телефон</span>
                                        </label>
                                    </div>
                            
                                    <div class="form-group" data-form-group>
                                        <label class="nx-dynamic-label" data-dynamic-label>
                                            <input type="text"
                                                   class="nx-dynamic-label__input nx-form-element"
                                                   name="email"
                                                   value=""
                                                   data-dynamic-inp>
                                    
                                            <span class="nx-dynamic-label__text">Электронная почта</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                
                        <div class="nx-section__item">
                            <div class="form-title">Доставка</div>
                    
                            <div class="nx-tabs nx-tabs_small" data-tabs-slider>
                                <ul class="nx-tabs__wrap nav nav-tabs swiper-wrapper">
                                    <li class="nav-item swiper-slide nx-tabs__item">
                                        <a href="#pickup"
                                           class="nav-link nx-tabs__link active"
                                           data-toggle="tab">
                                            <span>Самовывоз из магазина</span>
                                        </a>
                                    </li>
                            
                                    <li class="nav-item swiper-slide nx-tabs__item">
                                        <a href="#delivery"
                                           class="nav-link nx-tabs__link"
                                           data-toggle="tab">
                                            <span>Доставка до дома</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                    
                            <div class="tab-content">
                                <div class="tab-pane fade show active" id="pickup">
                                    <div class="flex-row nx-section__item">
                                        <div class="flex-col md-16">
                                            <div class="nx-pickup-info">
                                                <svg class="nx-pickup-info__icon"><use xlink:href="#icon-info"></use></svg>
                                        
                                                <div class="nx-pickup-info__text">Ваш заказ будет готов к выдаче 20.03.2020 в 12:00 и будет храниться до 12:00 24.03.2020. Для получения заказа необходимо иметь при себе документ удостоверяющий личность</div>
                                            </div>
                                        </div>
                                    </div>
                            
                                    <div class="flex-row">
                                        <div class="flex-col md-8">
                                            <div class="nx-contacts">
                                                <div class="nx-contacts__item">
                                                    <div class="nx-contacts__label">Местоположение:</div>
                                            
                                                    <div class="nx-contacts__text">Стройбаза “Резонанс Котово”, Псковский район, деревня Котово, дом 18</div>
                                                </div>
                                        
                                                <div class="nx-contacts__item">
                                                    <div class="nx-contacts__label">Пн-Пт:</div>
                                            
                                                    <div class="nx-contacts__text">9:00 - 19:00</div>
                                                </div>
                                        
                                                <div class="nx-contacts__item">
                                                    <div class="nx-contacts__label">Сб-Вс:</div>
                                            
                                                    <div class="nx-contacts__text">9:00 - 16:00</div>
                                                </div>
                                            </div>
                                        </div>
                                
                                        <div class="flex-col md-14">
                                            <div class="nx-map nx-map_small"
                                                 data-map
                                                 data-key="9b463b43-c9e4-41ad-aafc-a04e7d4a0243"
                                                 data-coord-x="44.23232323"
                                                 data-coord-y="44.23232323">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        
                                <div class="tab-pane fade" id="delivery">
                                    <div class="flex-row nx-section__item">
                                        <div class="flex-col md-16">
                                            <a href="#"
                                               class="link"
                                               data-map-link>
                                                <span data-map-link-text>Скрыть карту</span>
                                            </a>
                                    
                                            <div class="form-group">
                                                <div class="nx-map nx-map_xsmall"
                                                     data-map-target
                                                     data-map></div>
                                            </div>
                                    
                                            <div class="form-group-row">
                                                <div class="form-group" data-form-group>
                                                    <label class="nx-dynamic-label" data-dynamic-label>
                                                        <input type="text"
                                                               class="nx-dynamic-label__input nx-form-element"
                                                               name="address"
                                                               value=""
                                                               data-dynamic-inp>
                                                
                                                        <span class="nx-dynamic-label__text">Адрес</span>
                                                    </label>
                                            
                                                    <div class="form-group__label">Город, улица, дом/офис</div>
                                                </div>
                                        
                                                <div class="form-group" data-form-group>
                                                    <label class="nx-dynamic-label" data-dynamic-label>
                                                        <input type="text"
                                                               class="nx-dynamic-label__input nx-form-element"
                                                               name="delivery_date"
                                                               value=""
                                                               data-dynamic-inp>
                                                
                                                        <span class="nx-dynamic-label__text">Дата и время</span>
                                                    </label>
                                            
                                                    <svg class="nx-tooltip__icon"><use xlink:href="#icon-question"></use></svg>
                                                    
                                                    <div class="nx-tooltip__item">
                                                        <div class="nx-tooltip__text">Желаемое время доставки. Точное время определяется от загруженности службы, и обговаривается по телефону</div>
                                                        
                                                        <div class="nx-tooltip__arrow"></div>
                                                    </div>
                                                </div>
                                            </div>
                                    
                                            <div class="form-group-row form-group-row_inline">
                                                <div class="form-group">
                                                    <label class="checkbox checkbox_radio">
                                                        <input type="radio"
                                                               class="checkbox__input"
                                                               checked
                                                               value="entrance"
                                                               name="delivery_type"
                                                               data-delivery-type-input>
                                                
                                                        <span class="checkbox__text">Доставка до подъезда</span>
                                                    </label>
                                                </div>
                                        
                                                <div class="form-group">
                                                    <label class="checkbox checkbox_radio">
                                                        <input type="radio"
                                                               class="checkbox__input"
                                                               value="floor"
                                                               name="delivery_type"
                                                               data-delivery-type-input>
                                                
                                                        <span class="checkbox__text">Подъем на этаж</span>
                                                    </label>
                                                </div>
                                            </div>
                                    
                                            <div class="nx-delivery-info" data-delivery-info>
                                                <div class="nx-delivery-info__controls">
                                                    <div class="form-group nx-delivery-info__input" data-form-group>
                                                        <label class="nx-dynamic-label" data-dynamic-label>
                                                            <input type="text"
                                                                   class="nx-dynamic-label__input nx-form-element"
                                                                   name="delivery_floor"
                                                                   data-dynamic-inp>
                                                    
                                                            <span class="nx-dynamic-label__text">Номер этажа</span>
                                                        </label>
                                                    </div>
                                            
                                                    <label class="checkbox">
                                                        <input type="checkbox"
                                                               class="checkbox__input"
                                                               name="elevator">
                                                
                                                        <span class="checkbox__text">Лифт</span>
                                                    </label>
                                            
                                                    <label class="checkbox">
                                                        <input type="checkbox"
                                                               class="checkbox__input"
                                                               name="service_elevator">
                                                
                                                        <span class="checkbox__text">Грузовой лифт</span>
                                                    </label>
                                                </div>
                                                <div class="nx-delivery-info__price">
                                                    <div class="nx-delivery-info__price-title">Доставка:</div>
                                            
                                                    <div class="nx-delivery-info__price-cost">350
                                                        <i class="rub">q</i>
                                                    </div>
                                                </div>
                                            </div>
                                    
                                            <div class="form-group" data-form-group>
                                                <label class="nx-dynamic-label" data-dynamic-label>
                                                    <textarea class="nx-dynamic-label__input nx-form-element"
                                                              name="comment"
                                                              rows="1"
                                                              data-dynamic-inp
                                                              data-autosize-textarea></textarea>
                                            
                                                    <span class="nx-dynamic-label__text">Комментарий к заказу</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                
                        <div class="nx-section__item">
                            <div class="flex-row">
                                <? for ($i = 0; $i < 2; $i++): ?>
                                    <div class="flex-col md-8 sm-8">
                                        <label class="checkbox checkbox_big">
                                            <input type="radio"
                                                   name="payment_type_id"
                                                   class="checkbox__input"
                                                   value="1"
                                                   <?php if ($i===0):?>checked <?php endif; ?>
                                            >
                                    
                                            <span class="checkbox__wrap">
                                            <span class="checkbox__title">Онлайн на сайте</span>
                                            
                                            <span class="checkbox__text">Банковской картой</span>
                                            <?php if ($i===0):?>
                                                <span class="checkbox__list">
                                                    <? for ($x = 0; $x < 3; $x++): ?>
                                                        <img src="https://via.placeholder.com/130x30"
                                                             alt=""
                                                             class="checkbox__img">
                                                    <? endfor; ?>
                                                    </span>
                                            <?php endif; ?>
                                        </span>
                                        </label>
                                    </div>
                                <? endfor; ?>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div data-stop-affix></div>
        </div>
    </div>
    
    <? require_once '../templates/_blocks/footer.php'; ?>
</div>

<script src="/assets/app.min.js"></script>
</body>
</html>
