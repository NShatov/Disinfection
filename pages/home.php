<!DOCTYPE HTML>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta name="author" content="Контент">
    <meta name="keywords" content="keywords">
    <meta name="description" content="description">

    <title>Главная</title>

    <link rel="icon" type="image/png" href="/favicon.png">
    <link rel="stylesheet" href="/assets/vendor.css">
    <link rel="stylesheet" href="/assets/app.min.css">

    <meta property="og:title" content="title"/>
    <meta property="og:description" content="description"/>
    <meta property="og:image" content="favicon.png">

</head>
<body>

<? require_once '../templates/_blocks/header.php'; ?>

<div class="wrapper">
    <div class="wrapper__content">
        <div class="nx-section nx-section_page">
            <div class="nx-section container">
                <div class="nx-promo-slider">
                    <div class="nx-promo-slider__slider" data-promo-slider>
                        <div class="swiper-wrapper nx-promo-slider__wrapper">
                            <? for ($i = 0; $i < 2; $i++): ?>
                                <div class="swiper-slide nx-promo-slider__slide" style="background-image: url('https://via.placeholder.com/1920x1080');">
                                    <div class="bg-fade"></div>
                                    
                                    <div class="nx-promo-slider__content">
                                        <h3 class="nx-promo-slider__title">Новое поступление товара</h3>
                                        
                                        <div class="nx-promo-slider__text text content-narrow-med">
                                            <p>Начинаем новый сезон с распродажи! Горячие новинки и коллекции
                                                предыдущего сезона
                                                по
                                                невероятным ценам. Приходите и убедитесь в этом сами</p>
                                        </div>
                                        
                                        <div class="nx-promo-slider__actions">
                                            <div class="nx-actions">
                                                <div class="nx-actions__item">
                                                    <a href="#" class="btn btn_lt"> Смотреть подробнее</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <? endfor; ?>
                        </div>

                        <div class="nx-promo-slider__dots" data-promo-dots></div>

                        <div class="nx-promo-slider__prev" data-promo-prev>
                            <svg class="nx-promo-slider__icon"><use xlink:href="#icon-arrow"></use></svg>
                        </div>
                        
                        <div class="nx-promo-slider__next nx-promo-slider__next" data-promo-next>
                            <svg class="nx-promo-slider__icon"><use xlink:href="#icon-arrow"></use></svg>
                        </div>
                    </div>
                </div>
            </div>

            <section class="nx-section container" data-nx-tabs-wrapper>
                <div class="nx-section__head">
                    <h2 class="nx-section__title">Промо-блок</h2>
                    
                    <a href="#" class="nx-section__link link"><span>Смотреть весь каталог</span></a>
                </div>
                
                <div class="nx-tabs" data-tabs-slider>
                    <div class="swiper-wrapper nx-tabs__wrap">
                        <div class="swiper-slide nx-tabs__item is-active">
                            <a href="#"
                               class="nx-tabs__link"
                               data-nx-tabs="content-load"
                               data-load-id="123"
                               data-nx-url="test-1">
                                <span>Обувь</span></a>
                        </div>
                        
                        <div class="swiper-slide nx-tabs__item">
                            <a href="#"
                               class="nx-tabs__link"
                               data-nx-tabs="content-load"
                               data-load-id="123"
                               data-nx-url="test-2">
                                <span> Аксессуары</span></a>
                        </div>
                        
                        <div class="swiper-slide nx-tabs__item">
                            <a href="#"
                               class="nx-tabs__link"
                               data-nx-tabs="content-load"
                               data-load-id="123"
                               data-nx-url="test-3">
                                <span>Новая коллекция</span></a>
                        </div>
                    </div>
                </div>
                
                <div class="nx-section__item">
                    <div class="nx-list-slider">
                        <div class="nx-list-slider__slider nx-listing" data-list-slider>
                            <div class="swiper-wrapper" data-ajax-content>
                                <? for ($i = 0; $i < 5; $i++): ?>
                                    <div class="swiper-slide nx-listing__item">
                                        <a href="#" class="nx-listing__link">
                                            <div class="nx-listing__head">
                                                <img src="https://via.placeholder.com/550x550"
                                                     alt=""
                                                     class="nx-listing__img">
                                                
                                                <div class="nx-tags">
                                                    <div class="nx-tags__item">Новинка!</div>
                                                </div>
                                            </div>
                                            
                                            <div class="nx-listing__body">
                                                <span class="nx-listing__title">Название элемента</span>
                                            </div>
                                        </a>
                                    </div>
                                <? endfor; ?>
                            </div>

                            <div class="nx-list-slider__prev" data-list-slider-prev>
                                <svg class="nx-list-slider__prev-icon"><use xlink:href="#icon-arrow"></use></svg>
                            </div>
                            
                            <div class="nx-list-slider__next nx-list-slider__next" data-list-slider-next>
                                <svg class="nx-list-slider__prev-icon"><use xlink:href="#icon-arrow"></use></svg>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="nx-actions nx-actions_center">
                    <div class="nx-actions__item">
                        <a href="" class="btn btn_br">Смотреть весь каталог</a>
                    </div>
                </div>
            </section>

            <section class="nx-section container">
                <div class="content-narrow-med">
                    <div class="nx-section__box">
                        <div class="nx-section__head">
                            <h2 class="nx-section__title">О компании</h2>
                            
                            <a href="#" class="nx-section__link link">
                                <span>Узнать больше</span>
                            </a>
                            
                            <div class="nx-section__text  text text_dark-fade">Крупнейший интернет-магазин модной
                                одежды, обуви, аксессуаров,
                                косметических
                                средств, существующий уже 15 лет! Из года в год мы продолжаем развиваться, расширять
                                географию
                                присутствия и
                                улучшать качество обслуживания, чтобы радовать Вас каждый день!
                            </div>
                        </div>
                    </div>
                    
                    <div class="nx-section__box">
                        <h4 class="page-title">Wildberries в цифрах</h4>
                        
                        <div class="nx-section nx-section_inner nx-section_bg nx-section_c-dark">
                            <div class="nx-advans nx-advans_light flex-row">
                                <? for ($i = 0; $i < 3; $i++): ?>
                                    <div class="nx-advans__item flex-col md-8 sm-12">
                                        <div class="nx-advans__num factoid" data-anim-num="35000">35 000</div>
                                        
                                        <div class="nx-advans__text">Популярных и проверенных брендов на сайте</div>
                                    </div>
                                <? endfor; ?>
                            </div>
                        </div>
                    </div>
                    
                    <div class="nx-section__box">
                        <h4 class="page-title">Wildberries в цифрах</h4>
                        
                        <div class="nx-section nx-section_inner nx-section_bg nx-section_c-dark">
                            <div class="nx-advans nx-advans_light flex-row">
                                <? for ($i = 0; $i < 3; $i++): ?>
                                    <div class="nx-advans__item flex-col md-8 sm-12">
                                        <div class="nx-advans__icon" style="background-image: url('https://via.placeholder.com/140x140');"></div>
                                        
                                        <div class="nx-advans__text">Популярных и проверенных брендов на сайте</div>
                                    </div>
                                <? endfor; ?>
                            </div>
                        </div>
                    </div>

                    <h4 class="nx-section__title">Крупнейший интернет-магазин в России</h4>
                    
                    <div class="nx-gal-slider not-visible" data-gal>
                        <div class="nx-gal-slider__main" data-gal-main>
                            <div class="swiper-wrapper nx-gal-slider__main-wrap">
                                <!--Video-->
                                <div class="swiper-slide nx-gal-slider__main-slide nx-video" data-video-content>
                                    <a href="#"
                                       class="nx-video__link"
                                       data-video-load="mgcQDHpPVLo"
                                       style="background-image: url('https://via.placeholder.com/1980x1200')">
                                        <svg class="nx-video__icon"><use xlink:href="#icon-play"></use></svg>
                                    </a>
                                </div>
                                <? for ($i = 0; $i < 9; $i++): ?>
                                    <a href="https://i.imgur.com/fsyrScY.jpg"
                                       class="swiper-slide nx-gal-slider__main-slide"
                                       data-fslightbox="gal">
                                        <img src="https://via.placeholder.com/1980x1200"
                                             alt=""
                                             class="nx-gal-slider__main-img">
                                    </a>
                                <? endfor; ?>
                            </div>

                            <div class="nx-gal-slider__prev" data-gal-prev>
                                <svg class="nx-gal-slider__prev-icon"><use xlink:href="#icon-arrow"></use></svg>
                            </div>
                            
                            <div class="nx-gal-slider__next nx-gal-slider__next" data-gal-next>
                                <svg class="nx-gal-slider__next-icon"><use xlink:href="#icon-arrow"></use></svg>
                            </div>
                        </div>

                        <div class="nx-gal-slider__thumbs" data-gal-thumb>
                            <div class="swiper-wrapper nx-gal-slider__thumbs-wrap">
                                <? for ($i = 0; $i < 10; $i++): ?>
                                    <div class="swiper-slide nx-gal-slider__thumbs-slide"
                                         data-gal-thumb-slide
                                         style="background-image: url('https://via.placeholder.com/260x150');"></div>
                                <? endfor; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <div class="nx-section container">
                <div class="nx-section nx-section_bg nx-section_inner nx-section_c-dark">
                    <div class="nx-section__box">
                        <div class="nx-section__head content-narrow">
                            <h2 class="nx-section__title">О компании</h2>
                            
                            <div class="nx-section__text  text">Крупнейший интернет-магазин модной одежды, обуви,
                                аксессуаров,
                                косметических средств, существующий уже 15 лет! Из года в год мы продолжаем развиваться,
                                расширять географию присутствия и улучшать качество обслуживания, чтобы радовать Вас
                                каждый
                                день!
                            </div>
                        </div>
                        
                        <a href="" class="btn btn_lt">Узнать больше</a>
                    </div>
                    
                    <div class="nx-section__box">
                        <div class="line line_lt"></div>
                    </div>
                    
                    <div class="nx-section__box">
                        <h4 class="nx-section__title">Wildberries в цифрах</h4>
                        
                        <div class="nx-advans nx-advans_light flex-row">
                            <? for ($i = 0; $i < 3; $i++): ?>
                                <div class="nx-advans__item flex-col md-8 sm-12">
                                    <div class="nx-advans__num factoid" data-anim-num="300000">300 000</div>
                                    
                                    <div class="nx-advans__text">Популярных и проверенных брендов на сайте</div>
                                </div>
                            <? endfor; ?>
                        </div>
                    </div>
                </div>
            </div>

            <section class="nx-section container">
                <div class="nx-section__head content-narrow-med">
                    <h2 class="nx-section__title">Наши партнеры</h2>
                    
                    <div class="nx-section__text text text_dark-fade">Крупнейший интернет-магазин модной одежды, обуви,
                        аксессуаров,
                        косметических средств, существующий уже 15 лет! Из года в год мы продолжаем развиваться,
                        расширять
                        географию присутствия и улучшать качество обслуживания, чтобы радовать Вас каждый день!
                    </div>
                </div>
                
                <div class="nx-partners flex-row">
                    <? for ($i = 0; $i < 8; $i++): ?>
                        <div class="nx-partners__item flex-col md-6 sm-6 xs-12">
                            <a href="#" class="nx-partners__link" target="_blank">
                                <img src="https://via.placeholder.com/275x130" alt="" class="nx-partners__img">
                            </a>
                        </div>
                    <? endfor; ?>
                </div>
            </section>

            <section class="nx-section container">
                <div class="nx-section__head content-narrow-med">
                    <h2 class="nx-section__title">Наши партнеры</h2>
                    
                    <div class="nx-section__text  text text_dark-fade">Крупнейший интернет-магазин модной одежды, обуви,
                        аксессуаров,
                        косметических средств, существующий уже 15 лет! Из года в год мы продолжаем развиваться,
                        расширять
                        географию присутствия и улучшать качество обслуживания, чтобы радовать Вас каждый день!
                    </div>
                </div>
                <div class="nx-list-slider nx-list-slider_center">
                    <div class="nx-list-slider__slider nx-partners" data-list-slider>
                        <div class="swiper-wrapper">
                            <? for ($i = 0; $i < 5; $i++): ?>
                                <div class="swiper-slide nx-partners__item">
                                    <a href="#"
                                       class="nx-partners__link"
                                       target="_blank">
                                        <img src="https://via.placeholder.com/275x130"
                                             alt=""
                                             class="nx-partners__img">
                                    </a>
                                </div>
                            <? endfor; ?>
                        </div>

                        <div class="nx-list-slider__prev" data-list-slider-prev>
                            <svg class="nx-list-slider__prev-icon"><use xlink:href="#icon-arrow"></use></svg>
                        </div>
                        
                        <div class="nx-list-slider__next nx-list-slider__next" data-list-slider-next>
                            <svg class="nx-list-slider__prev-icon"><use xlink:href="#icon-arrow"></use></svg>
                        </div>
                    </div>
                </div>
            </section>

            <section class="nx-section container">
                <div class="nx-section__head content-narrow-med">
                    <h2 class="nx-section__title">История компании</h2>
                    
                    <div class="nx-section__text  text text_dark-fade">Крупнейший интернет-магазин модной одежды, обуви,
                        аксессуаров, косметических средств, существующий уже 15 лет! Из года в год мы продолжаем
                        развиваться, расширять географию присутствия и улучшать качество обслуживания, чтобы радовать
                        Вас каждый день!
                    </div>
                </div>

                <div class="nx-history" data-gal data-history="history">
                    <div class="nx-history__main" data-gal-main>
                        <div class="swiper-wrapper nx-history__main-wrap">
                            <? for ($i = 0; $i < 4; $i++): ?>
                                <div class="swiper-slide nx-history__main-slide">
                                    <div class="flex-row flex-row_ai-center nx-history__row">
                                        <div class="flex-col md-12 sm-12">
                                            <div class="nx-common-slider">
                                                <div class="nx-common-slider__slider" data-common-slider>
                                                    <div class="swiper-wrapper">
                                                        <div class="swiper-slide nx-video" data-video-content>
                                                            <a href="#" class="nx-video__link"
                                                               style="background-image: url('https://via.placeholder.com/1160x650')"
                                                               data-video-load="mgcQDHpPVLo">
                                                                <svg class="nx-video__icon"><use xlink:href="#icon-play"></use></svg>
                                                            </a>
                                                            <img src="https://via.placeholder.com/1160x650">
                                                        </div>
                                                        <? for ($j = 0; $j < 2; $j++): ?>
                                                            <div class="swiper-slide">
                                                                <img src="https://via.placeholder.com/1160x650">
                                                            </div>
                                                        <? endfor; ?>
                                                    </div>
                                                    
                                                    <div class="nx-common-slider__prev" data-common-slider-prev>
                                                        <svg class="nx-common-slider__prev-icon"><use xlink:href="#icon-arrow"></use></svg>
                                                    </div>
                                                    
                                                    <div class="nx-common-slider__next" data-common-slider-next>
                                                        <svg class="nx-common-slider__next-icon"><use xlink:href="#icon-arrow"></use></svg>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="flex-col md-12 sm-12">
                                            <div class="nx-history__body">
                                                <div class="nx-history__date">2009</div>
                                                
                                                <div class="nx-history__title">Торжественное открытие предприятия</div>
                                                
                                                <div class="nx-history__text text text_dark-fade">
                                                    Крупнейший интернет-магазин модной одежды, обуви, аксессуаров,
                                                    косметических
                                                    средств, существующий уже 15 лет! Из года в год мы продолжаем
                                                    развиваться,
                                                    расширять географию присутствия и улучшать качество обслуживания,
                                                    чтобы радовать
                                                    Вас каждый день!
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <? endfor; ?>
                        </div>
                    </div>
                    
                    <div class="nx-history__prev" data-gal-prev>
                        <svg class="nx-history__prev-icon"><use xlink:href="#icon-arrow"></use></svg>
                    </div>
                    
                    <div class="nx-history__next nx-history__next" data-gal-next>
                        <svg class="nx-history__next-icon"><use xlink:href="#icon-arrow"></use></svg>
                    </div>
                    
                    <div class="nx-history__thumbs" data-gal-thumb>
                        <div class="swiper-wrapper nx-history__thumbs-wrap">
                            <? for ($x = 0; $x < 4; $x++): ?>
                                <div class="swiper-slide nx-history__thumbs-slide" data-gal-thumb-slide>
                                    <div class="nx-history__item">
                                        <div class="nx-history__year">2020</div>
                                        
                                        <div class="nx-history__dot"></div>
                                    </div>
                                </div>
                            <? endfor; ?>
                        </div>
                    </div>
                </div>
            </section>

            <section class="nx-section container">
                <h2 class="nx-section__title">Нам доверяют</h2>
                
                <div class="nx-reverse">
                    <? for ($i = 0; $i < 4; $i++): ?>
                        <div class="flex-row nx-reverse__row">
                            <div class="flex-col md-12 nx-reverse__item">
                                <div class="nx-common-slider">
                                    <div class="nx-common-slider__slider" data-common-slider>
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide nx-video" data-video-content>
                                                <a href="#"
                                                   class="nx-video__link"
                                                   style="background-image: url('https://via.placeholder.com/1160x650')"
                                                   data-video-load="mgcQDHpPVLo">
                                                    <svg class="nx-video__icon"><use xlink:href="#icon-play"></use></svg>
                                                </a>
                                                
                                                <img src="https://via.placeholder.com/1160x650">
                                            </div>
                                            
                                            <? for ($j = 0; $j < 2; $j++): ?>
                                                <div class="swiper-slide">
                                                    <img src="https://via.placeholder.com/1160x650">
                                                </div>
                                            <? endfor; ?>
                                        </div>
                                        
                                        <div class="nx-common-slider__prev" data-common-slider-prev>
                                            <svg class="nx-common-slider__prev-icon"><use xlink:href="#icon-arrow"></use></svg>
                                        </div>
                                        
                                        <div class="nx-common-slider__next" data-common-slider-next>
                                            <svg class="nx-common-slider__next-icon"><use xlink:href="#icon-arrow"></use></svg>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="flex-col md-12 nx-reverse__item">
                                <div class="nx-advans">
                                    <div class="nx-advans__item">
                                        <div class="nx-advans__icon" style="background-image: url('https://via.placeholder.com/140x140');"></div>
                                        
                                        <h5 class="nx-advans__title">Филиалы в 78 регионах</h5>
                                        
                                        <div class="nx-advans__text">
                                            Крупнейший интернет-магазин модной одежды, обуви, аксессуаров, косметических
                                            средств, существующий уже 15 лет! Из года в год мы продолжаем развиваться,
                                            расширять географию присутствия и улучшать качество обслуживания, чтобы
                                            радовать Вас каждый день!
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <? endfor; ?>
                </div>
            </section>

            <section class="nx-section container">
                <h4 class="nx-section__title">Крупнейший интернет-магазин в России</h4>
                
                <div class="nx-gal-slider not-visible" data-gal>
                    <div class="nx-gal-slider__main" data-gal-main>
                        <div class="swiper-wrapper nx-gal-slider__main-wrap">
                            <!--Video-->
                            <div class="swiper-slide nx-gal-slider__main-slide nx-video" data-video-content>
                                <a href="#"
                                   class="nx-video__link"
                                   data-video-load="mgcQDHpPVLo"
                                   style="background-image: url('https://via.placeholder.com/1980x1200')">
                                    <svg class="nx-video__icon"><use xlink:href="#icon-play"></use></svg>
                                </a>
                            </div>
                            
                            <? for ($i = 0; $i < 10; $i++): ?>
                                <a href="https://via.placeholder.com/1920x1080"
                                   class="swiper-slide nx-gal-slider__main-slide"
                                   data-fancybox="gal">
                                    <img src="https://via.placeholder.com/1980x1200"
                                         alt=""
                                         class="nx-gal-slider__main-img">
                                </a>
                            <? endfor; ?>
                        </div>

                        <div class="nx-gal-slider__prev" data-gal-prev>
                            <svg class="nx-gal-slider__prev-icon"><use xlink:href="#icon-arrow"></use></svg>
                        </div>
                        
                        <div class="nx-gal-slider__next nx-gal-slider__next" data-gal-next>
                            <svg class="nx-gal-slider__next-icon"><use xlink:href="#icon-arrow"></use></svg>
                        </div>
                    </div>

                    <div class="nx-gal-slider__thumbs" data-gal-thumb>
                        <div class="swiper-wrapper nx-gal-slider__thumbs-wrap">
                            <? for ($i = 0; $i < 10; $i++): ?>
                                <div class="swiper-slide nx-gal-slider__thumbs-slide"
                                     data-gal-thumb-slide
                                     style="background-image: url('https://via.placeholder.com/260x150');"></div>
                            <? endfor; ?>
                        </div>
                    </div>
                </div>
            </section>

            <section class="nx-section container">
                <h4 class="nx-section__title">Крупнейший интернет-магазин в России </h4>
                
                <div class="nx-common-slider">
                    <div class="nx-common-slider__slider" data-common-slider>
                        <div class="swiper-wrapper">
                            <div class="swiper-slide nx-video" data-video-content>
                                <a href="#"
                                   class="nx-video__link"
                                   style="background-image: url('https://via.placeholder.com/1160x650')"
                                   data-video-load="mgcQDHpPVLo">
                                    <svg class="nx-video__icon"><use xlink:href="#icon-play"></use></svg>
                                </a>
                                
                                <img src="https://via.placeholder.com/1160x650">
                            </div>
                            <? for ($j = 0; $j < 2; $j++): ?>
                                <div class="swiper-slide">
                                    <img src="https://via.placeholder.com/1160x650">
                                </div>
                            <? endfor; ?>
                        </div>
                        
                        <div class="nx-common-slider__prev" data-common-slider-prev>
                            <svg class="nx-common-slider__prev-icon"><use xlink:href="#icon-arrow"></use></svg>
                        </div>
                        
                        <div class="nx-common-slider__next" data-common-slider-next>
                            <svg class="nx-common-slider__next-icon"><use xlink:href="#icon-arrow"></use></svg>
                        </div>
                    </div>
                </div>
            </section>

            <div class="nx-section container">
                <div class="nx-section__box">
                    <form action="/request/" class="nx-form-subscribe">
                        <div class="nx-form-subscribe__content content-narrow-med nx-section__item">
                            <h3 class="nx-form-feedback__title">Подпишитесь на акции и новости</h3>
                            
                            <div class="nx-form-feedback__text text">Подпишись на рассылку и получай персональные скидки
                                которых нет на сайте и в рекламе. Рассылка осуществляется раз в неделю, не отправляем
                                спам, только самое важное
                            </div>
                        </div>
                        
                        <div class="nx-form-subscribe__body nx-actions">
                            <div class="form-group nx-actions__item" data-form-group>
                                <label class="nx-dynamic-label nx-dynamic-label_tr nx-dynamic-label_lt" data-dynamic-label>
                                    <input type="text" 
                                           class="nx-dynamic-label__input nx-form-element"
                                           name="email" 
                                           data-dynamic-inp>
                                    <span class="nx-dynamic-label__text">Ваш E-mail</span>
                                </label>
                            </div>
                            
                            <div class="nx-actions__item nx-actions__item_btn">
                                <button type="submit"
                                        class="btn btn_lt  btn_d-block-xs"
                                        data-send-request="question">Подписаться на скидки</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="nx-section__box">
                    <form action="/request/" class="nx-form-subscribe nx-form-subscribe_row">
                        <div class="nx-form-subscribe__content">
                            <h3 class="nx-form-feedback__title">Подпишитесь на акции и новости</h3>
                            
                            <div class="nx-form-feedback__text text">Подпишись на рассылку и получай персональные скидки
                                которых нет на сайте и в рекламе. Рассылка осуществляется раз в неделю, не отправляем
                                спам, только самое важное
                            </div>
                        </div>
                        
                        <div class="nx-form-subscribe__body">
                            <div class="form-group" data-form-group>
                                <label class="nx-dynamic-label nx-dynamic-label_tr nx-dynamic-label_lt" data-dynamic-label>
                                    <input type="text"
                                           class="nx-dynamic-label__input nx-form-element"
                                           name="email"
                                           data-dynamic-inp>
                                    
                                    <span class="nx-dynamic-label__text">Ваш E-mail</span>
                                </label>
                            </div>
                            
                            <div class="nx-actions nx-actions_single">
                                <div class="nx-actions__item nx-actions__item_btn">
                                    <button type="submit"
                                            class="btn btn_lt btn_d-block-xs"
                                            data-send-request="question">Подписаться на скидки</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <section class="nx-section container">
                <div class="nx-section__head">
                    <h2 class="nx-section__title">Новости</h2>
                    
                    <a href="" class="nx-section__link link"><span>Смотреть все</span></a>
                </div>

                <div class="nx-section__item">
                    <div class="flex-row nx-listing nx-listing_news">
                        <? for ($i = 0; $i < 4; $i++): ?>
                            <div class="flex-col sm-6 nx-listing__item">
                                <a href="#" class="nx-listing__link">
                                    <div class="nx-listing__head">
                                        <img src="https://via.placeholder.com/600x340"
                                             alt=""
                                             class="nx-listing__img">
                                    </div>
                                    
                                    <div class="nx-listing__body">
                                        <div class="nx-listing__date">29.01.20</div>
                                        
                                        <span class="nx-listing__title">Распродажа зимней коллекции 2020 в самом разгаре </span>
                                        
                                        <div class="nx-listing__text">
                                            Успейте к главной распродаже года, скидки до 90% на все группы товаров.
                                            Только в этом феврале...
                                        </div>
                                    </div>
                                </a>
                            </div>
                        <? endfor; ?>
                    </div>
                </div>
                
                <div class="nx-actions nx-actions_center">
                    <div class="nx-actions__item">
                        <a href="" class="btn btn_br">Смотреть все</a>
                    </div>
                </div>
            </section>

            <div class="nx-section container">
                <form action="/request/" class="nx-form nx-form_row">
                    <div class="nx-form__content">
                        <h3 class="nx-form__title">У вас остались вопросы? С радостью ответим!</h3>
                        
                        <div class="nx-form__text text text_dark-fade">Заполните поля и мы позвоним вам для ответа на
                            вопрос. Напишем на почту если не дозвонимся
                        </div>
                    </div>
                    
                    <div class="nx-form__body">
                        <div class="form-group" data-form-group>
                            <label class="nx-dynamic-label" data-dynamic-label>
                                <input type="text"
                                       class="nx-dynamic-label__input nx-form-element"
                                       name="name"
                                       data-dynamic-inp>
                                
                                <span class="nx-dynamic-label__text">Имя</span>
                            </label>
                        </div>
                        
                        <div class="form-group" data-form-group>
                            <label class="nx-dynamic-label" data-dynamic-label>
                                <input type="text"
                                       class="nx-dynamic-label__input nx-form-element"
                                       name="phone"
                                       data-dynamic-inp
                                       data-phone-mask>
                                
                                <span class="nx-dynamic-label__text">Телефон</span>
                            </label>
                        </div>
                        
                        <div class="form-group" data-form-group>
                            <label class="nx-dynamic-label" data-dynamic-label>
                                <input type="text"
                                       class="nx-dynamic-label__input nx-form-element"
                                       name="email"
                                       data-dynamic-inp>
                                
                                <span class="nx-dynamic-label__text">E-mail</span>
                            </label>
                        </div>
                        
                        <div class="form-group" data-form-group>
                            <label class="nx-dynamic-label" data-dynamic-label>
                                <textarea class="nx-dynamic-label__input nx-form-element"
                                          name="comment"
                                          rows="1"
                                          data-dynamic-inp
                                          data-autosize-textarea></textarea>
                                
                                <span class="nx-dynamic-label__text">Вопрос</span>
                            </label>
                        </div>
                        
                        <div class="nx-actions nx-actions_note">
                            <div class="nx-actions__item nx-actions__item_btn">
                                <button type="submit"
                                        class="btn btn_d-block-xs"
                                        data-send-request="question">Отправить</button>
                            </div>
                            
                            <div class="nx-actions__item">
                                <div class="note">Нажимая на кнопку “Отправить ” вы даете согласие на обработку
                                    <a href="/politics/" class="link">
                                        <span>персональных данных</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <? require_once '../templates/_blocks/footer.php'; ?>
</div>

<script src="/assets/app.min.js"></script>
</body>
</html>
