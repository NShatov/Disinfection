<!DOCTYPE HTML>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta name="author" content="Контент">
    <meta name="keywords" content="keywords">
    <meta name="description" content="description">

    <title>Каталог список</title>

    <link rel="icon" type="image/png" href="/favicon.png">
    <link rel="stylesheet" href="/assets/vendor.css">
    <link rel="stylesheet" href="/assets/app.min.css">

    <meta property="og:title" content="title"/>
    <meta property="og:description" content="description"/>
    <meta property="og:image" content="favicon.png">

</head>
<body>

<? require_once '../templates/_blocks/header.php'; ?>
<? require_once '../templates/_blocks/cart_informer.php'; ?>

<div class="wrapper">
    <div class="wrapper__content">
        <div class="nx-section nx-section_page container">
            <div class="nx-breadcrumbs">
                <div class="nx-breadcrumbs__item">
                    <a href="#" class="nx-breadcrumbs__link">
                        <span>Каталог</span>
                    </a>
                    
                    <i class="nx-breadcrumbs__arrow mdi mdi-chevron-right"></i>
                </div>
            </div>

            <div class="nx-section__head content-narrow-med">
                <h1 class="nx-section__title">Дачные дома</h1>
            </div>

            <a href="#" class="btn nx-filter-btn" data-filter-toggle>Фильтр</a>

            <div class="flex-row" data-affix-parent>
                <div class="flex-col md-6" data-affix-wrap>
                    <div class="nx-affix" data-affix-block>
                        <div class="bg-fade bg-fade_fixed" data-filter-fade></div>
                        
                        <div class="nx-filter" data-filter>
                            <a href="#"
                               class="nx-filter__close"
                               data-filter-close>
                                <svg class="icon"><use xlink:href="#icon-close"></use></svg>
                            </a>
                            
                            <div class="nx-filter__nav">
                                <div class="nx-filter-nav">
                                    <div class="nx-filter-nav__item">
                                        <a href="#" class="nx-filter-nav__link">
                                            <i class="link-icon mdi mdi-arrow-left"></i>
                                            <span>Каталог</span>
                                        </a>
                                    </div>
                                    
                                    <div class="nx-filter-nav__item">
                                        <a href="#" class="nx-filter-nav__link">
                                            <span>Ссылка</span>
                                        </a>
                                    </div>
                                    
                                    <div class="nx-filter-nav__item">
                                        <a href="#" class="nx-filter-nav__link">
                                            <span>Ссылка</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            
                            <form action="" class="nx-filter__form">
                                <div class="nx-filter__section">
                                    <a href="#collapse-item-1"
                                       class="link collapsed"
                                       data-toggle="collapse">
                                        <i class="link-icon mdi mdi-chevron-down"></i>
                                        
                                        <span>Чекбоксы</span>
                                    </a>
                                    
                                    <div class="collapse" id="collapse-item-1">
                                        <div class="nx-filter__scroll" data-custom-scroll>
                                            <? for ($i = 0; $i < 12; $i++): ?>
                                                <div class="nx-filter__item">
                                                    <label class="checkbox">
                                                        <input type="checkbox"
                                                               class="checkbox__input"
                                                               name=""
                                                               value="">
                                                        
                                                        <span class="checkbox__text">С гаражом</span>
                                                    </label>
                                                </div>
                                            <? endfor; ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="nx-filter__section">
                                    <a href="#collapse-item-2"
                                       class="link collapsed"
                                       data-toggle="collapse">
                                        <i class="link-icon mdi mdi-chevron-down"></i>
                                        
                                        <span>Радиокнопки</span>
                                    </a>
                                    
                                    <div class="collapse in" id="collapse-item-2">
                                        <div class="nx-filter__scroll" data-custom-scroll>
                                            
                                            <? for ($i = 0; $i < 12; $i++): ?>
                                                <div class="nx-filter__item">
                                                    <label class="checkbox checkbox_radio">
                                                        <input type="radio"
                                                               class="checkbox__input"
                                                               name=""
                                                               value="">
                                                        
                                                        <span class="checkbox__text">5х6</span>
                                                    </label>
                                                </div>
                                            <? endfor; ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="nx-filter__section">
                                    <a href="#collapse-item-3"
                                       class="link collapsed"
                                       data-toggle="collapse">
                                        <i class="link-icon mdi mdi-chevron-down"></i>
                                        
                                        <span>Ползунок</span>
                                    </a>
                                    
                                    <div id="collapse-item-3" class="nx-filter__scroll collapse in">
                                        <div class="range-slider-box" data-rs>
                                            <div class="range-slider-box__inputs flex-row flex-row_form">
                                                <div class="rs-box__col flex-col xs-12">
                                                    <input type="text"
                                                           class="nx-form-element"
                                                           name="from"
                                                           value="100"
                                                           data-num-only
                                                           data-rs-inp=""
                                                           data-rs-min=""
                                                           placeholder="от">
                                                </div>
                                                
                                                <div class="rs-box__col flex-col xs-12">
                                                    <input type="text"
                                                           class="nx-form-element"
                                                           name="to"
                                                           value="100"
                                                           data-num-only
                                                           data-rs-inp=""
                                                           data-rs-max=""
                                                           placeholder="до">
                                                </div>
                                            </div>
                                            
                                            <input type="text"
                                                   data-rs-main-inp=""
                                                   data-from="100"
                                                   data-to="1000"
                                                   data-min="100"
                                                   data-max="1000">
                                        </div>
                                    </div>
                                </div>

                                <div class="nx-filter__actions">
                                    <div class="nx-filter__actions-item">
                                        <button type="submit" class="btn btn_d-block">Применить</button>
                                    </div>
                                    
                                    <div class="nx-filter__actions-item">
                                        <a href="#" class="link link_red">
                                            <i class="link-icon mdi mdi-close"></i>
                                            <span>Сбросить фильтр</span>
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="flex-col md-18">
                    <div class="nx-filter-line">
                        <div class="nx-filter-line__item">
                            <div class="nx-filter-line__title">Сортировать по:</div>
                            
                            <div class="nx-filter-line__select">
                                <select name="" id="" data-custom-select data-placeholder="Цене">
                                    <option value="1">Цене</option>
                                    
                                    <option value="1">Порядку</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="nx-filter-line__item">
                            <div class="nx-filter-line__title">Выводить по:</div>
                            
                            <div class="nx-filter-line__values">
                                <a href="#" class="nx-filter-line__link link active">
                                    <span>12</span>
                                </a>
                                
                                <a href="#" class="nx-filter-line__link link">
                                    <span>24</span>
                                </a>
                                
                                <a href="#" class="nx-filter-line__link link">
                                    <span>48</span>
                                </a>
                                
                                <a href="#" class="nx-filter-line__link link">
                                    <span>96</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="nx-listing nx-listing_prod-row">
                        <? for ($i = 0; $i < 2; $i++): ?>
                            <div class="nx-listing__item"
                                 data-product-weight="150"
                                 data-product="111">
                                <div class="nx-listing__icons">
                                    <a href="#" class="nx-listing__icon-btn">
                                        <svg class="nx-listing__icon"><use xlink:href="#icon-like"></use></svg>
                                    </a>
                                    
                                    <a href="#" class="nx-listing__icon-btn">
                                        <svg class="nx-listing__icon"><use xlink:href="#icon-promote"></use></svg>
                                    </a>
                                </div>
                                
                                <a href="#" class="nx-listing__link">
                                    <div class="nx-listing__head">
                                        <img src="https://via.placeholder.com/550x550"
                                             alt=""
                                             class="nx-listing__img">
                                        
                                        <div class="nx-tags nx-tags_small">
                                            <? for ($j = 0; $j < 3; $j++): ?>
                                                <div class="nx-tags__item">Лучшая цена</div>
                                            <? endfor; ?>
                                        </div>
                                    </div>
                                    
                                    <div class="nx-listing__body">
                                        <div class="nx-listing__note">
                                            <div class="nx-listing__articul">Артикул:
                                                <span>10073940</span>
                                            </div>
                                            
                                            <div class="nx-listing__availible">В наличии</div>
                                        </div>
                                        
                                        <span class="nx-listing__title"
                                              data-prod-title="Тестовый товар с длинным названием">Тестовый товар с длинным названием</span>
                                        
                                        <div class="nx-listing__note-list">
                                            <? for ($y = 0; $y < 2; $y++): ?>
                                                <div class="nx-listing__note">
                                                    <div class="nx-listing__articul">Материал:
                                                        <span> Клееный брус</span>
                                                    </div>
                                                </div>
                                            <? endfor; ?>
                                        </div>
                                    </div>
                                </a>
                                
                                <div class="nx-listing__actions">
                                    <div class="nx-prices-item nx-prices-item_lg">
                                        <div class="nx-prices-item__old">10 000 &#8381;</div>
                                        
                                        <div class="nx-prices-item__current" data-prod-price="1000">1 000 &#8381;</div>
                                    </div>
                                    
                                    <a href="#" class="nx-listing__delivery">
                                        <span>Рассчитать стоимость доставки в Псков</span>
                                    </a>
                                    
                                    <div class="nx-listing__actions-row">
                                        <div class="nx-listing__actions-item">
                                            <div class="nx-counter" data-counter>
                                                <a href="#"
                                                   class="nx-counter-btn nx-counter-btn_minus"
                                                   data-counter-btn="minus"></a>
                                                
                                                <input name="products[quantity]"
                                                       class="nx-counter__input"
                                                       type="text"
                                                       value="1"
                                                       data-counter-inp data-max="999"
                                                       data-prod-cnt>
                                                
                                                <a href="#"
                                                   class="nx-counter-btn nx-counter-btn_plus"
                                                   data-counter-btn="plus"></a>
                                            </div>
                                        </div>
                                        
                                        <div class="nx-listing__actions-item">
                                            <a href=""
                                               class="btn btn_br nx-listing__buy"
                                               data-prod-add>
                                                <span>В корзину</span>
                                                
                                                <svg class="icon"><use xlink:href="#icon-cart"></use></svg>
                                            </a>
                                        </div>
                                    </div>
                                    <a href="#" class="link nx-listing__click">
                                        <svg class="icon"><use xlink:href="#icon-one-click"></use></svg>
                                        
                                        <span>Купить в один клик</span>
                                    </a>
                                </div>
                            </div>
                        <? endfor; ?>
                    </div>

                    <div class="nx-pagination">
                        <a href="#" class="nx-pagination__arrow nx-pagination__arrow_prev disabled">
                            <svg class="nx-pagination__arrow-icon"><use xlink:href="#icon-arrow"></use></svg>
                        </a>

                        <div class="nx-pagination__item active">
                            <a href="#" class="nx-pagination__link link link_invert">
                                <span>1</span>
                            </a>
                        </div>
                        <div class="nx-pagination__item">
                            <a href="#" class="nx-pagination__link link link_invert">
                                <span>2</span>
                            </a>
                        </div>

                        <div class="nx-pagination__item nx-pagination__item_dots">
                            <div class="nx-pagination__dots">...</div>
                        </div>

                        <div class="nx-pagination__item">
                            <a href="#" class="nx-pagination__link link link_invert">
                                <span>9</span>
                            </a>
                        </div>
                        <div class="nx-pagination__item">
                            <a href="#" class="nx-pagination__link link link_invert">
                                <span>10</span>
                            </a>
                        </div>

                        <a href="#" class="nx-pagination__arrow nx-pagination__arrow_next">
                            <svg class="nx-pagination__arrow-icon"><use xlink:href="#icon-arrow"></use></svg>
                        </a>
                    </div>
                </div>
            </div>
            
            <div data-stop-affix></div>
        </div>

    </div>
    <? require_once '../templates/_blocks/footer.php'; ?>
</div>

<script src="/assets/app.min.js"></script>
</body>
</html>
