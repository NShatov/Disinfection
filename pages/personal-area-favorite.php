<!DOCTYPE HTML>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <meta name="author" content="Контент">
    <meta name="keywords" content="keywords">
    <meta name="description" content="description">
    
    <title>Главная</title>
    
    <link rel="icon" type="image/png" href="/favicon.png">
    <link rel="stylesheet" href="/assets/vendor.css">
    <link rel="stylesheet" href="/assets/app.min.css">
    
    <meta property="og:title" content="title"/>
    <meta property="og:description" content="description"/>
    <meta property="og:image" content="favicon.png">

</head>
<body>

<? require_once '../templates/_blocks/header.php'; ?>

<div class="wrapper">
    <div class="wrapper__content">
        <div class="nx-section nx-section_page">
            <div class="container">
                <div class="nx-section__element-inner">
                    <h4 class="nx-section__title">Личный кабинет</h4>
                    
                    <div class="nx-personal-actions">
                        <div class="nx-toggle-button">
                            <input type="checkbox"
                                   name="toggle"
                                   id="toggle-button"
                                   class="nx-toggle-button__input"
                                   data-mailing-toggle
                                   data-url="">
                            
                            <label for="toggle-button" class="nx-toggle-button__text">Подписка на рассылку</label>
                        </div>
                        
                        <a href="" class="link link_exit">
                            <svg class="link-icon"><use xlink:href="#icon-exit"></use></svg>
                            
                            <span>Выйти</span>
                        </a>
                    </div>
                </div>
                
                <div class="nx-tabs" data-tabs-slider>
                    <div class="swiper-wrapper nx-tabs__wrap">
                        <div class="swiper-slide nx-tabs__item">
                            <a href="#"
                               class="nx-tabs__link">
                                <span>Мои заказы</span>
                            </a>
                        </div>
                        
                        <div class="swiper-slide nx-tabs__item is-active">
                            <a href="#"
                               class="nx-tabs__link">
                                <span>Избранное</span>
                            </a>
                        </div>
                        
                        <div class="swiper-slide nx-tabs__item">
                            <a href="#"
                               class="nx-tabs__link">
                                <span>Личные данные</span>
                            </a>
                        </div>
                    </div>
                </div>
    
                <div class="nx-listing nx-listing_prod flex-row">
                    <? for ($i = 0; $i < 12; $i++): ?>
                        <div class="nx-listing__item flex-col md-6 sm-8"
                             data-prod="<?echo  $i ?>"
                             data-prod-weight="1000">
                            <div class="nx-listing__icons">
                                <a href="#" class="nx-listing__icon-btn">
                                    <svg class="nx-listing__icon"><use xlink:href="#icon-like"></use></svg>
                                </a>
                    
                                <a href="#" class="nx-listing__icon-btn nx-compare-link nx-tooltip nx-tooltip_top">
                                    <div class="nx-tooltip__item">
                                        <div class="nx-tooltip__text" data-prod-title="Выберите несколько товаров, чтобы сравнить их">Выберите несколько товаров, чтобы сравнить их</div>
                            
                                        <div class="nx-tooltip__arrow"></div>
                                    </div>
                        
                                    <svg class="nx-listing__icon"><use xlink:href="#icon-promote"></use></svg>
                                </a>
                            </div>
                
                            <a href="#" class="nx-listing__link">
                                <div class="nx-listing__head">
                                    <img src="https://via.placeholder.com/550x550"
                                         alt=""
                                         class="nx-listing__img">
                        
                                    <div class="nx-tags nx-tags_small">
                                        <? for ($j = 0; $j < 3; $j++): ?>
                                            <div class="nx-tags__item">Лучшая цена</div>
                                        <? endfor; ?>
                                    </div>
                                </div>
                    
                                <div class="nx-listing__body">
                                    <div class="nx-listing__note">
                                        <div class="nx-listing__articul">Артикул:
                                            <span>10073940</span>
                                        </div>
                            
                                        <div class="nx-listing__availible">В наличии</div>
                                    </div>
                        
                                    <span class="nx-listing__title">Баня Б-150 4х4 М с террасой и большой верандой </span>
                        
                                    <div class="spacer-v"></div>
                        
                                    <div class="nx-prices-item">
                                        <div class="nx-prices-item__old">10 000 &#8381;</div>
                            
                                        <div class="nx-prices-item__current" data-prod-price="8000">8 000 &#8381;</div>
                                    </div>
                                </div>
                            </a>
                
                            <div class="nx-listing__actions">
                                <div class="nx-listing__actions-row">
                                    <div class="nx-listing__actions-item">
                                        <div class="nx-counter" data-counter>
                                            <a href="#"
                                               class="nx-counter-btn nx-counter-btn_minus"
                                               data-counter-btn="minus"></a>
                                            <input type="text"
                                                   name="products[quantity]"
                                                   value="1"
                                                   class="nx-counter__input"
                                                   data-counter-inp data-max="999"
                                                   data-prod-cnt>
                                            <a href="#"
                                               class="nx-counter-btn nx-counter-btn_plus"
                                               data-counter-btn="plus"></a>
                                        </div>
                                    </div>
                        
                                    <div class="nx-listing__actions-item">
                                        <a href=""
                                           class="btn btn_br nx-listing__buy"
                                           data-prod-add>
                                            <span>В корзину</span>
                                
                                            <svg class="icon"><use xlink:href="#icon-cart"></use></svg>
                                        </a>
                                    </div>
                                </div>
                    
                                <a href="#" class="link nx-listing__click">
                                    <svg class="icon"><use xlink:href="#icon-one-click"></use></svg>
                        
                                    <span>Купить в один клик </span>
                                </a>
                            </div>
                        </div>
                    <? endfor; ?>
                </div>
    
                <div class="nx-pagination">
                    <a href="#" class="nx-pagination__arrow nx-pagination__arrow_prev disabled">
                        <svg class="nx-pagination__arrow-icon"><use xlink:href="#icon-arrow"></use></svg>
                    </a>
        
                    <div class="nx-pagination__item active">
                        <a href="#" class="nx-pagination__link link link_invert">
                            <span>1</span>
                        </a>
                    </div>
        
                    <div class="nx-pagination__item">
                        <a href="#" class="nx-pagination__link link link_invert">
                            <span>2</span>
                        </a>
                    </div>
        
                    <div class="nx-pagination__item nx-pagination__item_dots">
                        <div class="nx-pagination__dots">...</div>
                    </div>
        
                    <div class="nx-pagination__item">
                        <a href="#" class="nx-pagination__link link link_invert">
                            <span>9</span>
                        </a>
                    </div>
        
                    <div class="nx-pagination__item">
                        <a href="#" class="nx-pagination__link link link_invert">
                            <span>10</span>
                        </a>
                    </div>
        
                    <a href="#" class="nx-pagination__arrow nx-pagination__arrow_next">
                        <svg class="nx-pagination__arrow-icon"><use xlink:href="#icon-arrow"></use></svg>
                    </a>
                </div>
            
            </div>
        </div>
    </div>
    
    <? require_once '../templates/_blocks/footer.php'; ?>
</div>

<script src="/assets/app.min.js"></script>
</body>
</html>