<!DOCTYPE HTML>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta name="author" content="Контент">
    <meta name="keywords" content="keywords">
    <meta name="description" content="description">

    <title>Каталог</title>

    <link rel="icon" type="image/png" href="/favicon.png">
    <link rel="stylesheet" href="/assets/vendor.css">
    <link rel="stylesheet" href="/assets/app.min.css">

    <meta property="og:title" content="title"/>
    <meta property="og:description" content="description"/>
    <meta property="og:image" content="favicon.png">

</head>
<body>

<? require_once '../templates/_blocks/header.php'; ?>
<? require_once '../templates/_blocks/cart_informer.php'; ?>

<div class="wrapper">
    <div class="wrapper__content">
        <div class="nx-section nx-section_page container">
            <div class="nx-breadcrumbs">
                <div class="nx-breadcrumbs__item">
                    <a href="#" class="nx-breadcrumbs__link">
                        <span>Каталог</span>
                    </a>
                    
                    <i class="nx-breadcrumbs__arrow mdi mdi-chevron-right"></i>
                </div>
            </div>

            <div class="nx-section__head content-narrow-med">
                <h1 class="nx-section__title">Дачные дома</h1>
            </div>

            <a href="#" class="btn nx-filter-btn" data-filter-toggle>Фильтр</a>

            <div class="flex-row">
                <div class="flex-col md-6">
                    <div class="nx-affix">
                        <div class="bg-fade bg-fade_fixed" data-filter-fade></div>

                        <div class="nx-filter" data-filter>
                            <a href="#"
                               class="nx-filter__close"
                               data-filter-close>
                                <svg class="icon"><use xlink:href="#icon-close"></use></svg>
                            </a>

                            <div class="nx-filter__nav">
                                <div class="nx-filter-nav">
                                    <div class="nx-filter-nav__item">
                                        <a href="#" class="nx-filter-nav__link">
                                            <i class="link-icon mdi mdi-arrow-left"></i>
                                            
                                            <span>Каталог</span>
                                        </a>
                                    </div>
                                    
                                    <div class="nx-filter-nav__item">
                                        <a href="#" class="nx-filter-nav__link"><span>Ссылка</span></a>
                                    </div>
                                    
                                    <div class="nx-filter-nav__item">
                                        <a href="#" class="nx-filter-nav__link"><span>Ссылка</span></a>
                                    </div>
                                </div>
                            </div>


                            <form action="" class="nx-filter__form" data-filter-form>
                                <div class="nx-filter__section">
                                    <a href="#"
                                       class="link"
                                       data-hc-control="collapce-1">
                                        <i class="link-icon mdi mdi-chevron-down"></i>
                                        
                                        <span>Чекбоксы</span>
                                    </a>
                                    
                                    <div data-hc-content="collapce-1">
                                        <div class="nx-filter__scroll" data-custom-scroll>
                                            <? for ($i = 0; $i < 12; $i++): ?>
                                                <div class="nx-filter__item" data-filter-item>
                                                    <label class="checkbox">
                                                        <input type="checkbox"
                                                               class="checkbox__input"
                                                               name="checkbox-<?echo $i?>"
                                                               data-name="checkbox-<?echo $i?>"
                                                               value="<?echo $i?>"
                                                               data-filter-input>
                                                        
                                                        <span class="checkbox__text">С гаражом</span>
                                                        
                                                        <span class="checkbox__count" data-filter-count="5">(5)</span>
                                                    </label>
                                                </div>
                                            <? endfor; ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="nx-filter__section">
                                    <a href="#"
                                       class="link"
                                       data-hc-control="collapce-2">
                                        <i class="link-icon mdi mdi-chevron-down"></i>

                                        <span>Чекбоксы1</span>
                                    </a>

                                    <div data-hc-content="collapce-2">
                                        <div class="nx-filter__scroll" data-custom-scroll>
                                            <? for ($i = 0; $i < 12; $i++): ?>
                                                <div class="nx-filter__item" data-filter-item>
                                                    <label class="checkbox">
                                                        <input type="checkbox"
                                                               class="checkbox__input"
                                                               name="checkbox-<?echo $i?>"
                                                               data-name="checkbox1-<?echo $i?>"
                                                               value="<?echo $i?>"
                                                               data-filter-input>

                                                        <span class="checkbox__text">С гаражом</span>

                                                        <span class="checkbox__count" data-filter-count="5">(5)</span>
                                                    </label>
                                                </div>
                                            <? endfor; ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="nx-filter__section">
                                    <a href="#"
                                       class="link collapsed"
                                       data-hc-control="collapce-3">
                                        <i class="link-icon mdi mdi-chevron-down"></i>
                                        
                                        <span>Радиокнопки</span>
                                    </a>
                                    
                                    <div data-hc-content="collapce-3">
                                        <div class="nx-filter__scroll" data-custom-scroll>
                                            <? for ($i = 0; $i < 12; $i++): ?>
                                                <div class="nx-filter__item" data-filter-item>
                                                    <label class="checkbox checkbox_radio">
                                                        <input type="radio"
                                                               class="checkbox__input"
                                                               name="radio"
                                                               data-name="radio-<?echo $i?>"
                                                               <?php if ($i===0):?>checked <?php endif; ?>
                                                               value="<?echo $i?>"
                                                               data-filter-input>
                                                        
                                                        <span class="checkbox__text">5х6 </span>

                                                        <span class="checkbox__count" data-filter-count="5">(5)</span>
                                                    </label>
                                                </div>
                                            <? endfor; ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="nx-filter__section">
                                    <a href="#"
                                       class="link collapsed"
                                       data-hc-control="collapce-4">
                                        <i class="link-icon mdi mdi-chevron-down"></i>
                                        
                                        <span>Ползунок</span>
                                    </a>
                                    
                                    <div class="nx-filter__scroll" data-hc-content="collapce-4">
                                        <div class="range-slider" data-range>
                                            <div class="flex-row range-slider__inputs">
                                                <div class="flex-col xs-12">
                                                    <input type="text"
                                                           class="nx-form-element"
                                                           name="attrs[500]"
                                                           data-range-inp
                                                           data-ui-min="10">
                                                </div>

                                                <div class="flex-col xs-12">
                                                    <input type="text"
                                                           class="nx-form-element"
                                                           name="attrs[500]"
                                                           data-range-inp
                                                           data-ui-max="100">
                                                </div>
                                            </div>

                                            <div class="ui-slider"
                                                 data-ui-slider
                                                 data-name="attrs[500]"
                                                 data-ui-min="10"
                                                 data-ui-max="100"
                                                 data-step="1"></div>
                                        </div>
                                        
                                        <div class="range-slider" data-range>
                                            <div class="flex-row range-slider__inputs">
                                                <div class="flex-col xs-12">
                                                    <input type="text"
                                                           class="nx-form-element"
                                                           name="attrs[600]"
                                                           data-range-inp
                                                           data-ui-min="10">
                                                </div>

                                                <div class="flex-col xs-12">
                                                    <input type="text"
                                                           class="nx-form-element"
                                                           name="attrs[600]"
                                                           data-range-inp
                                                           data-ui-max="100">
                                                </div>
                                            </div>

                                            <div class="ui-slider"
                                                 data-ui-slider
                                                 data-name="attrs[600]"
                                                 data-ui-min="10"
                                                 data-ui-max="100"
                                                 data-step="1"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="nx-filter__actions">
                                    <div class="nx-filter__actions-item">
                                        <button type="submit" class="btn btn_d-block">Применить
                                            <span data-filter-cnt></span>
                                        </button>
                                    </div>
                                    
                                    <div class="nx-filter__actions-item">
                                        <a href="#"
                                           class="link link_red"
                                           data-filter-clear>
                                            <i class="link-icon mdi mdi-close"></i>
                                            
                                            <span>Сбросить фильтр</span>
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                
                <div class="flex-col md-18">
                    <div class="nx-filter-line">
                        <div class="nx-filter-line__item">
                            <div class="nx-filter-line__title">Сортировать по:</div>
                            
                            <div class="nx-filter-line__select">
                                <select id=""
                                        name=""
                                        data-sort
                                        data-custom-select
                                        data-placeholder="Цене">
                                    <option value="1">Цене</option>
                                    
                                    <option value="2">Порядку</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="nx-filter-line__item">
                            <div class="nx-filter-line__title">Выводить по:</div>
                            
                            <div class="nx-filter-line__values">
                                <a href="#"
                                   class="nx-filter-line__link link active"
                                   data-output="12">
                                    <span>12</span>
                                </a>
                                
                                <a href="#"
                                   class="nx-filter-line__link link"
                                   data-output="24">
                                    <span>24</span>
                                </a>
                                
                                <a href="#"
                                   class="nx-filter-line__link link"
                                   data-output="48">
                                    <span>48</span>
                                </a>
                                
                                <a href="#"
                                   class="nx-filter-line__link link"
                                   data-output="96">
                                    <span>96</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="nx-listing nx-listing_prod flex-row">
                        <? for ($i = 0; $i < 12; $i++): ?>
                            <div class="nx-listing__item flex-col md-8 sm-8"
                                 data-prod="<?echo  $i ?>"
                                 data-prod-type="prod"
                                 data-prod-weight="1000">
                                <div class="nx-listing__icons">
                                    <a href="#" class="nx-listing__icon-btn">
                                        <svg class="nx-listing__icon"><use xlink:href="#icon-like"></use></svg>
                                    </a>
                                    
                                    <a href="#" class="nx-listing__icon-btn nx-compare-link nx-tooltip nx-tooltip_top">
                                        <div class="nx-tooltip__item">
                                            <div class="nx-tooltip__text" data-compare-title="Выберите несколько товаров, чтобы сравнить их">Выберите несколько товаров, чтобы сравнить их</div>

                                            <div class="nx-tooltip__arrow"></div>
                                        </div>
                                        
                                        <svg class="nx-listing__icon"><use xlink:href="#icon-promote"></use></svg>
                                    </a>
                                </div>
                                
                                <a href="#" class="nx-listing__link">
                                    <div class="nx-listing__head">
                                        <img src="https://via.placeholder.com/550x550"
                                             alt=""
                                             class="nx-listing__img">
                                        
                                        <div class="nx-tags nx-tags_small">
                                            <? for ($j = 0; $j < 3; $j++): ?>
                                                <div class="nx-tags__item">Лучшая цена</div>
                                            <? endfor; ?>
                                        </div>
                                    </div>
                                    
                                    <div class="nx-listing__body">
                                        <div class="nx-listing__note">
                                            <div class="nx-listing__articul">Артикул:
                                                <span>10073940</span>
                                            </div>
                                            
                                            <div class="nx-listing__availible">В наличии</div>
                                        </div>
                                        
                                        <span class="nx-listing__title"
                                              data-prod-title="Баня Б-150 4х4 М с террасой и большой верандой">Баня Б-150 4х4 М с террасой и большой верандой </span>
                                        
                                        <div class="spacer-v"></div>
                                        
                                        <div class="nx-prices-item">
                                            <div class="nx-prices-item__old">10 000 &#8381;</div>
                                            
                                            <div class="nx-prices-item__current" data-prod-price="8000">8 000 &#8381;</div>
                                        </div>
                                    </div>
                                </a>
                                
                                <div class="nx-listing__actions">
                                    <div class="nx-listing__actions-row">
                                        <div class="nx-listing__actions-item">
                                            <div class="nx-counter" data-counter>
                                                <a href="#"
                                                   class="nx-counter-btn nx-counter-btn_minus"
                                                   data-counter-btn="minus"></a>
                                                <input type="text"
                                                       name="products[quantity]"
                                                       value="1"
                                                       class="nx-counter__input"
                                                       data-counter-inp data-max="999"
                                                       data-prod-cnt>
                                                <a href="#"
                                                   class="nx-counter-btn nx-counter-btn_plus"
                                                   data-counter-btn="plus"></a>
                                            </div>
                                        </div>
                                        
                                        <div class="nx-listing__actions-item">
                                            <a href=""
                                               class="btn btn_br nx-listing__buy"
                                               data-type="add"
                                               data-prod-add>
                                                <span>В корзину</span>
                                                
                                                <svg class="icon"><use xlink:href="#icon-cart"></use></svg>
                                            </a>
                                        </div>
                                    </div>
                                    
                                    <a href="#" class="link nx-listing__click">
                                        <svg class="icon"><use xlink:href="#icon-one-click"></use></svg>
                                        
                                        <span>Купить в один клик </span>
                                    </a>
                                </div>
                            </div>
                        <? endfor; ?>
                    </div>

                    <div class="nx-pagination">
                        <a href="#" class="nx-pagination__arrow nx-pagination__arrow_prev disabled">
                            <svg class="nx-pagination__arrow-icon"><use xlink:href="#icon-arrow"></use></svg>
                        </a>

                        <div class="nx-pagination__item active">
                            <a href="#" class="nx-pagination__link link link_invert">
                                <span>1</span>
                            </a>
                        </div>
                        
                        <div class="nx-pagination__item">
                            <a href="#" class="nx-pagination__link link link_invert">
                                <span>2</span>
                            </a>
                        </div>

                        <div class="nx-pagination__item nx-pagination__item_dots">
                            <div class="nx-pagination__dots">...</div>
                        </div>

                        <div class="nx-pagination__item">
                            <a href="#" class="nx-pagination__link link link_invert">
                                <span>9</span>
                            </a>
                        </div>
                        
                        <div class="nx-pagination__item">
                            <a href="#" class="nx-pagination__link link link_invert">
                                <span>10</span>
                            </a>
                        </div>

                        <a href="#" class="nx-pagination__arrow nx-pagination__arrow_next">
                            <svg class="nx-pagination__arrow-icon"><use xlink:href="#icon-arrow"></use></svg>
                        </a>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <? require_once '../templates/_blocks/footer.php'; ?>
</div>

<script src="/assets/app.min.js"></script>
</body>
</html>
