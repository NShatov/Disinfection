<!DOCTYPE HTML>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta name="author" content="Контент">
    <meta name="keywords" content="keywords">
    <meta name="description" content="description">

    <title>Сертификаты</title>

    <link rel="icon" type="image/png" href="/favicon.png">
    <link rel="stylesheet" href="/assets/vendor.css">
    <link rel="stylesheet" href="/assets/app.min.css">

    <meta property="og:title" content="title"/>
    <meta property="og:description" content="description"/>
    <meta property="og:image" content="favicon.png">

</head>
<body>

<? require_once '../templates/_blocks/header.php'; ?>

<div class="wrapper">
    <div class="wrapper__content">
        <section class="nx-section container">
            <div class="nx-section__head content-narrow-med">
                <h2 class="nx-section__title">Сертификаты</h2>
                
                <div class="nx-section__text  text text_dark-fade"> Крупнейший интернет-магазин модной одежды, обуви,
                    аксессуаров, косметических
                    средств, существующий уже 15 лет! Из года в год мы продолжаем развиваться, расширять географию
                    присутствия и
                    улучшать качество обслуживания, чтобы радовать Вас каждый день!
                </div>
            </div>

            <div class="nx-files nx-files_big flex-row">
                <? for ($i = 0; $i < 8; $i++): ?>
                    <div class="nx-files__item flex-col md-8 sm-12">
                        <a href="#"
                           class="nx-files__link"
                           target="_blank"
                           rel="noopener">
                            <div class="nx-files__icon" style="background-image: url('https://via.placeholder.com/225x300');"></div>
                            
                            <div class="nx-files__content">
                                <span class="nx-files__title">Сертификат пожарной опасности стальных профилей роликовой прокатки</span>
                                <div class="nx-files__type">PDF. 2.1 мб</div>
                            </div>
                        </a>
                    </div>
                <? endfor; ?>
            </div>
        </section>
    </div>
    <? require_once '../templates/_blocks/footer.php'; ?>
</div>

<script src="/assets/app.min.js"></script>
</body>
</html>

