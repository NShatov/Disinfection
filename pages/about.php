<!DOCTYPE HTML>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta name="author" content="Контент">
    <meta name="keywords" content="keywords">
    <meta name="description" content="description">

    <title>О компании</title>

    <link rel="icon" type="image/png" href="/favicon.png">
    <link rel="stylesheet" href="/assets/vendor.css">
    <link rel="stylesheet" href="/assets/app.min.css">

    <meta property="og:title" content="title"/>
    <meta property="og:description" content="description"/>
    <meta property="og:image" content="favicon.png">

</head>
<body>

<div class="wrapper">
    <div class="wrapper__content">
        <section class="nx-section container">
            <div class="content-narrow-med">
                <div class="nx-section__box">
                    <div class="nx-section__head">
                        <h2 class="nx-section__title">О проекте</h2>
                        
                        <div class="nx-section__text text text_dark-fade">
                            На нашем сайте вы узнаете, какие требования предъявляют проверяющие органы к дезинфекционным мероприятиям в этих организациях и как обеспечить их соблюдение. Вам помогут разобраться в правилах использования дезсредств и обеспечить безопасность при работе с ними
                        </div>
                    </div>
                </div>

                <div class="wrapper">
                    <div class="wrapper__content">
                        <? require_once 'pages/catalog.php'; ?>
                    </div>
                </div>
                
                <div class="nx-section__box">
                    <h4 class="page-title">Wildberries в цифрах</h4>
                    
                    <div class="nx-section nx-section_bg nx-section_c-dark nx-section_inner">
                        <div class="nx-advans nx-advans_light flex-row">
                            <? for ($i = 0; $i < 3; $i++): ?>
                                <div class="nx-advans__item flex-col md-8 sm-12">
                                    <div class="nx-advans__num factoid" data-anim-num="35000">35 000</div>
                                    
                                    <div class="nx-advans__text">Популярных и проверенных брендов на сайте</div>
                                </div>
                            <? endfor; ?>
                        </div>
                    </div>
                </div>
                
                <div class="nx-section__box">
                    <h4 class="page-title">Wildberries в цифрах</h4>
                    <div class="nx-section nx-section_bg nx-section_c-dark nx-section_inner">
                        <div class="nx-advans nx-advans_light flex-row">
                            <? for ($i = 0; $i < 3; $i++): ?>
                                <div class="nx-advans__item flex-col md-8 sm-12">
                                    <div class="nx-advans__icon"
                                         style="background-image: url('https://via.placeholder.com/140x140');"></div>
                                    
                                    <div class="nx-advans__text">Популярных и проверенных брендов на сайте</div>
                                </div>
                            <? endfor; ?>
                        </div>
                    </div>
                </div>

                <h4 class="nx-section__title">Крупнейший интернет-магазин в России</h4>
                
                <div class="nx-gal-slider not-visible" data-gal>
                    <div class="nx-gal-slider__main" data-gal-main>
                        <div class="swiper-wrapper nx-gal-slider__main-wrap">
                            <!--Video-->
                            <div class="swiper-slide nx-gal-slider__main-slide nx-video" data-video-content>
                                <a href="#"
                                   class="nx-video__link"
                                   data-video-load="mgcQDHpPVLo"
                                   style="background-image: url('https://via.placeholder.com/1980x1200')">
                                    <svg class="nx-video__icon"><use xlink:href="#icon-play"></use></svg>
                                </a>
                            </div>
                            
                            <? for ($i = 0; $i < 10; $i++): ?>
                                <a href="https://via.placeholder.com/1920x1080"
                                   class="swiper-slide nx-gal-slider__main-slide"
                                   data-fancybox="gal">
                                    
                                    <img src="https://via.placeholder.com/1980x1200"
                                         alt=""
                                         class="nx-gal-slider__main-img">
                                </a>
                            <? endfor; ?>
                        </div>

                        <div class="nx-gal-slider__prev" data-gal-prev>
                            <svg class="nx-gal-slider__prev-icon"><use xlink:href="#icon-arrow"></use></svg>
                        </div>
                        
                        <div class="nx-gal-slider__next nx-gal-slider__next" data-gal-next>
                            <svg class="nx-gal-slider__next-icon"><use xlink:href="#icon-arrow"></use></svg>
                        </div>
                    </div>

                    <div class="nx-gal-slider__thumbs" data-gal-thumb>
                        <div class="swiper-wrapper nx-gal-slider__thumbs-wrap">
                            <? for ($i = 0; $i < 10; $i++): ?>
                                <div class="swiper-slide nx-gal-slider__thumbs-slide"
                                     data-gal-thumb-slide
                                     style="background-image: url('https://via.placeholder.com/260x150');"></div>
                            <? endfor; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="nx-section container">
            <h4 class="nx-section__title">Крупнейший интернет-магазин в России</h4>
            
            <div class="nx-gal-slider not-visible" data-gal>
                <div class="nx-gal-slider__main" data-gal-main>
                    <div class="swiper-wrapper nx-gal-slider__main-wrap">
                        <div class="swiper-slide nx-gal-slider__main-slide nx-video" data-video-content>
                            <a href="#"
                               class="nx-video__link"
                               data-video-load="mgcQDHpPVLo"
                               style="background-image: url('https://via.placeholder.com/1980x1200')">
                                <svg class="nx-video__icon"><use xlink:href="#icon-play"></use></svg>
                            </a>
                        </div>
                        <? for ($i = 0; $i < 10; $i++): ?>
                            <a href="https://via.placeholder.com/1920x1080"
                               data-fancybox="gal"
                               class="swiper-slide nx-gal-slider__main-slide">
                                <img src="https://via.placeholder.com/1980x1200"
                                     alt=""
                                     class="nx-gal-slider__main-img">
                            </a>
                        <? endfor; ?>
                    </div>

                    <div class="nx-gal-slider__prev" data-gal-prev>
                        <svg class="nx-gal-slider__prev-icon"><use xlink:href="#icon-arrow"></use></svg>
                    </div>
                    
                    <div class="nx-gal-slider__next nx-gal-slider__next" data-gal-next>
                        <svg class="nx-gal-slider__next-icon"><use xlink:href="#icon-arrow"></use></svg>
                    </div>
                </div>

                <div class="nx-gal-slider__thumbs" data-gal-thumb>
                    <div class="swiper-wrapper nx-gal-slider__thumbs-wrap">
                        <? for ($i = 0; $i < 10; $i++): ?>
                            <div class="swiper-slide nx-gal-slider__thumbs-slide"
                                 data-gal-thumb-slide
                                 style="background-image: url('https://via.placeholder.com/260x150');"></div>
                        <? endfor; ?>
                    </div>
                </div>
            </div>
        </section>

        <div class="nx-section container">
            <div class="nx-section nx-section_bg nx-section_c-dark nx-section_inner">
                <div class="nx-section__box">
                    <div class="nx-section__head content-narrow">
                        <h2 class="nx-section__title">О компании</h2>
                        
                        <div class="nx-section__text text">Крупнейший интернет-магазин модной одежды, обуви,
                            аксессуаров,
                            косметических средств, существующий уже 15 лет! Из года в год мы продолжаем развиваться,
                            расширять
                            географию присутствия и улучшать качество обслуживания, чтобы радовать Вас каждый день!
                        </div>
                    </div>
                    
                    <a href="" class="btn btn_lt">Узнать больше</a>
                </div>
                
                <div class="nx-section__box">
                    <div class="line line_lt"></div>
                </div>
                
                <div class="nx-section__box">
                    <h4 class="nx-section__title">Wildberries в цифрах</h4>
                    
                    <div class="nx-advans nx-advans_light flex-row">
                        <? for ($i = 0; $i < 3; $i++): ?>
                            <div class="nx-advans__item flex-col md-8 sm-12">
                                <div class="nx-advans__num factoid" data-anim-num="300000">300 000</div>
                                
                                <div class="nx-advans__text">Популярных и проверенных брендов на сайте</div>
                            </div>
                        <? endfor; ?>
                    </div>
                </div>
            </div>
        </div>

        <section class="nx-section container">
            <div class="nx-section__head content-narrow-med">
                <h2 class="nx-section__title">История компании</h2>
                
                <div class="nx-section__text text text_dark-fade">Крупнейший интернет-магазин модной одежды, обуви,
                    аксессуаров, косметических
                    средств, существующий уже 15 лет! Из года в год мы продолжаем развиваться, расширять географию
                    присутствия и
                    улучшать качество обслуживания, чтобы радовать Вас каждый день!
                </div>
            </div>

            <div class="nx-section__box">
                <div class="nx-history" data-gal data-history="history">
                    <div class="nx-history__main" data-gal-main>
                        <div class="swiper-wrapper nx-history__main-wrap">
                            <? for ($i = 0; $i < 4; $i++): ?>
                                <div class="swiper-slide nx-history__main-slide">
                                    <div class="flex-row flex-row_ai-center nx-history__row">
                                        <div class="flex-col md-12 sm-12">
                                            <div class="nx-common-slider">
                                                <div class="nx-common-slider__slider" data-common-slider>
                                                    <div class="swiper-wrapper">
                                                        <div class="swiper-slide nx-video" data-video-content>
                                                            <a href="#"
                                                               class="nx-video__link"
                                                               data-video-load="mgcQDHpPVLo"
                                                               style="background-image: url('https://via.placeholder.com/1160x650')">
                                                                <svg class="nx-video__icon"><use xlink:href="#icon-play"></use></svg>
                                                            </a>
                                                            
                                                            <img src="https://via.placeholder.com/1160x650">
                                                        </div>
                                                        
                                                        <? for ($j = 0; $j < 2; $j++): ?>
                                                            <div class="swiper-slide">
                                                                <img src="https://via.placeholder.com/1160x650">
                                                            </div>
                                                        <? endfor; ?>
                                                    </div>
                                                    
                                                    <div class="nx-common-slider__prev" data-common-slider-prev>
                                                        <svg class="nx-common-slider__prev-icon"><use xlink:href="#icon-arrow"></use></svg>
                                                    </div>
                                                    
                                                    <div class="nx-common-slider__next" data-common-slider-next>
                                                        <svg class="nx-common-slider__next-icon"><use xlink:href="#icon-arrow"></use></svg>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex-col md-12 sm-12">
                                            <div class="nx-history__body">
                                                <div class="nx-history__date">2009</div>
                                                
                                                <div class="nx-history__title">Торжественное открытие предприятия</div>
                                                
                                                <div class="nx-history__text text text_dark-fade">
                                                    Крупнейший интернет-магазин модной одежды, обуви, аксессуаров,
                                                    косметических
                                                    средств, существующий уже 15 лет! Из года в год мы продолжаем
                                                    развиваться,
                                                    расширять географию присутствия и улучшать качество обслуживания,
                                                    чтобы радовать
                                                    Вас каждый день!
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <? endfor; ?>
                        </div>
                    </div>
                    
                    <div class="nx-history__prev" data-gal-prev>
                        <svg class="nx-history__prev-icon"><use xlink:href="#icon-arrow"></use></svg>
                    </div>
                    
                    <div class="nx-history__next nx-history__next" data-gal-next>
                        <svg class="nx-history__next-icon"><use xlink:href="#icon-arrow"></use></svg>
                    </div>
                    
                    <div class="nx-history__thumbs" data-gal-thumb>
                        <div class="swiper-wrapper nx-history__thumbs-wrap">
                            <? for ($x = 0; $x < 4; $x++): ?>
                                <div class="swiper-slide nx-history__thumbs-slide" data-gal-thumb-slide>
                                    <div class="nx-history__item">
                                        <div class="nx-history__year">2020</div>
                                        
                                        <div class="nx-history__dot"></div>
                                    </div>
                                </div>
                            <? endfor; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="nx-section__box flex-row">
                <div class="flex-col md-14">
                    <div class="nx-history nx-history_col">
                        <div class="nx-history__main">
                            <div class="nx-history__main-wrap">
                                <div data-load-content>
                                    <? for ($i = 0; $i < 4; $i++): ?>
                                        <div class="nx-history__main-slide">
                                            <div class="nx-history__item">
                                                <div class="nx-history__dot"></div>
                                            </div>
                                            
                                            <div class="nx-history__date">2009</div>
                                            
                                            <div class="nx-history__content">
                                                <div class="nx-common-slider">
                                                    <div class="nx-common-slider__slider" data-common-slider>
                                                        <div class="swiper-wrapper">
                                                            <div class="swiper-slide nx-video" data-video-content>
                                                                <a href="#"
                                                                   class="nx-video__link"
                                                                   data-video-load="mgcQDHpPVLo"
                                                                   style="background-image: url('https://via.placeholder.com/1160x650')">
                                                                    <svg class="nx-video__icon"><use xlink:href="#icon-play"></use></svg>
                                                                </a>
                                                                
                                                                <img src="https://via.placeholder.com/1160x650">
                                                            </div>
                                                            <? for ($j = 0; $j < 2; $j++): ?>
                                                                <div class="swiper-slide">
                                                                    <img src="https://via.placeholder.com/1160x650">
                                                                </div>
                                                            <? endfor; ?>
                                                        </div>
                                                        
                                                        <div class="nx-common-slider__prev" data-common-slider-prev>
                                                            <svg class="nx-common-slider__prev-icon"><use xlink:href="#icon-arrow"></use></svg>
                                                        </div>
                                                        
                                                        <div class="nx-common-slider__next" data-common-slider-next>
                                                            <svg class="nx-common-slider__next-icon"><use xlink:href="#icon-arrow"></use></svg>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="nx-history__body">
                                                <div class="nx-history__title">Торжественное открытие предприятия</div>
                                                
                                                <div class="nx-history__text text text_dark-fade">Крупнейший
                                                    интернет-магазин модной
                                                    одежды, обуви, аксессуаров, косметических средств, существующий уже
                                                    15 лет! Из
                                                    года в год мы продолжаем развиваться, расширять географию
                                                    присутствия и улучшать
                                                    качество обслуживания, чтобы радовать Вас каждый день!
                                                </div>
                                            </div>
                                        </div>
                                    <? endfor; ?>
                                    <div class="nx-actions nx-actions_single">
                                        <div class="nx-actions__item">
                                            <a href="" class="btn btn_br" data-show-more data-show-more-url="">Показать еще</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="nx-section container">
            <h4 class="nx-section__title">Крупнейший интернет-магазин в России </h4>
            
            <div class="nx-gal-slider not-visible" data-gal>
                <div class="nx-gal-slider__main" data-gal-main>
                    <div class="swiper-wrapper nx-gal-slider__main-wrap">
                        <!--Video-->
                        <div class="swiper-slide nx-gal-slider__main-slide nx-video" data-video-content>
                            <a href="#"
                               class="nx-video__link"
                               data-video-load="mgcQDHpPVLo"
                               style="background-image: url('https://via.placeholder.com/1980x1200')">
                                <svg class="nx-video__icon"><use xlink:href="#icon-play"></use></svg>
                            </a>
                        </div>
                        
                        <? for ($i = 0; $i < 10; $i++): ?>
                            <a href="https://via.placeholder.com/1920x1080"
                               class="swiper-slide nx-gal-slider__main-slide"
                               data-fancybox="gal">
                                <img src="https://via.placeholder.com/1980x1200"
                                     alt=""
                                     class="nx-gal-slider__main-img">
                            </a>
                        <? endfor; ?>
                    </div>

                    <div class="nx-gal-slider__prev" data-gal-prev>
                        <svg class="nx-gal-slider__prev-icon"><use xlink:href="#icon-arrow"></use></svg>
                    </div>
                    
                    <div class="nx-gal-slider__next nx-gal-slider__next" data-gal-next>
                        <svg class="nx-gal-slider__next-icon"><use xlink:href="#icon-arrow"></use></svg>
                    </div>
                </div>

                <div class="nx-gal-slider__thumbs" data-gal-thumb>
                    <div class="swiper-wrapper nx-gal-slider__thumbs-wrap">
                        <? for ($i = 0; $i < 10; $i++): ?>
                            <div class="swiper-slide nx-gal-slider__thumbs-slide"
                                 data-gal-thumb-slide
                                 style="background-image: url('https://via.placeholder.com/260x150');"></div>
                        <? endfor; ?>
                    </div>
                </div>
            </div>
        </section>

        <section class="nx-section container">

            <h2 class="nx-section__title">Наша команда</h2>

            <div class="nx-listing nx-listing_team">
                <? for ($i = 0; $i < 2; $i++): ?>
                    <div class="nx-listing__element">
                        <div class="nx-listing__item">
                            <div class="nx-listing__head">
                                <img src="https://via.placeholder.com/620x620" alt="" class="nx-listing__img">
                            </div>
                            
                            <div class="nx-listing__body">
                                <div class="nx-contacts">
                                    <div class="nx-contacts__element">
                                        <div class="nx-contacts__item">
                                            <div class="nx-contacts__label">Начальник отдела</div>
                                            <div class="nx-contacts__text">Иванова Наталья Владимировна</div>
                                        </div>
                                        
                                        <div class="nx-contacts__item">
                                            <div class="nx-contacts__label">Телефон</div>
                                            <a href="tel:79257863301" class="nx-contacts__link">+7 (925) 786-33-01</a>
                                            <a href="tel:79257863301" class="nx-contacts__link">+7 (925) 786-33-01</a>
                                        </div>
                                        
                                        <div class="nx-contacts__item">
                                            <div class="nx-contacts__label">E-mail</div>
                                            <a href="mailto:Ivanova@zti.ru" class="nx-contacts__link">Ivanova@zti.ru</a>
                                            <a href="mailto:Ivanova@zti.ru" class="nx-contacts__link">Ivanova@zti.ru</a>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="spacer-v"></div>
                                
                                <div class="nx-socials nx-socials_small">
                                    <div class="nx-socials__item">
                                        <a href=""
                                           class="nx-socials__link nx-socials__link_vk"
                                           target="_blank"
                                           rel="noopener">
                                            <svg class="nx-socials__icon"><use xlink:href="#icon-vk"></use></svg>
                                        </a>
                                    </div>
                                    
                                    <div class="nx-socials__item">
                                        <a href=""
                                           class="nx-socials__link nx-socials__link_fb"
                                           target="_blank"
                                           rel="noopener">
                                            <svg class="nx-socials__icon"><use xlink:href="#icon-fb"></use></svg>
                                        </a>
                                    </div>
                                    
                                    <div class="nx-socials__item">
                                        <a href=""
                                           class="nx-socials__link nx-socials__link_inst"
                                           target="_blank"
                                           rel="noopener">
                                            <svg class="nx-socials__icon"><use xlink:href="#icon-inst"></use></svg>
                                        </a>
                                    </div>
                                    
                                    <div class="nx-socials__item">
                                        <a href=""
                                           class="nx-socials__link nx-socials__link_yt"
                                           target="_blank"
                                           rel="noopener">
                                            <svg class="nx-socials__icon"><use xlink:href="#icon-yt"></use></svg>
                                        </a>
                                    </div>
                                    
                                    <div class="nx-socials__item">
                                        <a href=""
                                           class="nx-socials__link nx-socials__link_ok"
                                           target="_blank"
                                           rel="noopener">
                                            <svg class="nx-socials__icon"><use xlink:href="#icon-ok"></use></svg>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <? endfor; ?>
                
                <div class="nx-listing__element">
                    <div class="nx-listing__item">
                        <div class="nx-listing__head">
                            <img src="https://via.placeholder.com/620x620"
                                 alt=""
                                 class="nx-listing__img">
                        </div>
                        
                        <div class="nx-listing__body">
                            <div class="nx-contacts">
                                <div class="nx-contacts__element">
                                    <div class="nx-contacts__item">
                                        <div class="nx-contacts__label">Телефон</div>
                                        <a href="tel:79257863301" class="nx-contacts__link">+7 (925) 786-33-01</a>
                                    </div>
                                    
                                    <div class="nx-contacts__item">
                                        <div class="nx-contacts__label">E-mail</div>
                                        <a href="mailto:Ivanova@zti.ru" class="nx-contacts__link">Ivanova@zti.ru</a>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="spacer-v"></div>
                            
                            <div class="nx-socials nx-socials_small">
                                <div class="nx-socials__item">
                                    <a href=""
                                       class="nx-socials__link nx-socials__link_vk"
                                       target="_blank"
                                       rel="noopener">
                                        <svg class="nx-socials__icon"><use xlink:href="#icon-vk"></use></svg>
                                    </a>
                                </div>
                                
                                <div class="nx-socials__item">
                                    <a href=""
                                       class="nx-socials__link nx-socials__link_fb"
                                       target="_blank"
                                       rel="noopener">
                                        <svg class="nx-socials__icon"><use xlink:href="#icon-fb"></use></svg>
                                    </a>
                                </div>
                                
                                <div class="nx-socials__item">
                                    <a href=""
                                       class="nx-socials__link nx-socials__link_inst"
                                       target="_blank"
                                       rel="noopener">
                                        <svg class="nx-socials__icon"><use xlink:href="#icon-inst"></use></svg>
                                    </a>
                                </div>
                                
                                <div class="nx-socials__item">
                                    <a href=""
                                       class="nx-socials__link nx-socials__link_yt"
                                       target="_blank"
                                       rel="noopener">
                                        <svg class="nx-socials__icon"><use xlink:href="#icon-yt"></use></svg>
                                    </a>
                                </div>
                                
                                <div class="nx-socials__item">
                                    <a href=""
                                       class="nx-socials__link nx-socials__link_ok"
                                       target="_blank"
                                       rel="noopener">
                                        <svg class="nx-socials__icon"><use xlink:href="#icon-ok"></use></svg>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>

    </div>
    <? require_once '../templates/_blocks/footer.php'; ?>
</div>

<script src="/assets/app.min.js"></script>
</body>
</html>
