<!DOCTYPE HTML>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta name="author" content="Контент">
    <meta name="keywords" content="keywords">
    <meta name="description" content="description">

    <title>Новости список</title>

    <link rel="icon" type="image/png" href="/favicon.png">
    <link rel="stylesheet" href="/assets/vendor.css">
    <link rel="stylesheet" href="/assets/app.min.css">

    <meta property="og:title" content="title"/>
    <meta property="og:description" content="description"/>
    <meta property="og:image" content="favicon.png">

</head>
<body>

<? require_once '../templates/_blocks/header.php'; ?>

<div class="wrapper">
    <div class="wrapper__content">
        <div class="nx-section container">
            <div class="nx-section__head">
                <h2 class="nx-section__title">Новости</h2>
            </div>
            
            <div class="nx-section__row">
                <div class="flex-row nx-listing nx-listing_news">
                    <? for ($i = 0; $i < 3; $i++): ?>
                        <div class="flex-col sm-8 nx-listing__item">
                            <a href="#" class="nx-listing__link">
                                <div class="nx-listing__head">
                                    <img src="https://via.placeholder.com/600x340" alt="" class="nx-listing__img">
                                </div>
                                
                                <div class="nx-listing__body">
                                    <div class="nx-listing__date">29.01.20</div>
                                    
                                    <span class="nx-listing__title">Распродажа зимней коллекции 2020 в самом разгаре </span>
                                    
                                    <div class="nx-listing__text">
                                        Успейте к главной распродаже года, скидки до 90% на все группы товаров. Только в
                                        этом феврале...
                                    </div>
                                </div>
                            </a>
                        </div>
                    <? endfor; ?>
                </div>
            </div>

            <div class="nx-pagination">
                <a href="#" class="nx-pagination__arrow nx-pagination__arrow_prev disabled">
                    <svg class="nx-pagination__arrow-icon"><use xlink:href="#icon-arrow"></use></svg>
                </a>

                <div class="nx-pagination__item active">
                    <a href="#" class="nx-pagination__link link link_invert">
                        <span>1</span>
                    </a>
                </div>
                
                <div class="nx-pagination__item">
                    <a href="#" class="nx-pagination__link link link_invert">
                        <span>2</span>
                    </a>
                </div>

                <div class="nx-pagination__item nx-pagination__item_dots">
                    <div class="nx-pagination__dots">...</div>
                </div>

                <div class="nx-pagination__item">
                    <a href="#" class="nx-pagination__link link link_invert">
                        <span>9</span>
                    </a>
                </div>
                
                <div class="nx-pagination__item">
                    <a href="#" class="nx-pagination__link link link_invert">
                        <span>10</span>
                    </a>
                </div>

                <a href="#" class="nx-pagination__arrow nx-pagination__arrow_next">
                    <svg class="nx-pagination__arrow-icon"><use xlink:href="#icon-arrow"></use></svg>
                </a>
            </div>
        </div>
    </div>
    
    <? require_once '../templates/_blocks/footer.php'; ?>
</div>

<script src="/assets/app.min.js"></script>
</body>
</html>

