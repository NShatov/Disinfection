<!DOCTYPE HTML>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <meta name="author" content="Контент">
    <meta name="keywords" content="keywords">
    <meta name="description" content="description">
    
    <title>Форма входа</title>
    
    <link rel="icon" type="image/png" href="/favicon.png">
    <link rel="stylesheet" href="/assets/vendor.css">
    <link rel="stylesheet" href="/assets/app.min.css">
    
    <meta property="og:title" content="title"/>
    <meta property="og:description" content="description"/>
    <meta property="og:image" content="favicon.png">

</head>
<body>

<? require_once '../templates/_blocks/header.php'; ?>

<div class="wrapper wrapper_flex">
    <div class="wrapper__content">
        <div class="flex-row container">
            <div class="flex-col md-8 sm-6"></div>
            <div class="flex-col md-10 sm-18">
                <h3 class="nx-section__title">Регистрация</h3>

                <div class="nx-socials-group">
                    <div class="nx-socials-title">Через соцсети</div>

                    <div class="nx-socials nx-socials_small">
                        <div class="nx-socials__item">
                            <a href=""
                               class="nx-socials__link nx-socials__link_vk"
                               target="_blank"
                               rel="noopener">
                                <svg class="nx-socials__icon"><use xlink:href="#icon-vk"></use></svg>
                            </a>
                        </div>

                        <div class="nx-socials__item">
                            <a href=""
                               class="nx-socials__link nx-socials__link_fb"
                               target="_blank"
                               rel="noopener">
                                <svg class="nx-socials__icon"><use xlink:href="#icon-fb"></use></svg>
                            </a>
                        </div>

                        <div class="nx-socials__item">
                            <a href=""
                               class="nx-socials__link nx-socials__link_google"
                               target="_blank"
                               rel="noopener">
                                <svg class="nx-socials__icon"><use xlink:href="#icon-google"></use></svg>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="nx-socials-title">Или</div>
                
                <form action="">
                    <div class="form-group form-group_short form-group_small-offset" data-form-group>
                        <label class="nx-dynamic-label" data-dynamic-label>
                            <input type="text"
                                   class="nx-dynamic-label__input nx-form-element"
                                   name="phone"
                                   data-phone-mask
                                   data-dynamic-inp>
                
                            <span class="nx-dynamic-label__text">Телефон</span>
                        </label>
                    </div>
                    
                    <div class="form-group form-group_short form-group_small-offset" data-form-group>
                        <label class="nx-dynamic-label" data-dynamic-label>
                            <input type="text"
                                   class="nx-dynamic-label__input nx-form-element"
                                   name="email"
                                   data-dynamic-inp>
                            
                            <span class="nx-dynamic-label__text">E-mail</span>
                        </label>
                    </div>
                    
                    <div class="form-group form-group_short form-group_small-offset" data-form-group data-password-wr>
                        <label class="nx-dynamic-label"
                               data-dynamic-label>
                            <input type="password"
                                   class="nx-dynamic-label__input nx-form-element"
                                   name="password"
                                   data-password-input
                                   data-dynamic-inp>
                            
                            <span class="nx-dynamic-label__text">Пароль</span>
                            
                            <a href="#" class="nx-password" data-password-link>
                                <svg class="nx-password__icon"><use xlink:href="#icon-password-closed"></use></svg>
                            </a>
                        </label>
        
                        <div class="form-group__label">Может быть любой, но лучше - надежный</div>
                    </div>
                    
                    <div class="form-group form-group_short form-group_small-offset" data-form-group data-password-wr>
                        <label class="nx-dynamic-label"
                               data-dynamic-label>
                            <input type="password"
                                   class="nx-dynamic-label__input nx-form-element"
                                   name="password"
                                   data-password-input
                                   data-dynamic-inp>
                
                            <span class="nx-dynamic-label__text">Повторите пароль</span>
                            
                            <a href="#" class="nx-password" data-password-link>
                                <svg class="nx-password__icon"><use xlink:href="#icon-password-closed"></use></svg>
                            </a>
                        </label>
                    </div>

                    <div class="nx-section__element">
                        <div class="nx-actions nx-actions_offset-top">
                        <div class="nx-actions__item">
                            <button type="submit"
                                    class="btn btn_d-block-xs"
                                    data-send-request="signin">Зарегистрироваться</button>
                        </div>
            
                        <div class="nx-actions__item">
                            <span class="note">Уже есть аккаунт?</span>
                
                            <a href="#" class="link link_small">
                                <span>Вход</span>
                            </a>
                        </div>
                    </div>
                    </div>
                    
                    <div class="note">Нажимая на кнопку "Зарегистрироваться", вы даете согласие на обработку
                        <a href="/politics/" class="link">
                            <span>персональных данных</span>
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <? require_once '../templates/_blocks/footer.php'; ?>
</div>

<script src="/assets/app.min.js"></script>
</body>
</html>

