<!DOCTYPE HTML>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta name="author" content="Контент">
    <meta name="keywords" content="keywords">
    <meta name="description" content="description">

    <title>Страница вакансии</title>

    <link rel="icon" type="image/png" href="/favicon.png">
    <link rel="stylesheet" href="/assets/vendor.css">
    <link rel="stylesheet" href="/assets/app.min.css">

    <meta property="og:title" content="title"/>
    <meta property="og:description" content="description"/>
    <meta property="og:image" content="favicon.png">

</head>
<body>

<? require_once '../templates/_blocks/header.php'; ?>

<div class="wrapper">
    <div class="wrapper__content">
        <section class="nx-section nx-section_page container">
            <div class="nx-breadcrumbs">
                <div class="nx-breadcrumbs__item">
                    <a href="#" class="nx-breadcrumbs__link">
                        <span>Вакансии</span>
                    </a>
                    
                    <i class="nx-breadcrumbs__arrow mdi mdi-chevron-right"></i>
                </div>
            </div>

            <div class="nx-section__head content-narrow-med">
                <h2 class="nx-section__title">Кладовщик / Комплектовщик</h2>
                
                <div class="nx-section__text text text_dark-fade">Комплектование товара с помощью голосового терминала;
                    транспортировка товара к
                    месту отгрузки при помощи гидравлической / электрической тележки. Осуществление своевременной и
                    качественной
                    работы в поставленные сроки
                </div>
            </div>

            <div class="flex-row">
                <div class="flex-col md-14 nx-section__col">
                    <div class="text text_styled-list">
                        <h5>Обязанности:</h5>
                        <ul>
                            <li>Проверка и оформление счетов на оплату оборудования и расходных материалов</li>
                            <li>Проверка и оформление счетов на оплату оборудования и расходных материалов</li>
                            <li>Проверка и оформление счетов на оплату оборудования и расходных материалов</li>
                        </ul>
                        <h5>Требования:</h5>
                        <ul>
                            <li>Проверка и оформление счетов на оплату оборудования и расходных материалов</li>
                            <li>Проверка и оформление счетов на оплату оборудования и расходных материалов</li>
                            <li>Проверка и оформление счетов на оплату оборудования и расходных материалов</li>
                        </ul>
                        <h5>Условия:</h5>
                        <ul>
                            <li>Проверка и оформление счетов на оплату оборудования и расходных материалов</li>
                            <li>Проверка и оформление счетов на оплату оборудования и расходных материалов</li>
                            <li>Проверка и оформление счетов на оплату оборудования и расходных материалов</li>
                        </ul>
                    </div>
                </div>
                <div class="flex-col md-10 nx-section__col">
                    <form action="/request/" class="nx-form">
                        <input type="hidden"
                               name="vacancy_id"
                               value="1">
                        
                        <div class="nx-section__item">
                            <div class="nx-form__content">
                                <h4 class="nx-form__title">Откликнуться</h4>
                                
                                <div class="nx-form__text text text_dark-fade">Заполните поля для отклика на вакансию,
                                    обычно отвечаем в течение недели
                                </div>
                            </div>
                            
                            <div class="form-group" data-form-group>
                                <label class="nx-dynamic-label" data-dynamic-label>
                                    <input type="text"
                                           class="nx-dynamic-label__input nx-form-element"
                                           name="name"
                                           data-dynamic-inp>
                                    
                                    <span class="nx-dynamic-label__text">Ф.И.О. </span>
                                </label>
                            </div>
                            
                            <div class="form-group" data-form-group>
                                <label class="nx-dynamic-label" data-dynamic-label>
                                    <input type="text"
                                           class="nx-dynamic-label__input nx-form-element"
                                           name="phone"
                                           data-dynamic-inp
                                           data-phone-mask>
                                    
                                    <span class="nx-dynamic-label__text">Телефон</span>
                                </label>
                            </div>
                            
                            <div class="form-group" data-form-group>
                                <label class="nx-dynamic-label" data-dynamic-label>
                                    <input type="text"
                                           class="nx-dynamic-label__input nx-form-element"
                                           name="email"
                                           data-dynamic-inp>
                                    
                                    <span class="nx-dynamic-label__text">E-mail</span>
                                </label>
                            </div>
                            
                            <div class="form-group" data-form-group>
                                <label class="nx-dynamic-label" data-dynamic-label>
                                
                                <textarea class="nx-dynamic-label__input nx-form-element"
                                          name="comment"
                                          rows="1"
                                          data-dynamic-inp
                                          data-autosize-textarea></textarea>
                                    
                                    <span class="nx-dynamic-label__text">Комментарий (если необходимо) </span>
                                </label>
                            </div>
                            
                            <div class="nx-files nx-files_upload" data-files>
                                <label class="nx-files__item">
                                    <span class="nx-files__link link link_br-blue link_dashed">
                                        <svg class="nx-files__icon"><use xlink:href="#icon-upload"></use></svg>
                                        
                                        <span class="nx-files__content">
                                            <span class="nx-files__title">Загрузить резюме</span>
                                            
                                            <span class="nx-files__type">jpg, png, gif, pdf, xlsx, doc, txt, до 5 мб, не более 5 файлов</span>
                                        </span>
                                    </span>
                                    
                                    <input type="file"
                                           multiple=""
                                           class="hidden"
                                           data-files-inp="file"
                                           data-accept="jpg|png|gif|pdf|xlsx|doc|txt"
                                           data-name="files[]">
                                </label>
                            </div>
                        </div>
                        
                        <div class="nx-actions nx-actions_note">
                            <div class="nx-actions__item nx-actions__item_btn">
                                <button type="submit"
                                        class="btn btn_d-block-xs"
                                        data-send-request="question">Отправить</button>
                            </div>
                            
                            <div class="nx-actions__item">
                                <div class="note">Нажимая на кнопку "Отправить", вы даете согласие на обработку
                                    <a href="/politics/" class="link">
                                        <span>персональных данных</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
    
    <? require_once '../templates/_blocks/footer.php'; ?>
</div>
<?
    require_once '../templates/_modals/_modal-ok.php';
?>
<script src="/assets/app.min.js"></script>
</body>
</html>


