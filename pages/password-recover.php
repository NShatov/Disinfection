<!DOCTYPE HTML>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <meta name="author" content="Контент">
    <meta name="keywords" content="keywords">
    <meta name="description" content="description">
    
    <title>Корзина оформление заказа</title>
    
    <link rel="icon" type="image/png" href="/favicon.png">
    <link rel="stylesheet" href="/assets/vendor.css">
    <link rel="stylesheet" href="/assets/app.min.css">
    
    <meta property="og:title" content="title"/>
    <meta property="og:description" content="description"/>
    <meta property="og:image" content="favicon.png">

</head>
<body>

<? require_once '../templates/_blocks/header.php'; ?>

<div class="wrapper wrapper_flex">
    <div class="wrapper__content">
        <div class="flex-row container">
            <div class="flex-col md-8"></div>
            
            <div class="flex-col md-16">
                <h3 class="nx-section__title">Восстановление пароля</h3>
                
                <form action="">
                    <div class="form-group" data-form-group>
                        <label class="nx-dynamic-label" data-dynamic-label>
                            <input type="text"
                                   class="nx-dynamic-label__input nx-form-element nx-form-element_short"
                                   name="email"
                                   data-dynamic-inp>
                    
                            <span class="nx-dynamic-label__text">E-mail</span>
                        </label>
                        <div class="form-group__label">Введите e-mail, указанный вами при регистрации на сайте</div>
                    </div>
            
                    <div class="nx-promo-slider__actions">
                        <div class="nx-actions">
                            <div class="nx-actions__item">
                                <button type="submit"
                                        class="btn btn_d-block-xs"
                                        data-send-request="signin">Восстановить пароль</button>
                            </div>
                            
                            <div class="nx-actions__item">
                                <a href="#" class="link link_small">
                                    <span>Отмена</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <? require_once '../templates/_blocks/footer.php'; ?>
</div>

<script src="/assets/app.min.js"></script>
</body>
</html>
