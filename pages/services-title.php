<!DOCTYPE HTML>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta name="author" content="Контент">
    <meta name="keywords" content="keywords">
    <meta name="description" content="description">

    <title>Услуги страница с заголовком</title>

    <link rel="icon" type="image/png" href="/favicon.png">
    <link rel="stylesheet" href="/assets/vendor.css">
    <link rel="stylesheet" href="/assets/app.min.css">

    <meta property="og:title" content="title"/>
    <meta property="og:description" content="description"/>
    <meta property="og:image" content="favicon.png">

</head>
<body>

<? require_once '../templates/_blocks/header.php'; ?>

<div class="wrapper">
    <div class="wrapper__content">
        <div class="nx-section container">
            <div class="nx-breadcrumbs">
                <div class="nx-breadcrumbs__item">
                    <a href="#" class="nx-breadcrumbs__link">
                        <span>Услуги</span>
                    </a>
                    
                    <i class="nx-breadcrumbs__arrow mdi mdi-chevron-right"></i>
                </div>
            </div>

            <div class="nx-section__head content-narrow-med">
                <div class="h3 nx-section__title">Дизайн рекламы</div>
                
                <div class="text text_dark-fade nx-section__text">Создание или разработка дизайн макета является
                    первоначальным этапом в изготовление рекламы. Главная задача любой рекламы – привлечение внимания
                    потенциальных покупателей и побуждение к действию
                </div>
            </div>

            <div class="nx-section__head">
                <div class="flex-row">
                    <div class="flex-col md-16 nx-section__col">
                        <div class="nx-gal-slider nx-gal-slider_small not-visible" data-gal>
                            <div class="nx-gal-slider__main" data-gal-main>
                                <div class="swiper-wrapper nx-gal-slider__main-wrap">
                                    <? for ($i = 0; $i < 10; $i++): ?>
                                        <a href="https://via.placeholder.com/1920x1080"
                                           class="swiper-slide nx-gal-slider__main-slide"
                                           data-fancybox="gal">
                                            <img src="https://via.placeholder.com/1160x650"
                                                 alt=""
                                                 class="nx-gal-slider__main-img">
                                        </a>
                                    <? endfor; ?>
                                </div>

                                <div class="nx-gal-slider__prev" data-gal-prev>
                                    <svg class="nx-gal-slider__prev-icon"><use xlink:href="#icon-arrow"></use></svg>
                                </div>
                                
                                <div class="nx-gal-slider__next nx-gal-slider__next" data-gal-next>
                                    <svg class="nx-gal-slider__next-icon"><use xlink:href="#icon-arrow"></use></svg>
                                </div>
                            </div>

                            <div class="nx-gal-slider__thumbs" data-gal-thumb>
                                <div class="swiper-wrapper nx-gal-slider__thumbs-wrap">
                                    <? for ($i = 0; $i < 10; $i++): ?>
                                        <div class="swiper-slide nx-gal-slider__thumbs-slide"
                                             data-gal-thumb-slide
                                             style="background-image: url('https://via.placeholder.com/200x110');"></div>
                                    <? endfor; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="flex-col md-8 nx-section__col">
                        <div class="nx-section__box">
                            <div class="nx-actions nx-actions_col">
                                <div class="nx-actions__item h4">От 1500
                                    <i class="rub">q</i>
                                </div>
                                
                                <div class="nx-actions__item">
                                    <a href="#modal-order"
                                       class="btn btn_d-block-xs"
                                       data-toggle="modal">Сделать заказ</a>
                                </div>
                            </div>
                        </div>
                        
                        <div class="nx-section__box">
                            <div class="nx-advans nx-advans_row flex-row">
                                <? for ($i = 0; $i < 3; $i++): ?>
                                    <div class="nx-advans__item flex-col md-24 sm-12">
                                        <div class="nx-advans__icon" style="background-image: url('https://via.placeholder.com/100x100');"></div>
                                        
                                        <div class="nx-advans__text">Бесплатный выезд на замер</div>
                                    </div>
                                <? endfor; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="text text_styled-list content-narrow-med">
                <h5>Описание</h5>
                <p>Создание или разработка дизайн макета является первоначальным этапом в изготовление рекламы. Главная
                    задача любой рекламы – привлечение внимания потенциальных покупателей и побуждение к действию.
                    Продуманный дизайн листовок, баннеров, щитов даст хороший эффект и поможет вам продвинуть свой
                    бизнес. В ходе разработки макета широкоформатная печать и полиграфия требует к себе определенного
                    подхода.Красивый дизайн также сможет создать положительный имидж вашей организации и показать
                    потребителям, что вы серьезно относитесь к своему делу </p>
                <h5>Маркированный список</h5>
                <ul>
                    <li>Защита поверхности от воздействия внешней среды (в первую очередь от влаги-от дождя и пролитого
                        кофе) и защита красочного слоя от механических повреждений (царапины, соскобов и пр.)
                    </li>
                    <li>Защита поверхности от воздействия внешней среды (в первую очередь от влаги-от дождя и пролитого
                        кофе) и защита красочного слоя от механических повреждений (царапины, соскобов и пр.)
                    </li>
                </ul>
                <h5>Нумированный список</h5>
                <ol>
                    <li>Дизайн рекламы</li>
                    <li>Дизайн рекламы</li>
                </ol>
                <h5>Таблица</h5>
                <table>
                    <thead>
                    <tr>
                        <th>Задача</th>
                        <th>Цена (руб)</th>
                    </tr>
                    </thead>
                    <tbody>
                    <? for ($i = 0; $i < 3; $i++): ?>
                        <tr>
                            <td>
                                Календарь перикидной
                            </td>
                            <td>
                                1500
                            </td>
                        </tr>
                    <? endfor; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
    <? require_once '../templates/_blocks/footer.php'; ?>
</div>

<script src="/assets/app.min.js"></script>
</body>
</html>
