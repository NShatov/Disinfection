<!DOCTYPE HTML>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <meta name="author" content="Контент">
    <meta name="keywords" content="keywords">
    <meta name="description" content="description">
    
    <title>Главная</title>
    
    <link rel="icon" type="image/png" href="/favicon.png">
    <link rel="stylesheet" href="/assets/vendor.css">
    <link rel="stylesheet" href="/assets/app.min.css">
    
    <meta property="og:title" content="title"/>
    <meta property="og:description" content="description"/>
    <meta property="og:image" content="favicon.png">

</head>
<body>

<? require_once '../templates/_blocks/header.php'; ?>

<div class="wrapper">
    <div class="wrapper__content">
        <div class="nx-section nx-section_page">
            <div class="container">
                <div class="nx-section__element-inner">
                    <h4 class="nx-section__title">Личный кабинет</h4>

                    <div class="nx-personal-actions">
                        <div class="nx-toggle-button">
                            <input type="checkbox"
                                   name="toggle"
                                   id="toggle-button"
                                   class="nx-toggle-button__input"
                                   data-mailing-toggle
                                   data-url="">

                            <label for="toggle-button" class="nx-toggle-button__text">Подписка на рассылку</label>
                        </div>

                        <a href="" class="link link_exit">
                            <svg class="link-icon"><use xlink:href="#icon-exit"></use></svg>

                            <span>Выйти</span>
                        </a>
                    </div>
                </div>

                <div class="nx-tabs" data-tabs-slider>
                    <div class="swiper-wrapper nx-tabs__wrap">
                        <div class="swiper-slide nx-tabs__item is-active">
                            <a href="#"
                               class="nx-tabs__link">
                                <span>Мои заказы</span>
                            </a>
                        </div>

                        <div class="swiper-slide nx-tabs__item">
                            <a href="#"
                               class="nx-tabs__link">
                                <span>Избранное</span>
                            </a>
                        </div>

                        <div class="swiper-slide nx-tabs__item">
                            <a href="#"
                               class="nx-tabs__link">
                                <span>Личные данные</span>
                            </a>
                        </div>
                    </div>
                </div>
                
                <div class="nx-orders content-narrow-med">
                    <div class="nx-order-filter">
                        <div class="nx-order-filter__actions">
                            <div class="nx-order-filter__item">
                                <div class="nx-order-filter__title">Дата:</div>
                                
                                <div class="nx-order-filter__date">
                                    c <a href="#" class="nx-order-filter__date-link">
                                         <svg class="link-icon"><use xlink:href="#icon-calendar"></use></svg>
                                        
                                        <span>01.03.20</span>
                                    </a>
                                </div>
                                
                                <div class="nx-order-filter__date">
                                    по <a href="#" class="nx-order-filter__date-link">
                                         <svg class="link-icon"><use xlink:href="#icon-calendar"></use></svg>
                                        
                                        <span>01.03.20</span>
                                    </a>
                                </div>
                            </div>
                            
                            <div class="nx-order-filter__item">
                                <div class="nx-order-filter__title">Статус заказа:</div>
    
                                <div class="nx-order-filter__select">
                                    <select id=""
                                            name=""
                                            data-custom-select
                                            data-placeholder="Любой">
                                        <option value="1">Любой</option>
    
                                        <option value="1">Оформлен</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        <form action="" class="nx-order-filter__search">
                            <input type="text"
                                   name="q"
                                   class="nx-order-filter__search-input"
                                   data-search-inp
                                   placeholder="Поиск по заказам">
                            
                            <a href="#" class="nx-order-filter__search-link">
                                <svg class="nx-order-filter__search-icon"><use xlink:href="#icon-search"></use></svg>
                            </a>
                        </form>
                        
                        <a href="" class="link link_clear">
                            <span>Сбросить</span>

                            <svg class="link-icon"><use xlink:href="#icon-close"></use></svg>
                        </a>
                    </div>
                    
                    <div class="nx-section__item">
                        <div class="nx-order-head">
                            <div class="nx-order-head__item nx-order-head__item-number">Номер заказ</div>
    
                            <div class="nx-order-head__item nx-order-head__item-date">Дата</div>
    
                            <div class="nx-order-head__item nx-order-head__item-total">Сумма</div>
    
                            <div class="nx-order-head__item nx-order-head__item-status">Статус</div>
                        </div>
                        
                        <? for ($i = 0; $i < 3; $i++): ?>
                        <div class="nx-orders__row">
                            <a href="#collapse-item-<?echo $i?>"
                               class="nx-order-link link collapsed"
                               data-toggle="collapse">
                                <div class="nx-order-link__item nx-order-link__item-number">100172730</div>
                                
                                <div class="nx-order-link__item nx-order-link__item-date">20.03.20</div>
                                
                                <div class="nx-order-link__item nx-order-link__item-total">9 462 000 <i class="rouble">c</i></div>
                                
                                <div class="nx-order-link__item nx-order-link__item-status">Готов к отгрузке</div>
                                
                                <span class="spacer"></span>
                                
                                <div class="nx-order-link__details">
                                    <span>Подробнее</span>
                                    
                                    <svg class="nx-order-link__arrow"><use xlink:href="#icon-arrow-top"></use></svg>
                                </div>
                            </a>
                            
                            <div class="collapse in" id="collapse-item-<?echo  $i?>">
                                <div class="nx-order">
                                    <? for ($j = 0; $j < 3; $j++): ?>
                                        <div class="nx-order__item">
                                            <a href="" class="nx-order__title">
                                                <span>Баня Б-150 4х4 М с террасой и большой верандой</span>
                                            </a>
                                            
                                            <div class="nx-order__price">1 577 000 <i class="rouble">c</i>
                                                
                                                <div class="nx-order__price-note">1 шт</div>
                                            </div>
                                        </div>
                                    <? endfor; ?>
                                    <div class="nx-order-total">
                                        <div class="nx-order-total__item">
                                            <div class="nx-order-total__label">Доставка:</div>
                                            
                                            <div class="nx-order-total__type">Псков, Алтаева 54, дом 15</div>
                                        </div>
                                        
                                        <div class="nx-order-total__item">
                                            <div class="nx-order-total__label">Стоимость доставки:</div>
                                            
                                            <div class="nx-order-total__type">350 <i class="rouble">q</i></div>
                                        </div>
                                        
                                        <div class="nx-order-total__item">
                                            <div class="nx-order-total__label">Вес:</div>
                                            
                                            <div class="nx-order-total__type">58,83 кг</div>
                                        </div>
                                        
                                        <div class="spacer"></div>
                                        
                                        <div class="nx-order-total__item">
                                            <div class="nx-order-total__label">Итого, 4 товара + доставка:</div>
                                            
                                            <div class="nx-order-total__price">9 462 000 <i class="rouble">c</i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <? endfor; ?>
                    </div>
                    
                    <div class="nx-actions nx-actions_center nx-actions_single">
                        <div class="nx-actions__item">
                            <a href=""
                               class="btn btn_br"
                               data-show-more
                               data-show-more-url>Показать еще заказы</a>
                        </div>

                        <div class="nx-pagination nx-pagination_single">
                            <a href="#" class="nx-pagination__arrow nx-pagination__arrow_prev disabled">
                                <svg class="nx-pagination__arrow-icon"><use xlink:href="#icon-arrow"></use></svg>
                            </a>

                            <div class="nx-pagination__item active">
                                <a href="#" class="nx-pagination__link link link_invert">
                                    <span>1</span>
                                </a>
                            </div>

                            <div class="nx-pagination__item">
                                <a href="#" class="nx-pagination__link link link_invert">
                                    <span>2</span>
                                </a>
                            </div>

                            <div class="nx-pagination__item nx-pagination__item_dots">
                                <div class="nx-pagination__dots">...</div>
                            </div>

                            <div class="nx-pagination__item">
                                <a href="#" class="nx-pagination__link link link_invert">
                                    <span>9</span>
                                </a>
                            </div>

                            <div class="nx-pagination__item">
                                <a href="#" class="nx-pagination__link link link_invert">
                                    <span>10</span>
                                </a>
                            </div>

                            <a href="#" class="nx-pagination__arrow nx-pagination__arrow_next">
                                <svg class="nx-pagination__arrow-icon"><use xlink:href="#icon-arrow"></use></svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <? require_once '../templates/_blocks/footer.php'; ?>
</div>

<script src="/assets/app.min.js"></script>
</body>
</html>
