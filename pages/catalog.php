<!DOCTYPE HTML>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <meta name="author" content="Контент">
    <meta name="keywords" content="keywords">
    <meta name="description" content="description">
    
    <title>Каталог</title>
    
    <link rel="icon" type="image/png" href="/favicon.png">
    <link rel="stylesheet" href="/assets/vendor.css">
    <link rel="stylesheet" href="/assets/app.min.css">
    
    <meta property="og:title" content="title"/>
    <meta property="og:description" content="description"/>
    <meta property="og:image" content="favicon.png">

</head>
<body>

<div class="wrapper">
    <div class="wrapper__content">
        <div class="nx-section container">
            <div class="nx-section__head">
                <h2 class="nx-section__title">Каталог</h2>
            </div>
            
            <div class="nx-section__row">
                <div class="flex-row nx-listing">
                    <? for ($i = 0; $i < 3; $i++): ?>
                        <div class="flex-col md-6 sm-8 nx-listing__item">
                            <a href="#" class="nx-listing__link">
                                <div class="nx-listing__head">
                                    <img src="https://via.placeholder.com/560x560"
                                         alt=""
                                         class="nx-listing__img">
                                </div>
                                
                                <div class="nx-listing__body">
                                    <span class="nx-listing__title">Дачные дома</span>
                                </div>
                            </a>
                        </div>
                    <? endfor; ?>
                </div>
            </div>
        </div>
    </div>
    <? require_once '../templates/_blocks/footer.php'; ?>
</div>

<script src="/assets/app.min.js"></script>
</body>
</html>

