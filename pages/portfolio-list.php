<!DOCTYPE HTML>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta name="author" content="Контент">
    <meta name="keywords" content="keywords">
    <meta name="description" content="description">

    <title>Портфолио список</title>

    <link rel="icon" type="image/png" href="/favicon.png">
    <link rel="stylesheet" href="/assets/vendor.css">
    <link rel="stylesheet" href="/assets/app.min.css">

    <meta property="og:title" content="title"/>
    <meta property="og:description" content="description"/>
    <meta property="og:image" content="favicon.png">

</head>
<body>

<? require_once '../templates/_blocks/header.php'; ?>

<div class="wrapper">
    <div class="wrapper__content">
        <section class="nx-section container">
            <div class="nx-section__head content-narrow-med">
                <h2 class="nx-section__title">Портфолио</h2>
                <div class="nx-section__text text text_dark-fade">Крупнейший интернет-магазин модной одежды, обуви,
                    аксессуаров, косметических средств, существующий уже 15 лет! Из года в год мы продолжаем
                    развиваться, расширять географию присутствия и улучшать качество обслуживания, чтобы радовать Вас
                    каждый день!
                </div>
            </div>

            <div class="nx-tabs" data-tabs-slider>
                <div class="swiper-wrapper nx-tabs__wrap">
                    <div class="swiper-slide nx-tabs__item is-active">
                        <a href="#"
                           class="nx-tabs__link"
                           data-nx-tabs="content-load"
                           data-load-id="123"
                           data-nx-url="">
                            <span>Все вместе</span>
                        </a>
                    </div>
                    
                    <div class="swiper-slide nx-tabs__item">
                        <a href="#"
                           class="nx-tabs__link"
                           data-nx-tabs="content-load"
                           data-load-id="123"
                           data-nx-url="">
                            <span>Наружная реклама</span>
                        </a>
                    </div>
                    
                    <div class="swiper-slide nx-tabs__item">
                        <a href="#"
                           class="nx-tabs__link"
                           data-nx-tabs="content-load"
                           data-load-id="123"
                           data-nx-url="">
                            <span>Полиграфия</span>
                        </a>
                    </div>
                </div>
            </div>

            <div class="nx-section__item">
                <div class="nx-listing nx-listing_row">
                    <? for ($i = 0; $i < 8; $i++): ?>
                        <div class="nx-listing__item">
                            <a href="#" class="nx-listing__link">
                                <div class="nx-listing__head">
                                    <img src="https://via.placeholder.com/1160x650" alt="" class="nx-listing__img">
                                </div>
                                
                                <div class="nx-listing__body">
                                    <span class="nx-listing__title">Полиграфическая продукция к мероприятию “Пушкинские дни”</span>
                                    
                                    <div class="nx-list-titles">
                                        <div class="nx-list-titles__row">
                                            <div class="nx-list-titles__main">Заказчик:</div>
                                            
                                            <div class="nx-list-titles__text">ООО “Моя история”</div>
                                        </div>
                                        
                                        <div class="nx-list-titles__row">
                                            <div class="nx-list-titles__main">Заказчик:</div>
                                            
                                            <div class="nx-list-titles__text">ООО “Моя история”</div>
                                        </div>
                                    </div>
                                    
                                    <div class="nx-listing__text">Крупнейший интернет-магазин модной одежды, обуви,
                                        аксессуаров, косметических средств, существующий уже 15 лет! Из года в год мы
                                        продолжаем развиваться, расширять географию присутствия и улучшать качество
                                        обслуживания, чтобы радовать Вас каждый день!
                                    </div>
                                    
                                    <div class="nx-tags">
                                        <div class="nx-tags__item">Полиграфия</div>
                                        
                                        <div class="nx-tags__item">Брендирование</div>
                                        
                                        <div class="nx-tags__item">Визитки</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <? endfor; ?>
                </div>
            </div>

            <div class="nx-pagination">
                <a href="#" class="nx-pagination__arrow nx-pagination__arrow_prev disabled">
                    <svg class="nx-pagination__arrow-icon"><use xlink:href="#icon-arrow"></use></svg>
                </a>

                <div class="nx-pagination__item active">
                    <a href="#" class="nx-pagination__link link link_invert">
                        <span>1</span>
                    </a>
                </div>
                
                <div class="nx-pagination__item">
                    <a href="#" class="nx-pagination__link link link_invert">
                        <span>2</span>
                    </a>
                </div>

                <div class="nx-pagination__item nx-pagination__item_dots">
                    <div class="nx-pagination__dots">...</div>
                </div>

                <div class="nx-pagination__item">
                    <a href="#" class="nx-pagination__link link link_invert">
                        <span>9</span>
                    </a>
                </div>
                
                <div class="nx-pagination__item">
                    <a href="#" class="nx-pagination__link link link_invert">
                        <span>10</span>
                    </a>
                </div>

                <a href="#" class="nx-pagination__arrow nx-pagination__arrow_next">
                    <svg class="nx-pagination__arrow-icon"><use xlink:href="#icon-arrow"></use></svg>
                </a>
            </div>
        </section>
    </div>
    
    <? require_once '../templates/_blocks/footer.php'; ?>
</div>

<script src="/assets/app.min.js"></script>
</body>
</html>
