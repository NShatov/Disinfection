<!DOCTYPE HTML>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta name="author" content="Контент">
    <meta name="keywords" content="keywords">
    <meta name="description" content="description">

    <title>Страница новости</title>

    <link rel="icon" type="image/png" href="/favicon.png">
    <link rel="stylesheet" href="/assets/vendor.css">
    <link rel="stylesheet" href="/assets/app.min.css">

    <meta property="og:title" content="title"/>
    <meta property="og:description" content="description"/>
    <meta property="og:image" content="favicon.png">

</head>
<body>

<? require_once '../templates/_blocks/header.php'; ?>

<div class="wrapper">
    <div class="wrapper__content">
        <div class="nx-section nx-section_page container">
            <div class="nx-breadcrumbs">
                <div class="nx-breadcrumbs__item">
                    <a href="#" class="nx-breadcrumbs__link">
                        <span>Новости</span>
                    </a>
                    
                    <i class="nx-breadcrumbs__arrow mdi mdi-chevron-right"></i>
                </div>
            </div>

            <div class="content-narrow-med">
                <div class="nx-section__date">
                    <div class="date">29.01.20</div>
                </div>
                
                <div class="nx-section__head">
                    <h1>Распродажа зимней коллекции 2020 в самом разгаре</h1>
                </div>
                
                <div class="nx-section__box">
                    <div class="nx-gal-slider not-visible" data-gal>
                        <div class="nx-gal-slider__main" data-gal-main>
                            <div class="swiper-wrapper nx-gal-slider__main-wrap">
                                <!--Video-->
                                <div class="swiper-slide nx-gal-slider__main-slide nx-video">
                                    <a href="#"
                                       class="nx-video__link"
                                       style="background-image: url('https://via.placeholder.com/1980x1200')"
                                       data-video-load="mgcQDHpPVLo"
                                       data-video-content>
                                        <svg class="nx-video__icon"><use xlink:href="#icon-play"></use></svg>
                                    </a>
                                </div>
                                
                                <? for ($i = 0; $i < 10; $i++): ?>
                                    <a href="https://via.placeholder.com/1920x1080"
                                       class="swiper-slide nx-gal-slider__main-slide"
                                       data-fancybox="gal">
                                        <img src="https://via.placeholder.com/1980x1200"
                                             alt=""
                                             class="nx-gal-slider__main-img">
                                    </a>
                                <? endfor; ?>
                            </div>

                            <div class="nx-gal-slider__prev" data-gal-prev>
                                <svg class="nx-gal-slider__prev-icon"><use xlink:href="#icon-arrow"></use></svg>
                            </div>
                            
                            <div class="nx-gal-slider__next nx-gal-slider__next" data-gal-next>
                                <svg class="nx-gal-slider__next-icon"><use xlink:href="#icon-arrow"></use></svg>
                            </div>
                        </div>

                        <div class="nx-gal-slider__thumbs" data-gal-thumb>
                            <div class="swiper-wrapper nx-gal-slider__thumbs-wrap">
                                <? for ($i = 0; $i < 10; $i++): ?>
                                    <div class="swiper-slide nx-gal-slider__thumbs-slide"
                                         data-gal-thumb-slide
                                         style="background-image: url('https://via.placeholder.com/260x150');"></div>
                                <? endfor; ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="text text_dark-fade">
                    Успейте к главной распродаже года, скидки до 90% на все группы товаров. Только в этом феврале.
                    Крупнейший интернет-магазин модной одежды, обуви, аксессуаров, косметических средств, существующий
                    уже 15 лет! Из года в год мы продолжаем развиваться, расширять географию присутствия и улучшать
                    качество обслуживания, чтобы радовать Вас каждый день! Равным образом сложившаяся структура
                    организации требуют от нас анализа новых предложений. Значимость этих проблем настолько очевидна,
                    что дальнейшее развитие различных форм деятельности влечет за собой процесс внедрения и модернизации
                    направлений прогрессивного развития. Равным образом сложившаяся структура организации играет важную
                    роль в формировании новых предложений. Не следует, однако забывать, что постоянное
                    информационно-пропагандистское
                </div>
                <div class="nx-share">
                    <div class="nx-share__label">Поделитесь с друзьями:</div>
                    
                    <div class="likely likely_dnext">
                        <div class="facebook">Поделиться</div>
                        
                        <div class="vkontakte">Поделиться</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <? require_once '../templates/_blocks/footer.php'; ?>
</div>

<script src="/assets/app.min.js"></script>
</body>
</html>

