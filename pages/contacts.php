<!DOCTYPE HTML>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta name="author" content="Контент">
    <meta name="keywords" content="keywords">
    <meta name="description" content="description">

    <title>Контакты</title>

    <link rel="icon" type="image/png" href="/favicon.png">
    <link rel="stylesheet" href="/assets/vendor.css">
    <link rel="stylesheet" href="/assets/app.min.css">

    <meta property="og:title" content="title"/>
    <meta property="og:description" content="description"/>
    <meta property="og:image" content="favicon.png">

</head>
<body data-home="">

<? require_once '../templates/_blocks/header.php'; ?>

<div class="wrapper">
    <div class="wrapper__content">
        <div class="nx-section contacts">
            <div class="container">
                <div class="nx-section__head">
                    <h1 class="nx-section__title">Контакты</h1>
                    
                    <a href="#"
                       data-custom-open="modal-requisites"
                       class="nx-section__btn btn btn_d-block-xs btn_br"
                       data-toggle="modal">Смотреть реквизиты</a>
                </div>

                <div class="flex-row nx-section__col">
                    <div class="flex-col md-12 nx-section__box">
                        <div class="nx-contacts flex-row">
                            <div class="flex-col md-12 sm-12 nx-contacts__item">
                                <div class="nx-contacts__title h4">Адреса:</div>
                                
                                <div class="nx-contacts__item">
                                    <div class="nx-contacts__label">Магазин</div>
                                    
                                    <div class="nx-contacts__text">180019, г.Псков, ул.Юности, д.11, офис 111</div>
                                    
                                    <a href="#map"
                                       class="link"
                                       data-scroll-btn>
                                        <span>Смотреть на карте </span>
                                    </a>
                                </div>
                                
                                <div class="nx-contacts__item">
                                    <div class="nx-contacts__label">Магазин</div>
                                    
                                    <div class="nx-contacts__text">180019, г.Псков, ул.Юности, д.11, офис 111</div>
                                    
                                    <a href="#map"
                                       class="link"
                                       data-scroll-btn>
                                        <span>Смотреть на карте </span>
                                    </a>
                                </div>
                            </div>
                            
                            <div class="flex-col md-12 sm-12 nx-contacts__item">
                                <div class="nx-contacts__title h4">Телефоны:</div>
                                
                                <div class="nx-contacts__item">
                                    <div class="nx-contacts__label">Магазин</div>
                                    
                                    <a href="" class="nx-contacts__link">+7 (911) 544 54 54</a>
                                    
                                    <a href="" class="nx-contacts__link">+7 (911) 544 54 54</a>
                                </div>
                                
                                <div class="nx-contacts__item">
                                    <div class="nx-contacts__label">Отдел кадров</div>
                                    <a href=""
                                       target="_blank"
                                       rel="nofollow noopener"
                                       class="nx-contacts__link">+7 (911) 544 54 54</a>
                                </div>
                            </div>
                            
                            <div class="flex-col md-12 sm-12 nx-contacts__item">
                                <div class="nx-contacts__title h4">E-mail:</div>
                                
                                <div class="nx-contacts__item">
                                    <div class="nx-contacts__label">По общим вопросам</div>
                                    
                                    <a href="mailto:test@test.ru"
                                       target="_blank"
                                       rel="nofollow noopener"
                                       class="nx-contacts__link">test@test.ru</a>
                                    
                                    <a href="mailto:test@test.ru"
                                       target="_blank"
                                       rel="nofollow noopener"
                                       class="nx-contacts__link">test@test.ru</a>
                                </div>
                                
                                <div class="nx-contacts__item">
                                    <div class="nx-contacts__label">Отдел кадров</div>
                                    
                                    <a href="mailto:test@test.ru"
                                       target="_blank"
                                       rel="nofollow noopener"
                                       class="nx-contacts__link">test@test.ru</a>
                                </div>
                            </div>
                            
                            <div class="flex-col md-12 sm-12 nx-contacts__item">
                                <div class="nx-contacts__title h4">Режим работы:</div>
                                
                                <div class="nx-contacts__item">
                                    <div class="nx-contacts__label">Пн - Пт</div>
                                    <div class="nx-contacts__text">9:00 - 18:00</div>
                                </div>
                                
                                <div class="nx-contacts__item">
                                    <div class="nx-contacts__label">Сб - Вс</div>
                                    <div class="nx-contacts__text">9:00 - 17:00</div>
                                </div>
                            </div>
                            
                            <div class="flex-col md-12 sm-12 nx-contacts__item">
                                <div class="nx-contacts__item">
                                    <div class="nx-contacts__label">Больше скидок в наших соц. сетях</div>
                                    
                                    <div class="nx-socials">
                                        <div class="nx-socials__item">
                                            <a href=""
                                               class="nx-socials__link nx-socials__link_vk"
                                               target="_blank"
                                               rel="noopener">
                                                <svg class="nx-socials__icon"><use xlink:href="#icon-vk"></use></svg>
                                            </a>
                                        </div>
                                        
                                        <div class="nx-socials__item">
                                            <a href=""
                                               class="nx-socials__link nx-socials__link_fb"
                                               target="_blank"
                                               rel="noopener">
                                                <svg class="nx-socials__icon"><use xlink:href="#icon-fb"></use></svg>
                                            </a>
                                        </div>
                                        
                                        <div class="nx-socials__item">
                                            <a href=""
                                               class="nx-socials__link nx-socials__link_inst"
                                               target="_blank"
                                               rel="noopener">
                                                <svg class="nx-socials__icon"><use xlink:href="#icon-inst"></use></svg>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="flex-col md-12 nx-section__box">
                        <form action="/request/" class="nx-form">
                            <div class="nx-form__content">
                                <h4 class="nx-form__title">Есть вопросы? Мы ответим </h4>
                                
                                <div class="nx-form__text text text_dark-fade">Пишите по любым интересующим вас
                                    вопросам. Наши специалисты обработают вашу
                                    заявку и ответят в течение рабочего дня
                                </div>
                            </div>
                            
                            <div class="form-group" data-form-group>
                                <label class="nx-dynamic-label" data-dynamic-label>
                                    <input type="text"
                                           class="nx-dynamic-label__input nx-form-element"
                                           name="name"
                                           data-dynamic-inp>
                                    <span class="nx-dynamic-label__text">Ф.И.О. или компания </span>
                                </label>
                            </div>
                            
                            <div class="form-group" data-form-group>
                                <label class="nx-dynamic-label" data-dynamic-label>
                                    <input type="text"
                                           class="nx-dynamic-label__input nx-form-element"
                                           name="phone"
                                           data-dynamic-inp
                                           data-phone-mask>
                                    <span class="nx-dynamic-label__text">Телефон</span>
                                </label>
                            </div>
                            
                            <div class="form-group" data-form-group>
                                <label class="nx-dynamic-label" data-dynamic-label>
                                    <input type="text"
                                           class="nx-dynamic-label__input nx-form-element"
                                           name="email"
                                           data-dynamic-inp>
                                    <span class="nx-dynamic-label__text">E-mail</span>
                                </label>
                            </div>
                            
                            <div class="form-group" data-form-group>
                                <label class="nx-dynamic-label" data-dynamic-label>
                                    <textarea class="nx-dynamic-label__input nx-form-element"
                                              name="comment"
                                              rows="1"
                                              data-dynamic-inp
                                              data-autosize-textarea></textarea>
                                    <span class="nx-dynamic-label__text">Ваш вопрос</span>
                                </label>
                            </div>
                            
                            <div class="nx-actions nx-actions_note">
                                <div class="nx-actions__item nx-actions__item_btn">
                                    <button type="submit"
                                            class="btn btn_d-block-xs"
                                            data-send-request="question">Задать вопрос</button>
                                </div>
                                
                                <div class="nx-actions__item">
                                    <div class="note">Нажимая на кнопку “Задать вопрос ” вы даете согласие на обработку
                                        <a href="/politics/"
                                           class="link">
                                            <span>персональных данных</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="flex-row nx-section__col">
                    <h2 class="nx-section__title">Мы на карте</h2>
                </div>
            </div>
            <script>
                var contactPoints =
                    {
                        title: 'ООО "Рога и копыта" ',
                        address: 'Псков, ул. Ротная, 34, 2 этаж',
                        phones: ['8 (112) 66-47-78', '8 (112) 66-47-78'],
                        emails: ['office@mppskov.ru', 'office@mppskov.ru'],
                        coords: [57.79981020228962, 28.349447879956628],
                    };
            </script>
            <div
                    id="map"
                    data-map="with-balloon"
                    data-key="9b463b43-c9e4-41ad-aafc-a04e7d4a0243"
                    data-coord-x="44.23232323"
                    data-coord-y="44.23232323"
                    class="nx-map"></div>
        </div>

    </div>
    <? require_once '../templates/_blocks/footer.php'; ?>
</div>

<?
    
    require_once '../templates/_modals/_modal-requisites.php';
?>
<script src="/assets/app.min.js"></script>
</body>
</html>
