<!DOCTYPE HTML>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <meta name="author" content="Контент">
    <meta name="keywords" content="keywords">
    <meta name="description" content="description">
    
    <title>Корзина оформление заказа</title>
    
    <link rel="icon" type="image/png" href="/favicon.png">
    <link rel="stylesheet" href="/assets/vendor.css">
    <link rel="stylesheet" href="/assets/app.min.css">
    
    <meta property="og:title" content="title"/>
    <meta property="og:description" content="description"/>
    <meta property="og:image" content="favicon.png">

</head>
<body data-home>

<? require_once '../templates/_blocks/header.php'; ?>
<? require_once '../templates/_blocks/cart_informer.php'; ?>

<div class="wrapper order-success">
    <div class="wrapper__content">
        <div class="nx-section nx-section_page container">
            <div class="nx-breadcrumbs">
                <div class="nx-breadcrumbs__item">
                    <svg class="nx-breadcrumbs__back"><use xlink:href="#icon-arrow"></use></svg>
                    
                    <a href="#" class="nx-breadcrumbs__link">
                        <span>Вернуться в корзину</span>
                    </a>
                    
                    <i class="nx-breadcrumbs__arrow mdi mdi-chevron-right"></i>
                </div>
            </div>
            
            <form action="" class="flex-row">
                <div class="nx-order-info flex-col content-narrow-med">
                    <div class="h3">Заказ успешно оформлен!</div>
                    
                    <div class="nx-order-info__note">Номер заказа: 100172730, оплата при получении</div>
                    
                    <div class="nx-order-info__text">На вашу электронную почту придет письмо с подробной информацией о заказе. При получении не забудьте предоставить паспорт или другой документ подтверждающий личность</div>
                </div>
                
                <div class="flex-col md-18 nx-section__col">
                    <div class="nx-cart-head">
                        <div class="nx-cart-head__item nx-cart-head__item_title">Товар</div>
        
                        <div class="nx-cart-head__item nx-cart-head__item_price"></div>
        
                        <div class="nx-cart-head__item nx-cart-head__item_cnt">Колличество</div>
        
                        <div class="nx-cart-head__item nx-cart-head__item_total">Сумма</div>
                    </div>
                    
                    <div class="nx-section__item">
                        <div class="nx-cart-list">
                            <? for ($i = 0; $i < 2; $i++): ?>
                                <div class="nx-cart-list__item" data-cart-el data-prod="<? echo $i ?>">
                                    <div class="nx-cart-list__main">
                                        <a href="#" class="nx-cart-list__img-link">
                                            <img src="https://via.placeholder.com/200x200"
                                                 alt=""
                                                 class="nx-cart-list__img">
                                        </a>
                        
                                        <div class="nx-cart-list__main-body">
                                            <div class="nx-cart-list__note">Артикул:
                                                <span>10073940</span>
                                            </div>
                            
                                            <a href="#" class="nx-cart-list__title">
                                                <span>Баня Б-150 4х4 М с террасой и большой верандой</span>
                                            </a>
                                        </div>
                                    </div>
        
                                    <div class="nx-cart-list__counter">
                                        <span>1</span> шт
                                    </div>
                    
                                    <div class="nx-cart-list__total">
                                        <div class="nx-cart-list__total-price">
                                            <span>1 577 000</span>
                            
                                            <i class="rub">q</i>
                                        </div>
                        
                                        <div class="nx-cart-list__total-weight">
                                            <span>14.55</span>
                                            
                                            <span>кг</span>
                                        </div>
                                    </div>
                                </div>
                            <? endfor; ?>
                        </div>
        
                        <div class="nx-cart-complects">
                        <? for ($j = 0; $j < 2; $j++): ?>
                            <div class="nx-cart-list__item">
                                <div class="nx-cart-list__main">
                                    <a href="#" class="nx-cart-list__img-link">
                                        <img src="https://via.placeholder.com/200x200"
                                             alt=""
                                             class="nx-cart-list__img">
                                    </a>
                    
                                    <div class="nx-cart-list__main-body">
                                        <div class="nx-cart-list__note">Артикул:
                                            <span>10073940</span>
                                        </div>
                        
                                        <a href="#" class="nx-cart-list__title">
                                            <span>Баня Б-150 4х4 М с террасой и большой верандой</span>
                                        </a>
                                    </div>
                                </div>
                
                                <div class="nx-cart-list__price-wr">
                                    <div class="nx-cart-list__price nx-cart-list__price_old">
                                        <span>1 577 000</span>
                        
                                        <i class="rub">q</i>
                                    </div>
                    
                                    <div class="nx-cart-list__price" data-cart-price>1 577 000
                                        <i class="rub">q</i>
                                        
                                        <span class="nx-cart-list__price-cnt">за шт</span>
                                    </div>
                                </div>
                
                                <div class="nx-cart-list__counter">
                                    <span>1</span> шт
                                </div>
                
                                <div class="nx-cart-list__total">
                                    <div class="nx-cart-list__total-price">
                                        <span>1 577 000</span>
                        
                                        <i class="rub">q</i>
                                    </div>
                    
                                    <div class="nx-cart-list__total-weight">
                                        <span>14.55</span>
                                        
                                        <span>кг</span>
                                    </div>
                                </div>
                            </div>
                        <? endfor; ?>

                            <div class="nx-set-total">
                                <div class="nx-set-total__discount">
                                    Скидка <span>54 000</span>
                                    
                                    <i class="rub">q</i>
                                </div>

                                <div class="nx-set-total__body">
                                    <div class="nx-set-total__label">Комплект со скидкой:</div>

                                    <div class="nx-set-total__price">
                                        <span>3 100 000</span>

                                        <i class="rub">q</i>
                                    </div>

                                    <div class="nx-set-total__price-old">
                                        <span>3 200 000</span>

                                        <i class="rub">q</i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-title">Доставка: самовывоз</div>
    
                    <div class="flex-row">
                        <div class="flex-col md-8">
                            <div class="nx-contacts">
                                <div class="nx-contacts__item">
                                    <div class="nx-contacts__label">Местоположение:</div>
                    
                                    <div class="nx-contacts__text">Стройбаза “Резонанс Котово”, Псковский район, деревня Котово, дом 18</div>
                                </div>
                
                                <div class="nx-contacts__item">
                                    <div class="nx-contacts__label">Пн-Пт:</div>
                    
                                    <div class="nx-contacts__text">9:00 - 19:00</div>
                                </div>
                
                                <div class="nx-contacts__item">
                                    <div class="nx-contacts__label">Сб-Вс:</div>
                    
                                    <div class="nx-contacts__text">9:00 - 16:00</div>
                                </div>
                            </div>
                        </div>
        
                        <div class="flex-col md-14">
                            <div class="nx-map nx-map_small"
                                 data-key="9b463b43-c9e4-41ad-aafc-a04e7d4a0243"
                                 data-map
                                 data-coord-x="44.23232323"
                                 data-coord-y="44.23232323"
                            ></div>
                        </div>
                    </div>
                </div>
                
                <div class="flex-col md-6 nx-section__col">
                    <div class="nx-cart-info">
                        <div class="nx-cart-info__body">
                            <div class="nx-cart-info__total">Итого,
                                <span>4</span> позиции:
                            </div>
                            
                            <div class="nx-cart-info__price h3">6 308 000
                                <i class="rub">q</i>
                            </div>
                            
                            <div class="nx-cart-info__discount">Скидка: 33 000
                                <i class="rub">q</i>
                            </div>
                            
                            <div class="nx-cart-info__weight">Вес:
                                <span>58,83</span> кг
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="flex-col">
                    <div class="line line_lg"></div>
                    
                    <div class="nx-actions">
                        <div class="nx-actions__item">
                            <a href="" class="btn btn_brd">
                                <span>Перейти на главную</span>
                            </a>
                        </div>
                        
                        <div class="nx-actions__item">
                            <a href="" class="btn btn_br">
                                <span>Перейти в личный кабинет</span>
                            </a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <? require_once '../templates/_blocks/footer.php'; ?>
</div>

<script src="/assets/app.min.js"></script>
</body>
</html>
