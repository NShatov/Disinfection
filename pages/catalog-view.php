<!DOCTYPE HTML>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta name="author" content="Контент">
    <meta name="keywords" content="keywords">
    <meta name="description" content="description">

    <title>Каталог страница</title>

    <link rel="icon" type="image/png" href="/favicon.png">
    <link rel="stylesheet" href="/assets/vendor.css">
    <link rel="stylesheet" href="/assets/app.min.css">

    <meta property="og:title" content="title"/>
    <meta property="og:description" content="description"/>
    <meta property="og:image" content="favicon.png">

</head>
<body>

<? require_once '../templates/_blocks/header.php'; ?>
<? require_once '../templates/_blocks/cart_informer.php'; ?>

<div class="wrapper">
    <div class="wrapper__content">
        <div class="nx-section container" data-prod="1">
            <div class="nx-breadcrumbs">
                <div class="nx-breadcrumbs__item">
                    <a href="#" class="nx-breadcrumbs__link">
                        <span>Каталог</span>
                    </a>
                    
                    <i class="nx-breadcrumbs__arrow mdi mdi-chevron-right"></i>
                </div>
                
                <div class="nx-breadcrumbs__item">
                    <a href="#" class="nx-breadcrumbs__link">
                        <span>Категория</span>
                    </a>
                    
                    <i class="nx-breadcrumbs__arrow mdi mdi-chevron-right"></i>
                </div>
            </div>

            <h4 class="nx-section__title" data-prod-title="Альпиец 2 к-150">Альпиец 2 к-150</h4>

            <div class="flex-row nx-section__box nx-product">
                <div class="flex-col md-10 nx-section__col-small">
                    <div class="nx-gal-slider not-visible" data-gal="vertical">
                        <div class="nx-gal-slider__main" data-gal-main>
                            <div class="swiper-wrapper nx-gal-slider__main-wrap">
                                <? for ($i = 0; $i < 10; $i++): ?>
                                    <a href="https://via.placeholder.com/1920x1080"
                                       class="swiper-slide nx-gal-slider__main-slide"
                                       data-fancybox="gal">
                                        <img
                                            src="https://via.placeholder.com/1200x800"
                                            alt=""
                                            class="nx-gal-slider__main-img">
                                    </a>
                                <? endfor; ?>
                            </div>
                        </div>

                        <div class="nx-gal-slider__thumbs" data-gal-thumb>
                            <div class="swiper-wrapper nx-gal-slider__thumbs-wrap">
                                <? for ($i = 0; $i < 10; $i++): ?>
                                    <div class="swiper-slide nx-gal-slider__thumbs-slide"
                                         data-gal-thumb-slide
                                         style="background-image: url('https://via.placeholder.com/100x100');"></div>
                                <? endfor; ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="flex-col md-14 nx-section__col-small">
                    <div class="nx-product__item">
                        <div class="nx-articul nx-articul_start">
                            <div class="nx-articul__articul">Артикул:
                                <span>10073940</span>
                            </div>
                            
                            <div class="nx-articul__availible">В наличии</div>
                        </div>
                    </div>

                    <div class="nx-product__item">
                        <div class="nx-tags">
                            <div class="nx-tags__item">Новинка!</div>
                            
                            <div class="nx-tags__item">Новинка!</div>
                        </div>
                    </div>

                    <div class="nx-product__item">
                        <div class="h4 prod-price" data-prod-price="1577000">От 1 577 000 ₽</div>
                    </div>

                    <div class="nx-product__item">
                        <div class="nx-actions">
                            <div class="nx-actions__item">
                                <a href="#"
                                   class="btn  btn_d-block-xs"
                                   data-prod-add>Добавить в корзину</a>
                            </div>
                            
                            <div class="nx-actions__item">
                                <a href="#" class="link one-click">
                                    <svg class="icon"><use xlink:href="#icon-one-click"></use></svg>
                                    
                                    <span>Купить в один клик</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="nx-product__item">
                        <div class="text">
                            <p>Проект индивидуального дома со стенами на основе деревянного каркаса разработан в
                                соответствии с требованиями стандартов, строительных норм и правил, предъявляемым к
                                строениям данного типа </p>
                        </div>
                    </div>
                    
                    <a href="#tabs"
                       class="link"
                       data-scroll-btn="tabs"><span>Смотреть характеристики</span></a>
                </div>
            </div>
            
            <div class="nx-section__box">
                <div class="nx-advans flex-row">
                    <? for ($i = 0; $i < 4; $i++): ?>
                        <div class="nx-advans__item flex-col md-6 sm-12">
                            <div class="nx-advans__icon"
                                 style="background-image: url('https://via.placeholder.com/140x140');"></div>
                            
                            <div class="nx-advans__text">От начала строительства до сдачи 50 дней</div>
                        </div>
                    <? endfor; ?>
                </div>
            </div>

            <div class="nx-section__box">
                <div class="nx-tabs" data-tabs-slider id="tabs">
                    <ul class="nx-tabs__wrap swiper-wrapper">
                        <li class="swiper-slide nx-tabs__item">
                            <a href="#descr"
                               class="link nx-tabs__link active"
                                data-tab-btn="tab-1">
                                <span>Описание</span>
                            </a>
                        </li>
                        
                        <li class="swiper-slide nx-tabs__item">
                            <div class="link nx-tabs__link"
                               data-tab-btn="tab-2"
                               data-scroll-target>
                                <span>Характеристики</span>
                            </div>
                        </li>
                        
                        <li class="swiper-slide nx-tabs__item">
                            <div
                               class="link nx-tabs__link"
                               data-tab-btn="tab-3">
                                <span>Отзывы</span>
                            </div>
                        </li>
                        
                        <li class="swiper-slide nx-tabs__item">
                            <a class="link nx-tabs__link"
                               data-tab-btn="tab-4">
                                <span>Документы</span>
                            </a>
                        </li>
                    </ul>
                </div>
                
                <div class="tab-content is-active" data-tab-content="tab-1" >
                    <div class="text content-narrow-med">
                        <p>Создание или разработка дизайн макета является первоначальным этапом в изготовление
                            рекламы. Главная задача любой рекламы – привлечение внимания потенциальных покупателей и
                            побуждение к действию. Продуманный дизайн листовок, баннеров, щитов даст хороший эффект
                            и поможет вам продвинуть свой бизнес. В ходе разработки макета широкоформатная печать и
                            полиграфия требует к себе определенного подхода.Красивый дизайн также сможет создать
                            положительный имидж вашей организации и показать потребителям, что вы серьезно
                            относитесь к своему делу </p>
                        <h5>Маркированный список </h5>
                        <ul>
                            <li>Защита поверхности от воздействия внешней среды (в первую очередь от влаги-от дождя
                                и пролитого кофе) и защита красочного слоя от механических повреждений (царапины,
                                соскобов и пр.)
                            </li>
                            <li>Защита поверхности от воздействия внешней среды (в первую очередь от влаги-от дождя
                                и пролитого кофе) и защита красочного слоя от механических повреждений (царапины,
                                соскобов и пр.)
                            </li>
                            <li>Защита поверхности от воздействия внешней среды (в первую очередь от влаги-от дождя
                                и пролитого кофе) и защита красочного слоя от механических повреждений (царапины,
                                соскобов и пр.)
                            </li>
                        </ul>
                    </div>
                </div>
                
                <div class="tab-content" data-tab-content="tab-2">
                    <div class="nx-attrs content-narrow-med">
                        <div class="nx-attrs__body">
                            <? for ($i = 0; $i < 5; $i++): ?>
                                <div class="nx-attrs__item">
                                    <div class="nx-attrs__name">Высота потолка 1-го этажа</div>
                                    
                                    <div class="nx-attrs__val">Из клееного профилированного бруса 146, 168 или 210
                                        мм. или каркасная технология: каркас из бруса. Утепление - минеральная вата.
                                        стены обшиваются вагонкой. Снаружи по по ветро- гидроизоляционной пленке,
                                        внутри по пароизоляционной пленке
                                    </div>
                                </div>
                            <? endfor; ?>
                        </div>
                    </div>
                </div>
                
                <div class="tab-content" data-tab-content="tab-3">
                    <div class="nx-section__head">
                        <h4 class="nx-section__title">Отзывы</h4>
                        
                        <a href="#modal-review"
                           class="nx-section__link link"
                           data-toggle="modal">
                            <span>Оставить отзыв</span>
                        </a>
                    </div>
                    
                    <div class="nx-reviews" data-load-content>
                        <? for ($x = 0; $x < 3; $x++): ?>
                            <div class="nx-reviews__item">
                                <div class="nx-reviews__line">
                                    <div class="nx-avatar">
                                        <div class="nx-avatar__icon"
                                             style="background-image: url('https://via.placeholder.com/120x120')"></div>
                                        
                                        <div class="nx-avatar__body">
                                            <div class="nx-avatar__title">Петров Петр Петрович</div>
                                            
                                            <div class="nx-avatar__descr">10.04.20</div>
                                        </div>
                                    </div>

                                    <div class="nx-rating">
                                        <div class="nx-rating__body">
                                            <div class="nx-rating__list">
                                                <? for ($i = 0; $i < 5; $i++): ?>
                                                    <span class="nx-rating__star">
                                                        <i class="nx-rating__empty mdi mdi-star-outline"></i>
                                                    </span>
                                                <? endfor; ?>
                                            </div>
                                            
                                            <div class="nx-rating__result"> 3.7</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="nx-section__head">
                                    <div class="nx-rating">
                                        <? for ($j = 0; $j < 4; $j++): ?>
                                            <div class="nx-rating__el">
                                                <div class="nx-rating__title">Практичность</div>
                                                
                                                <div class="nx-rating__body">
                                                    <div class="nx-rating__list">
                                                        <? for ($i = 0; $i < 5; $i++): ?>
                                                            <span class="nx-rating__star">
                                                                <i class="nx-rating__empty mdi mdi-star-outline"></i>
                                                            </span>
                                                        <? endfor; ?>
                                                    </div>
                                                    
                                                    <div class="nx-rating__result"> 3.7</div>
                                                </div>
                                            </div>
                                        <? endfor; ?>
                                    </div>
                                </div>

                                <div class="text">
                                    <h6>Достоинства</h6>
                                    <p>Отличная картинка, лёгкий, дешёвый. Есть возможность установить SSD2'5 (нужно
                                        открутить два винтика). Есть RJ 45</p>
                                </div>
                            </div>
                        <? endfor; ?>
                    </div>

                    <div class="nx-actions nx-actions_center">
                        <div class="nx-actions__item">
                            <a href=""
                               class="btn btn_br"
                               data-show-more data-show-more-url>Показать еще</a>
                        </div>
                    </div>
                </div>
                
                <div class="tab-content" data-tab-content="tab-4">
                        <div class="nx-files flex-row">
                            <? for ($i = 0; $i < 8; $i++): ?>
                                <div class="nx-files__item flex-col md-8 sm-8 xs">
                                    <a href="#"
                                       class="nx-files__link"
                                       target="_blank"
                                       rel="noopener">
                                        <div class="nx-files__icon"
                                             style="background-image: url('https://via.placeholder.com/135x180');"></div>
                                        
                                        <div class="nx-files__content">
                                            <span class="nx-files__title">Условия предоставления гарантии</span>
                                            
                                            <div class="nx-files__type">PDF. 2.1 мб</div>
                                        </div>
                                    </a>
                                </div>
                            <? endfor; ?>
                        </div>
                    </div>
            </div>

            <div class="nx-section__box">
                <div class="nx-section__head">
                    <h4>С этим товаром смотрят</h4>
                </div>
                
                <div class="nx-list-slider">
                    <div class="nx-list-slider__prev" data-list-slider-prev>
                        <svg class="nx-list-slider__prev-icon"><use xlink:href="#icon-arrow"></use></svg>
                    </div>
                    
                    <div class="nx-list-slider__next nx-list-slider__next" data-list-slider-next>
                        <svg class="nx-list-slider__prev-icon"><use xlink:href="#icon-arrow"></use></svg>
                    </div>
                    
                    <div class="nx-list-slider__slider nx-listing nx-listing_prod" data-list-slider>
                        <div class="swiper-wrapper">
                            <? for ($i = 0; $i < 6; $i++): ?>
                                <div class="swiper-slide nx-listing__item">
                                    <div class="nx-listing__icon-list">
                                        <a href="#" class="nx-listing__icon-btn">
                                            <svg class="nx-listing__icon"><use xlink:href="#icon-like"></use></svg>
                                        </a>
                                        
                                        <a href="#" class="nx-listing__icon-btn">
                                            <svg class="nx-listing__icon"><use xlink:href="#icon-promote"></use></svg>
                                        </a>
                                    </div>
                                    
                                    <a href="#" class="nx-listing__link">
                                        <div class="nx-listing__head">
                                            <img src="https://via.placeholder.com/550x550"
                                                 alt=""
                                                 class="nx-listing__img">
                                            
                                            <div class="nx-tags nx-tags_small">
                                                <? for ($j = 0; $j < 3; $j++): ?>
                                                    <div class="nx-tags__item">Лучшая цена</div>
                                                <? endfor; ?>
                                            </div>
                                        </div>
                                        
                                        <div class="nx-listing__body">
                                            <div class="nx-listing__note">
                                                <div class="nx-listing__articul">Артикул:
                                                    <span>10073940</span>
                                                </div>
                                                
                                                <div class="nx-listing__availible">В наличии</div>
                                            </div>
                                            
                                            <span class="nx-listing__title">Баня Б-150 4х4 М с террасой и большой верандой </span>
                                            
                                            <div class="spacer-v"></div>
                                            
                                            <div class="nx-prices-item">
                                                <div class="nx-prices-item__old">10 000 &#8381;</div>
                                                
                                                <div class="nx-prices-item__current">8 000 &#8381;</div>
                                            </div>
                                        </div>
                                    </a>
                                    
                                    <div class="nx-listing__actions">
                                        <div class="nx-listing__actions-row">
                                            <div class="nx-listing__actions-el">
                                                <div class="nx-counter" data-counter>
                                                    <a href="#"
                                                       class="nx-counter-btn nx-counter-btn_minus"
                                                       data-counter-btn="minus"></a>
                                                    
                                                    <input type="text"
                                                           name="products[quantity]"
                                                           value="1"
                                                           class="nx-counter__input"
                                                           data-counter-inp data-max="999"
                                                           data-prod-cnt>
                                                    
                                                    <a href="#"
                                                       class="nx-counter-btn nx-counter-btn_plus"
                                                       data-counter-btn="plus"></a>
                                                </div>
                                            </div>
                                            
                                            <div class="nx-listing__actions-el">
                                                <a href="" class="btn btn_br nx-listing__buy">
                                                    <span>В корзину</span>
                                                    
                                                    <svg class="icon"><use xlink:href="#icon-cart"></use></svg>
                                                </a>
                                            </div>
                                        </div>
                                        
                                        <a href="#" class="link nx-listing__click">
                                            <svg class="icon"><use xlink:href="#icon-one-click"></use></svg>
                                            
                                            <span>Купить в один клик </span>
                                        </a>
                                    </div>
                                </div>
                            <? endfor; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <? require_once '../templates/_blocks/footer.php'; ?>
</div>

<?
    
    require_once '../templates/_modals/_modal-review.php';
?>
<script src="/assets/app.min.js"></script>
</body>
</html>
