<!DOCTYPE HTML>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <meta name="author" content="Контент">
    <meta name="keywords" content="keywords">
    <meta name="description" content="description">
    
    <title>Корзина</title>
    
    <link rel="icon" type="image/png" href="/favicon.png">
    <link rel="stylesheet" href="/assets/vendor.css">
    <link rel="stylesheet" href="/assets/app.min.css">
    
    <meta property="og:title" content="title"/>
    <meta property="og:description" content="description"/>
    <meta property="og:image" content="favicon.png">

</head>
<body data-home>

<? require_once '../templates/_blocks/header-empty.php'; ?>
<? require_once '../templates/_blocks/cart_informer.php'; ?>

<div class="wrapper">
    <div class="wrapper__content">
        <div class="nx-section nx-section_page container">
            <h4 class="nx-section__title">Способ оплаты</h4>
            
            <div class="flex-row" data-affix-parent>
                <div class="flex-col md-18 nx-section__col">
                    <div class="flex-row">
                        <div class="flex-col md-16">
                            <div class="nx-section__item">
                                <? for ($i = 0; $i < 2; $i++): ?>
                                    <label class="checkbox checkbox_big">
                                        <input type="radio"
                                               name="payment_type_id"
                                               class="checkbox__input"
                                               value="1"
                                               <?php if ($i===0):?>checked <?php endif; ?>
                                        >
                
                                        <span class="checkbox__wrap">
                                            <span class="checkbox__title">Онлайн на сайте</span>
                                            
                                            <span class="checkbox__text">Банковской картой</span>
                                            <?php if ($i===0):?>
                                                <span class="checkbox__list">
                                                    <? for ($x = 0; $x < 3; $x++): ?>
                                                        <img src="https://via.placeholder.com/130x30"
                                                             alt=""
                                                             class="checkbox__img">
                                                    <? endfor; ?>
                                                    </span>
                                            <?php endif; ?>
                                        </span>
                                    </label>
                                <?endfor;?>
                            </div>
                            
                            <div class="nx-actions nx-actions_space-between">
                                <div class="nx-actions__item">
                                    <a href="#" class="nx-actions__link link">
                                        <i class="link-icon mdi mdi-chevron-left"></i>
                                        
                                        <span>Назад</span>
                                    </a>
                                </div>
                                
                                <div class="nx-actions__item">
                                    <a href="#" class="nx-actions__link btn">
                                        <span>Подтвердить</span>
                                    </a>
                                </div>
                            </div>

                            <div class="note note_btn text-right">Нажимая кнопку "Подтвердить", вы даете согласие на обработку
                                <a href="/politics/" class="link">
                                    <span>персональных данных</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="nx-order-block flex-col md-6 nx-section__col" data-affix-wrap>
                    <div class="nx-affix" data-affix-block>
                        <div class="nx-order-block__title">Ваш заказ</div>
                        
                        <div class="nx-ordered">
                            <? for ($i = 0; $i < 2; $i++): ?>
                                <div class="nx-ordered__item">
                                    <div class="nx-ordered__body">
                                        <a href="" class="nx-ordered__title">
                                            <span>Баня Б-150 4х4 М с террасой и большой верандой</span>
                                        </a>
                                        
                                        <div class="nx-cart-list__note">Артикул:
                                            <span>10073940</span>
                                        </div>
                                    </div>
                                    
                                    <div class="nx-ordered__info">
                                        <div class="nx-ordered__price-old">
                                            <span>1 600 000</span>
                                            
                                            <i class="rub">q</i>
                                        </div>
                                        
                                        <div class="nx-ordered__price">
                                            <span>1 577 000</span>
                                            
                                            <i class="rub">q</i>
                                        </div>
                                        
                                        <div class="nx-ordered__counter">
                                            <span>1</span> шт
                                        </div>
                                    </div>
                                </div>
                            <? endfor; ?>
                        </div>
                        
                        <div class="nx-cart-info">
                            <div class="nx-cart-info__body">
                                <div class="nx-cart-info__total">Итого, <span>4</span> позиции:</div>
                                
                                <div class="nx-cart-info__price h3">6 308 000 <i class="rub">q</i></div>
                                
                                <div class="nx-cart-info__discount">Скидка: 33 000 <i class="rub">q</i> </div>
                                
                                <div class="nx-cart-info__weight">Вес: <span>58,83</span> кг</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div data-stop-affix></div>
        </div>
    </div>
    <? require_once '../templates/_blocks/footer.php'; ?>
</div>

<script src="/assets/app.min.js"></script>
</body>
</html>
