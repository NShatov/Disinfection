<!DOCTYPE HTML>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <meta name="author" content="Контент">
    <meta name="keywords" content="keywords">
    <meta name="description" content="description">
    
    <title>Главная</title>
    
    <link rel="icon" type="image/png" href="/favicon.png">
    <link rel="stylesheet" href="/assets/vendor.css">
    <link rel="stylesheet" href="/assets/app.min.css">
    
    <meta property="og:title" content="title"/>
    <meta property="og:description" content="description"/>
    <meta property="og:image" content="favicon.png">

</head>
<body>

<? require_once '../templates/_blocks/header.php'; ?>

<div class="wrapper">
    <div class="wrapper__content">
        <div class="nx-section nx-section_page">
            <div class="container">
                <div class="nx-section__element-inner">
                    <h4 class="nx-section__title">Личный кабинет</h4>
                    
                    <div class="nx-personal-actions">
                        <div class="nx-toggle-button">
                            <input type="checkbox"
                                   name="toggle"
                                   id="toggle-button"
                                   class="nx-toggle-button__input"
                                   data-mailing-toggle
                                   data-url="">
                            
                            <label for="toggle-button" class="nx-toggle-button__text">Подписка на рассылку</label>
                        </div>
                        
                        <a href="" class="link link_exit">
                            <svg class="link-icon"><use xlink:href="#icon-exit"></use></svg>
                            
                            <span>Выйти</span>
                        </a>
                    </div>
                </div>
                
                <div class="nx-tabs" data-tabs-slider>
                    <div class="swiper-wrapper nx-tabs__wrap">
                        <div class="swiper-slide nx-tabs__item">
                            <a href="#"
                               class="nx-tabs__link">
                                <span>Мои заказы</span>
                            </a>
                        </div>
                        
                        <div class="swiper-slide nx-tabs__item">
                            <a href="#"
                               class="nx-tabs__link">
                                <span>Избранное</span>
                            </a>
                        </div>
                        
                        <div class="swiper-slide nx-tabs__item is-active">
                            <a href="#"
                               class="nx-tabs__link">
                                <span>Личные данные</span>
                            </a>
                        </div>
                    </div>
                </div>
                
                <div class="flex-row">
                    <div class="flex-col md-6">
                        <div class="nx-section__item">
                            <h5 class="nx-section__title">Личные данные</h5>
                            
                            <form action="">
                                <div class="form-group form-group_small-offset" data-form-group>
                                    <label class="nx-dynamic-label" data-dynamic-label>
                                        <input type="text"
                                               class="nx-dynamic-label__input nx-form-element"
                                               name="name"
                                               value="Николай"
                                               data-dynamic-inp>
                
                                        <span class="nx-dynamic-label__text">Имя</span>
                                    </label>
                                </div>
        
                                <div class="form-group form-group_small-offset" data-form-group>
                                    <label class="nx-dynamic-label" data-dynamic-label>
                                        <input type="text"
                                               class="nx-dynamic-label__input nx-form-element"
                                               name="phone"
                                               value="+79291211125"
                                               data-phone-mask
                                               data-dynamic-inp>
                
                                        <span class="nx-dynamic-label__text">Телефон</span>
                                    </label>
                                </div>
        
                                <div class="form-group form-group_small-offset" data-form-group>
                                    <label class="nx-dynamic-label" data-dynamic-label>
                                        <input type="text"
                                               class="nx-dynamic-label__input nx-form-element"
                                               name="email"
                                               value="konst@mail.ru"
                                               data-dynamic-inp>
                
                                        <span class="nx-dynamic-label__text">E-mail</span>
                                    </label>
                                </div>
        
                                <div class="nx-actions nx-actions_offset-top nx-actions_single">
                                    <div class="nx-actions__item">
                                        <a href=""
                                           disabled
                                           class="btn">Сохранить изменения</a>
                                    </div>
                                </div>
                            </form>
                        </div>
    
                        <div class="nx-section__item">
                            <h5 class="nx-section__title">Адрес</h5>
                            
                            <form action="">
                                <div class="form-group form-group_small-offset" data-form-group>
                                    <label class="nx-dynamic-label" data-dynamic-label>
                                        <input type="text"
                                               class="nx-dynamic-label__input nx-form-element"
                                               name="name"
                                               value="Николай"
                                               data-dynamic-inp>
                    
                                        <span class="nx-dynamic-label__text">Адрес</span>
                                    </label>
                                    <div class="form-group__label">Город, улица, дом/офис</div>
                                </div>
            
                                <div class="nx-actions nx-actions_offset-top nx-actions_single">
                                    <div class="nx-actions__item">
                                        <a href=""
                                           disabled
                                           class="btn"
                                           data-show-more
                                           data-show-more-url>Сохранить изменения</a>
                                    </div>
                                </div>
                            </form>
                        </div>
    
                        <div class="nx-section__item">
                            <h5 class="nx-section__title">Пароль</h5>
                            
                            <form action="">
                                <div class="form-group form-group_short form-group_small-offset"
                                     data-form-group
                                     data-password-wr>
                                    <label class="nx-dynamic-label"
                                           data-dynamic-label>
                                        <input type="password"
                                               class="nx-dynamic-label__input nx-form-element"
                                               name="password"
                                               data-password-input
                                               data-dynamic-inp>
                
                                        <span class="nx-dynamic-label__text">Пароль</span>
                
                                        <a href="#"
                                           class="nx-password"
                                           data-password-link>
                                            <svg class="nx-password__icon"><use xlink:href="#icon-password-closed"></use></svg>
                                        </a>
                                    </label>
            
                                    <div class="form-group__label">Может быть любой, но лучше - надежный</div>
                                </div>
                                
                                <div class="form-group form-group_short form-group_small-offset"
                                     data-form-group
                                     data-password-wr>
                                    <label class="nx-dynamic-label"
                                           data-dynamic-label>
                                        <input type="password"
                                               class="nx-dynamic-label__input nx-form-element"
                                               name="password"
                                               data-password-input
                                               data-dynamic-inp>
                
                                        <span class="nx-dynamic-label__text">Повторите пароль</span>
                
                                        <a href="#"
                                           class="nx-password"
                                           data-password-link>
                                            <svg class="nx-password__icon"><use xlink:href="#icon-password-closed"></use></svg>
                                        </a>
                                    </label>
                                </div>
        
                                <div class="nx-actions nx-actions_offset-top nx-actions_single">
                                    <div class="nx-actions__item">
                                        <a href=""
                                           disabled
                                           class="btn"
                                           data-show-more
                                           data-show-more-url>Изменить пароль</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    
    <? require_once '../templates/_blocks/footer.php'; ?>
</div>

<script src="/assets/app.min.js"></script>
</body>
</html>