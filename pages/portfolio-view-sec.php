<!DOCTYPE HTML>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta name="author" content="Контент">
    <meta name="keywords" content="keywords">
    <meta name="description" content="description">

    <title>Портфолио страница</title>

    <link rel="icon" type="image/png" href="/favicon.png">
    <link rel="stylesheet" href="/assets/vendor.css">
    <link rel="stylesheet" href="/assets/app.min.css">

    <meta property="og:title" content="title"/>
    <meta property="og:description" content="description"/>
    <meta property="og:image" content="favicon.png">

</head>
<body>

<? require_once '../templates/_blocks/header.php'; ?>

<div class="wrapper">
    <div class="wrapper__content">
        <div class="nx-section container">
            <div class="nx-breadcrumbs">
                <div class="nx-breadcrumbs__item">
                    <a href="#" class="nx-breadcrumbs__link">
                        <span>Портфолио</span>
                    </a>
                    
                    <i class="nx-breadcrumbs__arrow mdi mdi-chevron-right"></i>
                </div>
            </div>

            <div class="nx-section__box">
                <div class="nx-section__head">
                    <div class="nx-tags">
                        <div class="nx-tags__item">Новинка!</div>
                        
                        <div class="nx-tags__item">Новинка!</div>
                        
                        <div class="nx-tags__item">Новинка!</div>
                    </div>
                </div>
                
                <div class="nx-section__head content-narrow-med">
                    <div class="h3 nx-section__title">Брендирование главного офиса Curtis</div>
                    
                    <div class="text text_dark-fade nx-section__text">Крупнейший интернет-магазин модной одежды, обуви,
                        аксессуаров, косметических средств, существующий уже 15 лет! Из года в год мы продолжаем
                        развиваться, расширять географию присутствия и улучшать качество обслуживания, чтобы радовать
                        Вас
                        каждый день! Равным образом сложившаяся структура организации требуют от нас анализа новых
                        предложений
                    </div>
                </div>
                <div class="nx-section__head">
                    <div class="flex-row">
                        <div class="flex-col md-14 nx-section__col">
                            <div class="nx-gal-slider nx-gal-slider_med not-visible" data-gal>
                                <div class="nx-gal-slider__main" data-gal-main>
                                    <div class="swiper-wrapper nx-gal-slider__main-wrap">
                                        <? for ($i = 0; $i < 10; $i++): ?>
                                            <a href="https://via.placeholder.com/1920x1080"
                                               class="swiper-slide nx-gal-slider__main-slide"
                                               data-fancybox="gal">
                                                <img src="https://via.placeholder.com/1580x890"
                                                     alt=""
                                                     class="nx-gal-slider__main-img">
                                            </a>
                                        <? endfor; ?>
                                    </div>

                                    <div class="nx-gal-slider__prev" data-gal-prev>
                                        <svg class="nx-gal-slider__prev-icon"><use xlink:href="#icon-arrow"></use></svg>
                                    </div>
                                    
                                    <div class="nx-gal-slider__next nx-gal-slider__next" data-gal-next>
                                        <svg class="nx-gal-slider__next-icon"><use xlink:href="#icon-arrow"></use></svg>
                                    </div>
                                </div>

                                <div class="nx-gal-slider__thumbs" data-gal-thumb>
                                    <div class="swiper-wrapper nx-gal-slider__thumbs-wrap">
                                        <? for ($i = 0; $i < 10; $i++): ?>
                                            <div class="swiper-slide nx-gal-slider__thumbs-slide"
                                                 data-gal-thumb-slide
                                                 style="background-image: url('https://via.placeholder.com/200x110');"></div>
                                        <? endfor; ?>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="flex-col md-10 nx-section__col">
                            <div class="nx-advans">
                                <? for ($i = 0; $i < 3; $i++): ?>
                                    <div class="nx-advans__item">
                                        <div class="nx-advans__num" data-anim-num="35000">35 000</div>
                                        
                                        <div class="nx-advans__text">Популярных и проверенных брендов на сайте</div>
                                    </div>
                                <? endfor; ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="likely likely_dnext">
                    <div class="facebook">Поделиться</div>
                    
                    <div class="vkontakte">Поделиться</div>
                </div>
            </div>

            <div class="nx-section__box">
                <form action="/request/" class="nx-form nx-form_row">
                    <div class="nx-form__content">
                        <h4 class="nx-form__title">Хотите такой же или похожий проект?</h4>
                        
                        <div class="nx-form__text text text_dark-fade">Заполните поля и мы позвоним вам для
                            уточнения деталей. Напишем на почту если не дозвонимся
                        </div>
                    </div>
                    
                    <div class="nx-form__body">
                        <div class="form-group" data-form-group>
                            <label class="nx-dynamic-label" data-dynamic-label>
                                <input type="text"
                                       class="nx-dynamic-label__input nx-form-element"
                                       name="name"
                                       data-dynamic-inp>
                                
                                <span class="nx-dynamic-label__text">Ф.И.О.</span>
                            </label>
                        </div>
                        
                        <div class="form-group" data-form-group>
                            <label class="nx-dynamic-label" data-dynamic-label>
                                <input type="text"
                                       class="nx-dynamic-label__input nx-form-element"
                                       name="phone"
                                       data-dynamic-inp
                                       data-phone-mask>
                                
                                <span class="nx-dynamic-label__text">Телефон</span>
                            </label>
                        </div>
                        
                        <div class="form-group" data-form-group>
                            <label class="nx-dynamic-label" data-dynamic-label>
                                <input type="text"
                                       class="nx-dynamic-label__input nx-form-element"
                                       name="email"
                                       data-dynamic-inp>
                                
                                <span class="nx-dynamic-label__text">E-mail</span>
                            </label>
                        </div>
                        
                        <div class="nx-files nx-files_upload js-file-box">
                            <div class="nx-files__item">
                                <a href="#" class="nx-files__link dropzone js-dropzone js-file-btn">
                                    <svg class="nx-files__icon"><use xlink:href="#icon-upload"></use></svg>
                                    
                                    <div class="nx-files__content">
                                        <span class="nx-files__title">Прикрепить ТЗ</span>
                                        
                                        <div class="nx-files__type">
                                            jpg, png, gif, pdf, xlsx, doc, txt, до 5 мб, не более 5 файлов
                                        </div>
                                    </div>
                                </a>
                            </div>
                            
                            <input type="file"
                                   multiple=""
                                   name="files[]"
                                   class="hidden js-file-inp">
                        </div>
                        
                        <div class="nx-actions nx-actions_note">
                            <div class="nx-actions__item nx-actions__item_btn">
                                <button type="submit"
                                        class="btn  btn_d-block-xs"
                                        data-send-request="order">Отправить</button>
                            </div>
                            
                            <div class="nx-actions__item">
                                <div class="note">Нажимая на кнопку "Отправить", вы даете согласие на обработку
                                    <a href="/politics/" class="link">
                                        <span>персональных данных</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <? require_once '../templates/_blocks/footer.php'; ?>
</div>

<script src="/assets/app.min.js"></script>
</body>
</html>
