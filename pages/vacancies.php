<!DOCTYPE HTML>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta name="author" content="Контент">
    <meta name="keywords" content="keywords">
    <meta name="description" content="description">

    <title>Вакансии</title>

    <link rel="icon" type="image/png" href="/favicon.png">
    <link rel="stylesheet" href="/assets/vendor.css">
    <link rel="stylesheet" href="/assets/app.min.css">

    <meta property="og:title" content="title"/>
    <meta property="og:description" content="description"/>
    <meta property="og:image" content="favicon.png">

</head>
<body>

<? require_once '../templates/_blocks/header.php'; ?>

<div class="wrapper">
    <div class="wrapper__content">
        <section class="nx-section container">
            <div class="nx-section__head content-narrow-med">
                <h2 class="nx-section__title">Вакансии</h2>
                
                <div class="nx-section__text text text_dark-fade">Крупнейший интернет-магазин модной одежды, обуви,
                    аксессуаров, косметических средств, существующий уже 15 лет! Из года в год мы продолжаем
                    развиваться, расширять географию присутствия и улучшать качество обслуживания, чтобы радовать Вас
                    каждый день!
                </div>
            </div>

            <div class="nx-tabs" data-tabs-slider>
                <div class="swiper-wrapper nx-tabs__wrap">
                    <div class="swiper-slide nx-tabs__item is-active">
                        <a href="#"
                           class="nx-tabs__link"
                           data-nx-url="">
                            <span>Все вакансии</span>
                        </a>
                    </div>
                    
                    <div class="swiper-slide nx-tabs__item">
                        <a href="#"
                           class="nx-tabs__link"
                           data-nx-url="">
                            <span> Административно-производственный персонал</span>
                        </a>
                    </div>
                    
                    <div class="swiper-slide nx-tabs__item">
                        <a href="#"
                           class="nx-tabs__link"
                           data-nx-url="">
                            <span>Рабочий персонал</span>
                        </a>
                    </div>
                </div>
            </div>

            <div class="nx-list-row" data-ajax-content>
                <div class="nx-list-row__item">
                    <a href="#" class="nx-list-row__link">
                        <div class="nx-list-row__content">
                            <span class="nx-list-row__title">Кладовщик / Комплектовщик</span>
                            
                            <div class="nx-list-row__text text text_dark-fade">Комплектование товара с помощью
                                голосового терминала; транспортировка товара к месту отгрузки при помощи гидравлической
                                / электрической тележки. Осуществление своевременной и качественной работы.
                                Ответственность. Дисциплинированность...
                            </div>
                        </div>
                    </a>
                    
                    <div class="nx-list-row__actions">
                        <div class="nx-list-row__price">з/п не указана</div>
                        
                        <a href="#"
                           class="btn btn_d-block-xs nx-list-row__btn"
                           data-custom-open="modal-vacancy"
                           data-modal-btn
                           data-id="1"
                           >Откликнуться</a>
                    </div>
                </div>
                
                <div class="nx-list-row__item">
                    <a href="#" class="nx-list-row__link">
                        <div class="nx-list-row__content">
                            <span class="nx-list-row__title">Кладовщик / Комплектовщик</span>
                            
                            <div class="nx-list-row__text text text_dark-fade">Комплектование товара с помощью
                                голосового терминала; транспортировка товара к месту отгрузки при помощи гидравлической
                                / электрической тележки. Осуществление своевременной и качественной работы.
                                Ответственность. Дисциплинированность...
                            </div>
                        </div>
                    </a>
                    
                    <div class="nx-list-row__actions">
                        <div class="nx-list-row__price">от 30 000
                            <i class="rub">q</i>
                        </div>
                        <a href="#"
                           data-custom-open="modal-vacancy"
                           class="btn btn_d-block-xs nx-list-row__btn"
                           data-modal-btn
                           data-id="2">Откликнуться</a>
                    </div>
                </div>
            </div>
        </section>
    </div>
    
    <? require_once '../templates/_blocks/footer.php'; ?>
</div>
<?
    require_once '../templates/_modals/_modal-vacancy.php';
?>
<script src="/assets/app.min.js"></script>
</body>
</html>

