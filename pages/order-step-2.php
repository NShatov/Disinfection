<!DOCTYPE HTML>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <meta name="author" content="Контент">
    <meta name="keywords" content="keywords">
    <meta name="description" content="description">
    
    <title>Корзина</title>
    
    <link rel="icon" type="image/png" href="/favicon.png">
    <link rel="stylesheet" href="/assets/vendor.css">
    <link rel="stylesheet" href="/assets/app.min.css">
    
    <meta property="og:title" content="title"/>
    <meta property="og:description" content="description"/>
    <meta property="og:image" content="favicon.png">

</head>
<body>

<? require_once '../templates/_blocks/header-empty.php'; ?>
<? require_once '../templates/_blocks/cart_informer.php'; ?>

<div class="wrapper">
    <div class="wrapper__content">
        <div class="nx-section nx-section_page container">
            <h4 class="nx-section__title">Контактная информация</h4>
            
            <div class="flex-row" data-affix-parent>
                <div class="flex-col md-18 nx-section__col">
                    <div class="flex-row">
                        <div class="flex-col md-16">
                            <div class="nx-order-account">
                                <div class="nx-order-account__title">Есть аккаунт?</div>
                
                                <div class="nx-order-account__text">Если у вас есть аккаунт, войдите в него для автозаполнения полей ниже и просмотра истории заказов</div>
                
                                <a href="" class="nx-order-account__link">
                                    <span>Войти</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="flex-row">
                        <div class="flex-col md-16">
                            <div class="nx-section__item">
                                <div class="form-title">Контактная информация</div>
                
                                <div class="nx-tabs nx-tabs_small" data-tabs-slider>
                                    <ul class="nx-tabs__wrap nav nav-tabs swiper-wrapper">
                                        <li class="nav-item swiper-slide nx-tabs__item">
                                            <a href="#individual"
                                               class="nav-link nx-tabs__link active"
                                               data-toggle="tab">
                                                <span>Физ. лицо</span>
                                            </a>
                                        </li>
                        
                                        <li class="nav-item swiper-slide nx-tabs__item">
                                            <a href="#entity"
                                               class="nav-link nx-tabs__link"
                                               data-toggle="tab">
                                                <span>Юр. лицо</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                
                                <div class="tab-content nx-section__item">
                                    <div class="tab-pane fade show active" id="individual">
                                        <div class="form-group" data-form-group>
                                            <label class="nx-dynamic-label" data-dynamic-label>
                                                <input type="text"
                                                       class="nx-dynamic-label__input nx-form-element"
                                                       name="name"
                                                       data-dynamic-inp>
                                
                                                <span class="nx-dynamic-label__text">Имя</span>
                                            </label>
                                        </div>
                        
                                        <div class="form-group" data-form-group>
                                            <label class="nx-dynamic-label" data-dynamic-label>
                                                <input type="text"
                                                       class="nx-dynamic-label__input nx-form-element"
                                                       name="phone"
                                                       data-phone-mask
                                                       data-dynamic-inp>
                                
                                                <span class="nx-dynamic-label__text">Телефон</span>
                                            </label>
                                        </div>
                        
                                        <div class="form-group nx-section__item" data-form-group>
                                            <label class="nx-dynamic-label" data-dynamic-label>
                                                <input type="text"
                                                       class="nx-dynamic-label__input nx-form-element"
                                                       name="email"
                                                       data-dynamic-inp>
                                
                                                <span class="nx-dynamic-label__text">Электронная почта</span>
                                            </label>
                                        </div>
                        
                                        <div class="form-group">
                                            <label class="checkbox checkbox_light">
                                                <input type="checkbox"
                                                       name="subscribe"
                                                       class="checkbox__input"
                                                       checked>
                                                <span class="checkbox__text">Подписаться на акции и спецпредложения. Не присылаем спам, только самые выгодные предложения и горячие скидки</span>
                                            </label>
                                        </div>
                        
                                        <div class="form-group">
                                            <label class="checkbox checkbox_light">
                                                <input type="checkbox"
                                                       name="signup_automatically"
                                                       class="checkbox__input"
                                                       checked>
                                                <span class="checkbox__text">Автоматически зарегистрировать меня для доступа в личный кабинет и автозаполнения полей</span>
                                            </label>
                                        </div>
                                    </div>
                    
                                    <div class="tab-pane fade" id="entity">
                                        <div class="form-group" data-form-group>
                                            <label class="nx-dynamic-label" data-dynamic-label>
                                                <input type="text"
                                                       class="nx-dynamic-label__input nx-form-element"
                                                       name="name"
                                                       data-dynamic-inp>
                                
                                                <span class="nx-dynamic-label__text">Имя</span>
                                            </label>
                                        </div>
                        
                                        <div class="form-group" data-form-group>
                                            <label class="nx-dynamic-label" data-dynamic-label>
                                                <input type="text"
                                                       class="nx-dynamic-label__input nx-form-element"
                                                       name="phone"
                                                       data-phone-mask
                                                       data-dynamic-inp>
                                
                                                <span class="nx-dynamic-label__text">Телефон</span>
                                            </label>
                                        </div>
                        
                                        <div class="form-group" data-form-group>
                                            <label class="nx-dynamic-label" data-dynamic-label>
                                                <input type="text"
                                                       class="nx-dynamic-label__input nx-form-element"
                                                       name="email"
                                                       data-dynamic-inp>
                                
                                                <span class="nx-dynamic-label__text">Электронная почта</span>
                                            </label>
                                        </div>
                        
                                        <div class="form-group" data-form-group>
                                            <label class="nx-dynamic-label" data-dynamic-label>
                                                <input type="text"
                                                       class="nx-dynamic-label__input nx-form-element"
                                                       name="inn"
                                                       data-dynamic-inp>
                                
                                                <span class="nx-dynamic-label__text">Инн</span>
                                            </label>
                                        </div>
                        
                                        <div class="form-group">
                                            <label class="checkbox checkbox_light">
                                                <input type="checkbox"
                                                       name="subscribe"
                                                       class="checkbox__input"
                                                       checked>
                                                <span class="checkbox__text">Подписаться на акции и спецпредложения. Не присылаем спам, только самые выгодные предложения и горячие скидки</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="nx-actions nx-actions_space-between">
                                    <div class="nx-actions__el">
                                        <a href="#" class="nx-actions__link link">
                                            <i class="link-icon mdi mdi-chevron-left"></i>
                                            
                                            <span>Назад</span>
                                        </a>
                                    </div>
                                    
                                    <div class="nx-actions__el">
                                        <a href="#" class="nx-actions__link btn">
                                            <span>К способу доставки</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    
                <div class="nx-order-block flex-col md-6 nx-section__col" data-affix-wrap>
                    <div class="nx-affix" data-affix-block>
                        <div class="nx-order-block__title">Ваш заказ</div>
    
                        <div class="nx-ordered">
                            <? for ($i = 0; $i < 2; $i++): ?>
                                <div class="nx-ordered__item">
                                    <div class="nx-ordered__body">
                                        <a href="" class="nx-ordered__title">
                                            <span>Баня Б-150 4х4 М с террасой и большой верандой</span>
                                        </a>
                    
                                        <div class="nx-cart-list__note">Артикул:
                                            <span>10073940</span>
                                        </div>
                                    </div>
                
                                    <div class="nx-ordered__info">
                                        <div class="nx-ordered__price-old">
                                            <span>1 600 000</span>
                        
                                            <i class="rub">q</i>
                                        </div>
                    
                                        <div class="nx-ordered__price">
                                            <span>1 577 000</span>
                        
                                            <i class="rub">q</i>
                                        </div>
                    
                                        <div class="nx-ordered__counter">
                                            <span>1</span> шт
                                        </div>
                                    </div>
                                </div>
                            <? endfor; ?>
                        </div>
                        
                        <div class="nx-cart-info">
                            <div class="nx-cart-info__body">
                                <div class="nx-cart-info__total">Итого, <span>4</span> позиции:</div>
                    
                                <div class="nx-cart-info__price h3">6 308 000 <i class="rub">q</i></div>
                    
                                <div class="nx-cart-info__discount">Скидка: 33 000 <i class="rub">q</i> </div>
                    
                                <div class="nx-cart-info__weight">Вес: <span>58,83</span> кг</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div data-stop-affix></div>
        </div>
    </div>
    <? require_once '../templates/_blocks/footer.php'; ?>
</div>

<script src="/assets/app.min.js"></script>
</body>
</html>
